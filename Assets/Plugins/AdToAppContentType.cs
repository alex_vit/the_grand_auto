namespace AdToApp 
{
	public static class AdToAppContentType
	{
		public const string IMAGE = "Image";
		public const string VIDEO = "Video";
		public const string INTERSTITIAL = "Interstitial"; //Image + Video
		public const string REWARDED = "Rewarded";
		public const string BANNER = "Banner";
		public const string NATIVE = "Native";
	};
}