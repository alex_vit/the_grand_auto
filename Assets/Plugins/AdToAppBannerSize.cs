﻿namespace AdToApp
{
    public static class AdToAppBannerSize
    {
        public const string Size_300x50 = "300x50";
        public const string Size_320x50 = "320x50";
        public const string Size_728x90 = "728x90";
        public const string Size_768x90 = "768x90";
        public const string Size_1024x90 = "1024x90";
        public const string Size_300x250 = "300x250";
        public const string Size_320x250 = "320x250";
    };
}
