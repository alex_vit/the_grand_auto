using UnityEngine;
using System.Collections;

public class EveryplayManager : MonoBehaviour
{
    public bool showUploadStatus = true;
    private bool isRecording = false;
    private bool isPaused = false;
    private bool isRecordingFinished = false;
    private GUIText uploadStatusLabel;
    private Texture2D previousThumbnail;

	public static EveryplayManager Instance;
	public bool useEveryplay;
	public bool isEnable
	{get {return useEveryplay /*&& Everyplay.IsSupported() && Everyplay.IsRecordingSupported()*/;}}
    void Awake()
    {
		if (!isEnable)
		{
			Destroy (gameObject);
		}
		if (Instance == null)
		{
//			if(enabled && showUploadStatus) {
//				CreateUploadStatusLabel();
//			}
			Instance = this;
			DontDestroyOnLoad(gameObject);
		}
		else
		{
			Destroy(gameObject);
		}
        
    }

    void Start()
    {
        if(uploadStatusLabel != null) {
            Everyplay.UploadDidStart += UploadDidStart;
            Everyplay.UploadDidProgress += UploadDidProgress;
            Everyplay.UploadDidComplete += UploadDidComplete;
        }

        Everyplay.RecordingStarted += RecordingStarted;
        Everyplay.RecordingStopped += RecordingStopped;

        Everyplay.ThumbnailReadyAtFilePath += ThumbnailReadyAtFilePath;

    }

    void Destroy()
    {
        if(uploadStatusLabel != null) {
            Everyplay.UploadDidStart -= UploadDidStart;
            Everyplay.UploadDidProgress -= UploadDidProgress;
            Everyplay.UploadDidComplete -= UploadDidComplete;
        }

        Everyplay.RecordingStarted -= RecordingStarted;
        Everyplay.RecordingStopped -= RecordingStopped;

        Everyplay.ThumbnailReadyAtFilePath -= ThumbnailReadyAtFilePath;
    }

    private void RecordingStarted()
    {
        isRecording = true;
        isPaused = false;
        isRecordingFinished = false;
    }

    private void RecordingStopped()
    {
        isRecording = false;
        isRecordingFinished = true;
    }

    private void CreateUploadStatusLabel()
    {
//        GameObject uploadStatusLabelObj = new GameObject("UploadStatus", typeof(GUIText));

//        if(uploadStatusLabelObj) {
//            uploadStatusLabelObj.transform.parent = transform;
//            uploadStatusLabel = uploadStatusLabelObj.GetComponent<GUIText>();
//
//            if(uploadStatusLabel != null) {
//                uploadStatusLabel.anchor = TextAnchor.LowerLeft;
//                uploadStatusLabel.alignment = TextAlignment.Left;
//                uploadStatusLabel.text = "Not uploading";
//            }
//        }
    }

    private void UploadDidStart(int videoId)
    {
        uploadStatusLabel.text = "Upload " + videoId + " started.";
    }

    private void UploadDidProgress(int videoId, float progress)
    {
        uploadStatusLabel.text = "Upload " + videoId + " is " + Mathf.RoundToInt((float)progress * 100) + "% completed.";
    }

    private void UploadDidComplete(int videoId)
    {
        uploadStatusLabel.text = "Upload " + videoId + " completed.";

        StartCoroutine(ResetUploadStatusAfterDelay(2.0f));
    }

    private IEnumerator ResetUploadStatusAfterDelay(float time)
    {
        yield return new WaitForSeconds(time);
        uploadStatusLabel.text = "Not uploading";
    }

    private void ThumbnailReadyAtFilePath(string path)
    {
        // We are loading the thumbnail during the recording for demonstration purposes only.
        // Normally you should start the load after you have stopped the recording to make sure the rendering does not stutter.
        Everyplay.LoadThumbnailFromFilePath(path, ThumbnailSuccess, ThumbnailError);
    }

    private void ThumbnailSuccess(Texture2D texture)
    {
        if(texture != null) {
            previousThumbnail = texture;
        }
    }

    private void ThumbnailError(string error)
    {
        Debug.Log("Thumbnail loading failed: " + error);
    }

	public void ShowEveryplay()
	{
		Everyplay.Show();
	}
	public void StopRecording()
	{
		if(isRecording) 
		{
			Everyplay.StopRecording();
			#if UNITY_EDITOR
			Debug.Log("The video recording is not available in the Unity editor. Please compile and run on a device.");
			#endif
		}
	}
	public void StartRecording()
	{
		if (!isRecording) {
						Everyplay.StartRecording ();
						#if UNITY_EDITOR
						Debug.Log ("The video recording is not available in the Unity editor. Please compile and run on a device.");
						#endif
				} else
						ResumeRecording ();
	}

	public void PauseRecording()
	{
		if (isRecording)
		{
			if(!isPaused) {
				Everyplay.PauseRecording();
				isPaused = true;
				#if UNITY_EDITOR
				Debug.Log("The video recording is not available in the Unity editor. Please compile and run on a device.");
				#endif
			}
		}

	}
	public void ResumeRecording()
	{
		if (isRecording)
		{
			if(isPaused) {
				Everyplay.ResumeRecording();
				isPaused = false;
				#if UNITY_EDITOR
				Debug.Log("The video recording is not available in the Unity editor. Please compile and run on a device.");
				#endif
			}
		}
	}
	public void PlayLastRecording()
	{
		if(isRecordingFinished) {
			Everyplay.PlayLastRecording();
			#if UNITY_EDITOR
			Debug.Log("The video playback is not available in the Unity editor. Please compile and run on a device.");
			#endif
		}
	}
//    void OnGUI()
//    {
//        if(GUI.Button(new Rect(10, 10, 138, 48), "Everyplay")) {
//			Everyplay.Show();
//            #if UNITY_EDITOR
//            Debug.Log("Everyplay view is not available in the Unity editor. Please compile and run on a device.");
//            #endif
//        }
//
//        if(isRecording && GUI.Button(new Rect(10, 64, 138, 48), "Stop Recording")) {
//            Everyplay.StopRecording();
//            #if UNITY_EDITOR
//            Debug.Log("The video recording is not available in the Unity editor. Please compile and run on a device.");
//            #endif
//        }
//        else if(!isRecording && GUI.Button(new Rect(10, 64, 138, 48), "Start Recording")) {
//            Everyplay.StartRecording();
//            #if UNITY_EDITOR
//            Debug.Log("The video recording is not available in the Unity editor. Please compile and run on a device.");
//            #endif
//        }
//
//        if(isRecording) {
//            if(!isPaused && GUI.Button(new Rect(10 + 150, 64, 138, 48), "Pause Recording")) {
//                Everyplay.PauseRecording();
//                isPaused = true;
//                #if UNITY_EDITOR
//                Debug.Log("The video recording is not available in the Unity editor. Please compile and run on a device.");
//                #endif
//            }
//            else if(isPaused && GUI.Button(new Rect(10 + 150, 64, 138, 48), "Resume Recording")) {
//                Everyplay.ResumeRecording();
//                isPaused = false;
//                #if UNITY_EDITOR
//                Debug.Log("The video recording is not available in the Unity editor. Please compile and run on a device.");
//                #endif
//            }
//        }
//
//        if(isRecordingFinished && GUI.Button(new Rect(10, 118, 138, 48), "Play Last Recording")) {
//            Everyplay.PlayLastRecording();
//            #if UNITY_EDITOR
//            Debug.Log("The video playback is not available in the Unity editor. Please compile and run on a device.");
//            #endif
//        }
//
//        if(isRecording && GUI.Button(new Rect(10, 118, 138, 48), "Take Thumbnail")) {
//            Everyplay.TakeThumbnail();
//            #if UNITY_EDITOR
//            Debug.Log("Everyplay take thumbnail is not available in the Unity editor. Please compile and run on a device.");
//            #endif
//        }
//
//        if(isRecordingFinished && GUI.Button(new Rect(10, 172, 138, 48), "Show sharing modal")) {
//            Everyplay.ShowSharingModal();
//            #if UNITY_EDITOR
//            Debug.Log("The sharing modal is not available in the Unity editor. Please compile and run on a device.");
//            #endif
//        }
//
//        if(previousThumbnail != null) {
////            int xPos = previousThumbnail.width;
////            int yPos = previousThumbnail.height - 10;
//
//            GUI.DrawTexture(new Rect(100, 100, 100, 100), previousThumbnail, ScaleMode.ScaleToFit, false, 0);
//        }
//    }
}
