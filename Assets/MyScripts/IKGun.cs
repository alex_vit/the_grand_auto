﻿using UnityEngine;
using System;
using System.Collections;

[RequireComponent(typeof(Animator))] 

public class IKGun : MonoBehaviour {
	
	protected Animator animator;
	[Range(0f,1f)]
	public float Weight=1f;
	
	public bool ikActive = false;
	public Transform rightHandObj = null;
	//public Transform rightElbowObj = null;
	public Transform lookObj = null;

	void Awake()
	{
		animator = GetComponent<Animator>();
	}
	
	//a callback for calculating IK
	void OnAnimatorIK()
	{
		if(animator) {
			
			//if the IK is active, set the position and rotation directly to the goal. 
			if(ikActive) {
				// Set the look target position, if one has been assigned
				if(lookObj != null) {
					animator.SetLookAtWeight(Weight);
					animator.SetLookAtPosition(lookObj.position);
				}    
				
				// Set the right hand target position and rotation, if one has been assigned
				if(rightHandObj != null) {
					animator.SetIKPositionWeight(AvatarIKGoal.RightHand,Weight);

					animator.SetIKRotationWeight(AvatarIKGoal.RightHand,Weight);
					animator.SetIKPosition(AvatarIKGoal.RightHand,rightHandObj.position);
					animator.SetIKRotation(AvatarIKGoal.RightHand,rightHandObj.rotation);
				}
				/*if(rightElbowObj != null) {
					animator.SetIKRotationWeight(AvatarIKHint.RightElbow,temp);
					
					//animator.SetIKRotationWeight(AvatarIKGoal.RightHand,temp);
					//animator.SetIKPosition(AvatarIKHint.RightElbow,rightHandObj.position);
					animator.SetIKRotation(AvatarIKHint.RightElbow,rightElbowObj.rotation);
				}*/
				
			}
			
			//if the IK is not active, set the position and rotation of the hand and head back to the original position
			else {          
				animator.SetIKPositionWeight(AvatarIKGoal.RightHand,0);
				//animator.SetIKRotationWeight(AvatarIKGoal.RightHand,0); 
				animator.SetLookAtWeight(0);
			}
		}
	}    
}
