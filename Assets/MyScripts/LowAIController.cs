using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityStandardAssets.Characters.ThirdPerson;

[RequireComponent(typeof (AICharacterControl))]
[RequireComponent(typeof (Weapon))]
public class LowAIController : MonoBehaviour {

	Transform MoveTarget;
	public SkinnedMeshRenderer SMR;
	public RagdollEnabler RagE;
	public Action<int,Transform> OnDeath;
	AICharacterControl AICC;
	Weapon weapon;
	float radiusAttack,defaultRA=1f;
	Transform AttackTarget,thisTrans;
	Ray ray;
	Vector3 tempVec;
	int currentWaypoint=0,team=0,enemyTeam;
	bool endWay=false;
	List<int> Targets=new List<int>();
	Animator anim;
	Rigidbody rig;
	Collider col;
	Healther HealthTarget;
	SliderForUnit SFU;



	//string enemyTag;


	public float RadiusAttack
	{
		get{ return radiusAttack;}
		set{
			if (value > defaultRA)
				radiusAttack = value;
			else
				radiusAttack = defaultRA;
		}
	}

	public Transform TargetAttack
	{
		get{return AttackTarget;}
		set{if(AttackTarget!=value) AttackTarget=value;}
	}

	void Awake()
	{
		thisTrans = transform;
		weapon = GetComponent<Weapon> ();
		AICC = GetComponent<AICharacterControl> ();
		GetComponent<Healther> ().OnDeath += Death;
		anim = GetComponent<Animator> ();
		rig= GetComponent<Rigidbody> ();
		col= GetComponent<Collider> ();
		SFU= GetComponent<SliderForUnit> ();
	}

	public void Init(int Team)
	{
		team = Team;
		enemyTeam = (team + 1) % 2;
		tag = "Team" + team;
		//enemyTag = "Team" + enemyTeam;
		RadiusAttack = 15f;
		currentWaypoint = 0;
		if (!gameObject.activeSelf)
			gameObject.SetActive (true);
		Live ();
		weapon.PW.ChangeCountAmmo = 10000;
		weapon.PW.Init ();
		weapon.PW.ActivePW ();
		endWay = false;
		//InvokeRepeating ("DetectionEnemy",0f,UnityEngine.Random.Range(1f,1.5f));
		InvokeRepeating ("SelectTarget",0f,1f);
		InvokeRepeating ("UlimitedAmmo",60f,60f);
	}

	void Death(GameObject sender)
	{
		SFU.Disable ();
		//Targets.Clear ();
		AICC.agent.Stop ();
		AICC.agent.enabled = false;
		AttackTarget = null;
		HealthTarget = null;
		col.enabled = false;
		AICC.character.enabled = false;
		//anim.SetBool ("KickHand", false);
		anim.enabled = false;
		rig.isKinematic = true;
		//RagE.ActivateRagdoll ();
		CancelInvoke ();
		if (OnDeath != null) OnDeath (team,thisTrans);

	}

	void Live()
	{
		SFU.Active ();
		AICC.agent.enabled = true;
		col.enabled = true;
		AICC.character.enabled = true;
		//anim.SetBool ("KickHand", false);
		anim.enabled = true;
		rig.isKinematic = false;
		//RagE.DisableRagdoll ();
	}

	void UlimitedAmmo()
	{
		weapon.PW.ChangeCountAmmo = 10000;
	}

	void Update () {
		if (AttackTarget != null&&HealthTarget.isLived) {
			/*ray.origin = thisTrans.position;
			ray.direction = (AttackTarget.position - thisTrans.position).normalized;*/
			thisTrans.LookAt (AttackTarget.position);
			if((thisTrans.position - AttackTarget.position).magnitude <= RadiusAttack) weapon.MakeAShot (HealthTarget);
			else AttackTarget=null;
		}
	}

	void DetectionEnemy()
	{
		Transform tempTrans;
		TargetAttack = null;
		for (int i=0; i<DotaController.Instance.ListTeams[enemyTeam].Count; i++){
			//tempTrans=DotaController.Instance.ListTeams[enemyTeam][i];
			if ((thisTrans.position-DotaController.Instance.ListTeams[enemyTeam][i].position).magnitude <= RadiusAttack) Targets.Add(i);
		}
		if (Targets.Count != 0) {
			TargetAttack = DotaController.Instance.ListTeams[enemyTeam][Targets [UnityEngine.Random.Range (0, Targets.Count)]];
			HealthTarget = AttackTarget.GetComponent<Healther> ();
		}
		Targets.Clear ();
	}

	void SelectTarget()
	{
		if (AttackTarget==null) DetectionEnemy();
		else if(!HealthTarget.isLived) DetectionEnemy();
		if (AttackTarget == null) {
			if (!endWay) {
				if (AICC.EndPoint) {
					MoveTarget = DotaController.Instance.GetWaypoint (team, currentWaypoint);
					AICC.Target = MoveTarget;
					AICC.Move ();
					if (MoveTarget != null)
						currentWaypoint++;
					else
						endWay = true;
				} else
					AICC.Move ();
			} else {
				if (!AICC.EndPoint)
					AICC.Move ();
				else
					AICC.Stop ();
			}
		} else {
			AICC.Stop();
		}
	}
	
	/*void DetectionEnemy()
	{
		Transform tempTrans;
		for (int i=0; i<DotaController.Instance.ListTeams[enemyTeam].Count; i++){
			tempTrans=DotaController.Instance.ListTeams[enemyTeam][i];
			if ((thisTrans.position-tempTrans.position).magnitude <= RadiusAttack)
			{
				if(!Targets.Contains(tempTrans)) Targets.Add(tempTrans);
			}
			else Targets.Remove(tempTrans);
		}
	}*/

	/*void SelectTarget()
	{
		if (Targets.Count == 0) {
			AttackTarget=null;
			if (!endWay){
				if (AICC.EndPoint) {
					MoveTarget=DotaController.Instance.GetWaypoint(team,currentWaypoint);
					AICC.Target = MoveTarget;
					AICC.Move();
					if(MoveTarget!=null) currentWaypoint++;
					else endWay=true;
				} else AICC.Move();
			} else
			{
				if (!AICC.EndPoint) AICC.Move();
				else AICC.Stop();
			}
		}
		else {
			AICC.Stop();
			if(!Targets.Contains(TargetAttack)) {

				if(Targets.Count!=0) {
					TargetAttack=Targets[UnityEngine.Random.Range(0,Targets.Count)];
					HealthTarget=TargetAttack.GetComponent<Healther>();
				}
			}
			else
			{
				if(!HealthTarget.isLived)
				{
					Targets.Remove(TargetAttack);
					if(Targets.Count!=0) {
						TargetAttack=Targets[UnityEngine.Random.Range(0,Targets.Count)];
						HealthTarget=TargetAttack.GetComponent<Healther>();
					}
					else TargetAttack=null;
				}
			}
		}
	}*/
}
