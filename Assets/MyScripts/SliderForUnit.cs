﻿using UnityEngine;
using System.Collections;

public class SliderForUnit : MonoBehaviour {

	float val=1f;
	public Transform Background;
	public Transform Foreground;
	public Transform Target;
	public Transform GO;
	public bool StaticSlider=false;
	float halfSizeSlider,scaleSlider;
	SpriteRenderer SR;
	Vector3 tempVec,defaultPos,defPosForBack;
	float tempY=0f,tempZ=0.8f;
	bool isInit=false;
	// Use this for initialization

	public void Active()
	{
		GO.parent = Target.parent;
	}

	public void Disable()
	{
		GO.parent = Target;
	}

	void Awake()
	{
		Init();
	}

	public float Value
	{
		get{ return val;}
		set{
			if (value >= 0){
				if(value>1f)
				val = 1f;
				else val = value;
			}
			else
				val = 0;
			ChangeSlider();
		}
	}
	public void Init()
	{
		//go = gameObject;
		defaultPos = GO.localPosition;
		//tempY = thisTrans.localPosition.y;
		SR = Foreground.GetComponent<SpriteRenderer> ();
		halfSizeSlider = SR.sprite.bounds.size.x / 2f;
		scaleSlider = Foreground.localScale.x;
		defPosForBack = Foreground.localPosition;
		isInit = true;
	}

	void LateUpdate()
	{
		if (StaticSlider)
			return;
		if (Target != null) {
			tempVec=Target.position;
			tempVec.x+=defaultPos.x;
			tempVec.y+=defaultPos.y;
			tempVec.z+=defaultPos.z;
			GO.position = tempVec;
		}
	}

	void ChangeSlider()
	{
		if (!isInit) Init ();
		//halfSizeSlider = (1f-valueXp);
		//scaleSlider = valueXp;
		float temp = -halfSizeSlider * (1f - val);
		defPosForBack.x = temp;
		//Foreground.localPosition = new Vector3 (temp, 0, 0);
		Foreground.localPosition = defPosForBack;
		temp = scaleSlider * val;
		Foreground.localScale = new Vector3 (temp,1,1);
		if (val > 0.66f)
			SR.color = Color.green;
		else
			if(val>0.33f)
				SR.color = Color.yellow;
		else
			SR.color = Color.red;

	}
}
