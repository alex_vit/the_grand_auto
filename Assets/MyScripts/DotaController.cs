﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class DotaController : MonoBehaviour {

	public static DotaController Instance;
	public Transform ParentWaypoints,ParentStartPoints,ParentUnits;
	public Transform[,] Waypoints;
	public Dictionary<int,List<Transform>> ListTeams;
	Dictionary<int,List<LowAIController>> ListLowAIC;
	Transform[] StartPoints;
	public Material[] MaterialTeams;
	public Texture[] TextureTeams;
	public Transform[] Bases;
	int[] countWaypoints;
	YieldInstruction yi_returninpull = new WaitForSeconds (3f);
	Vector3 defaultPos = new Vector3 (1000, 1000, 1000);

	public event EmptyEvent OnWin;
	public event GameObjectEvent OnLose;

	//public List<LowAIController> DefenseTeam=new List<LowAIController>();
	//public List<LowAIController> AttackTeam=new List<LowAIController>();
	//public List<Transform> AllObjectDefenseTeam=new List<Transform>();
	//public List<Transform> AllObjectAttackTeam=new List<Transform>();

	List<GameObject> PullObjectsTeams=new List<GameObject>();

	void ReturnInPull(GameObject sender)
	{
		sender.SetActive (false);
		PullObjectsTeams.Add (sender);
		sender.transform.position = defaultPos;
		//Debug.Log ("RETURN");
		//StartCoroutine (ReturnGOinPull(sender));
	}

	void RemoveInListTeams(int t,Transform targ)
	{
		ListTeams [t].Remove (targ);
		//for(int i=0;i<
	}

	IEnumerator ReturnGOinPull(GameObject sender)
	{
		yield return yi_returninpull;
		sender.SetActive (false);
		PullObjectsTeams.Add (sender);
		sender.transform.position = defaultPos;
		//StopAllCoroutines ();
	}

	public void YouLoose()
	{
		//Debug.Log ("BB");
		if (OnLose != null)
			OnLose (gameObject);
		ReturnInDefault ();
	}

	public void YouLooseBecauseDeathPlayer(GameObject sender)
	{
		if (tempCountVideo2 == 1) {
			//LifeController.Instance.Ressurection();
			YouLoose();
		} else {
			MissionController.Instance.EnableMissionFailedOnDeath ();
			PlayerController.Instance.TPlayer.GetComponent<Healther> ().OnDeath -= LifeController.Instance.CharDeathNoEffects;
			tempCountVideo2++;
		}
	}

	public void YouWin()
	{
		if (OnWin != null)
			OnWin ();
		ReturnInDefault ();
	}

	void OnEnable()
	{
		OnWin += MissionController.Instance.MissionCompleted;
		OnLose += MissionController.Instance.MissionFailed;
	}
	void OnDestroy()
	{
		OnWin -= MissionController.Instance.MissionCompleted;
		OnLose -= MissionController.Instance.MissionFailed;
	}



	/*public void Init()
	{
		MaterialTeams [0].mainTexture = TextureTeams [0];
		MaterialTeams [1].mainTexture = TextureTeams [1];
	}*/

	public Transform GetWaypoint(int num,int last)
	{
		if (countWaypoints [num] > last + 1) return Waypoints [num, last+1];
		return null;
	}

	void Awake()
	{
		Instance = this;
		countWaypoints = new int[2];
		countWaypoints [0] = ParentWaypoints.childCount;
		countWaypoints [1] = ParentWaypoints.childCount;
		Waypoints=new Transform[2,ParentWaypoints.childCount];
		//Waypoints[0]=new Transform[ParentWaypoints.childCount];
		//Waypoints[1]=new Transform[ParentWaypoints.childCount];
		StartPoints=new Transform[ParentStartPoints.childCount];
		for (int i=0,k=ParentWaypoints.childCount-1; i<ParentWaypoints.childCount; i++,k--) {
			Waypoints [0,i] = ParentWaypoints.GetChild (i);
			Waypoints [1,i] = ParentWaypoints.GetChild (k);
		}
		for (int i=0; i<ParentStartPoints.childCount; i++)
			StartPoints [i] = ParentStartPoints.GetChild (i);
	}




	void CreateUnits()
	{
		GameObject obj;
		LowAIController tempAI;
		bool isCreate = false;
		for (int i=0; i<5; i++) {
			if (PullObjectsTeams.Count == 0) {
				obj = Instantiate (StaticData.Prefabs ["AIUnit"], StartPoints [0].position, Quaternion.identity) as GameObject;
				obj.transform.parent=ParentUnits;
				obj.SetActive(true);
				obj.GetComponent<Healther>().OnDeath+=ReturnInPull;
				/*obj2 = Instantiate(StaticData.Prefabs ["SliderHP"]) as GameObject;
				obj2.GetComponent<SliderForUnit>().Target=obj.transform;*/
				isCreate=true;
			} else {
				obj = PullObjectsTeams [0];
				PullObjectsTeams.Remove (obj);
				obj.transform.position=StartPoints [0].position;
				obj.transform.parent=ParentUnits;
				obj.SetActive(true);
			}
			tempAI = obj.GetComponent<LowAIController> ();
			if(isCreate) {
				isCreate=false;
				tempAI.OnDeath+=RemoveInListTeams;
			}
			tempAI.SMR.material=MaterialTeams[0];
			ListTeams [0].Add (obj.transform);
			tempAI.Init (0);
			//DefenseTeam.Add (tempAI);
			//PullObjectsTeams.Add (obj);
			if (PullObjectsTeams.Count == 0) {
				obj = Instantiate (StaticData.Prefabs ["AIUnit"], StartPoints [1].position, Quaternion.identity) as GameObject;
				obj.transform.parent=ParentUnits;
				obj.SetActive(true);
				obj.GetComponent<Healther>().OnDeath+=ReturnInPull;
				/*obj2 = Instantiate(StaticData.Prefabs ["SliderHP"]) as GameObject;
				obj2.GetComponent<SliderForUnit>().Target=obj.transform;*/
				isCreate=true;
			} else {
				obj = PullObjectsTeams [0];
				PullObjectsTeams.Remove (obj);
				obj.transform.position=StartPoints [1].position;
				obj.transform.parent=ParentUnits;
				obj.SetActive(true);
			}
			tempAI = obj.GetComponent<LowAIController> ();
			if(isCreate) {
				isCreate=false;
				tempAI.OnDeath+=RemoveInListTeams;
			}
			tempAI.SMR.material=MaterialTeams[1];
			ListTeams [1].Add (obj.transform);
			tempAI.Init (1);
			//DefenseTeam.Add (tempAI);
			//PullObjectsTeams.Add (obj);
		}


	}

	void ReturnInDefault()
	{
		GameController.Instance.MMMCAMERA.SetActive (true);
		StoreGun.Instance.EnableChangeValue ();
		//LifeController.Instance.miniGame = false;
		MainGameController.Instance.State = GameState.WALK;
		GameMenu.Instance.countVideo = tempCountVideo;
		MissionController.Instance.EnableMissionFailedOnDeath ();
		PlayerController.Instance.TPlayer.GetComponent<CarDetecting> ().enabled = true;
		PlayerController.Instance.TPlayer.GetComponent<Player> ().EnableOnDeath ();
		Healther tempH = PlayerController.Instance.TPlayer.GetComponent<Healther> ();
		tempH.SetDefaultHealth ();
		tempH.OnDeath -= LifeController.Instance.CharDeathNoEffects;
		tempH.OnDeath -= YouLooseBecauseDeathPlayer;
		PlayerController.Instance.ReturnDefaultAmmo ();
		ChooseWeapon.Instance.SwitchWeapon (-1);
		GameController.Instance.SetDynamicJoystick ();
		GameController.Instance.ExitMiniGame ();

	}
	int tempCountVideo,tempCountVideo2;
	void SetForMiniGame(int tm)
	{
		GameController.Instance.MMMCAMERA.SetActive (false);
		StoreGun.Instance.DisableChangeValue ();
		//LifeController.Instance.miniGame = true;
		MainGameController.Instance.State = GameState.MINI_GAME;
		tempCountVideo = GameMenu.Instance.countVideo;
		GameMenu.Instance.countVideo = 0;
		tempCountVideo2 = 0;
		MissionController.Instance.DisableMissionFailedOnDeath ();
		PlayerController.Instance.SetEnemyTag = "Team" + tm;
		PlayerController.Instance.TPlayer.GetComponent<CarDetecting> ().enabled = false;
		PlayerController.Instance.TPlayer.GetComponent<Player> ().DisableOnDeath ();
		Healther tempH = PlayerController.Instance.TPlayer.GetComponent<Healther> ();
		tempH.SetDefaultHealth ();
		tempH.OnDeath += LifeController.Instance.CharDeathNoEffects;
		tempH.OnDeath += YouLooseBecauseDeathPlayer;
		//PlayerController.Instance.TPlayer.GetComponent<Healther> ().OnDeath += RemoveInListTeams;
		ChooseWeapon.Instance.SwitchWeapon (0);
		PlayerController.Instance.Init ();
		GameController.Instance.SetStaticJoystick ();
	}
	public void StartMiniGame(int YourTeam)
	{
		BaseController.Instance.Init (YourTeam);
		ListTeams=new Dictionary<int, List<Transform>>();
		//ListLowAIC=new Dictionary<int, List<LowAIController>>();
		ListTeams [0] = new List<Transform> ();
		ListTeams [1] = new List<Transform> ();
		ListTeams [YourTeam].Add (PlayerController.Instance.TPlayer);
		ListTeams [YourTeam].Add (Bases[YourTeam]);
		int tempI = (YourTeam + 1) % 2;
		ListTeams [tempI].Add (Bases[tempI]);
		MaterialTeams [0].mainTexture = TextureTeams [0];
		MaterialTeams [1].mainTexture = TextureTeams [1];
		SetForMiniGame(tempI);
		InvokeRepeating ("CreateUnits",5f,30f);

	}

	void Start()
	{
		StartMiniGame (0);
	}


}
