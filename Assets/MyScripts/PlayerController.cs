using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.Events;
using System.Collections;
using System.Collections.Generic;
using System;

public class PlayerController : MonoBehaviour {

	public static PlayerController Instance;
	public Camera TopCam;
	public Transform MnCam;
	//public Tank MyTank;
	//public Camera MainCam;
	//public int TypeCam;
	//public Transform ForPosition;
	//public CharacterController CharCtrl;
	//public DemoRpgCharaCS Freak2;
	public EventSystem ES;
	public GraphicRaycaster GR;
	//public Image ReloadImage;
	public Camera GunCamera;
	public Camera CurrCam;
	PointerEventData pED;
	List<RaycastResult> list= new List<RaycastResult>();
	int sizeX,sizeY,materialTank;
	bool isInitPlayer=false,isRespawnPoint=false,isInitCotroller=false,pauseGame=false;
	RaycastHit hit;
	Ray ray;
	Vector2 tempToMove,tempPos;
	Vector3 posBegin;
	float timeToReload,fillInTime;
	int num,countBullet,CurCountBullet,healthTank;
	YieldInstruction YI_reload,YI_endframe,YI_fixed;
	Color32 colorGreen=new Color32(241, 255, 151,255);
	Color32 colorBlue=new Color32(147, 232,254,255);
	Vector3 PosCenterCam = new Vector3 (Screen.width / 2, Screen.height / 2, 0);
	string enemyTag="";
	int lastTypeFire;
	public void ActiveWeapon()
	{
		GunCamera.enabled = true;
	}
	public void InActiveWeapon()
	{
		GunCamera.enabled = false;
	}
	
	public Camera SetGunCam
	{
		set{ GunCamera = value;}
	}

	public string SetEnemyTag
	{
		get{return enemyTag;}
		set{if(value==null) enemyTag="";
			else enemyTag=value;

		}
	}

	void Awake()
	{
		Instance = this;
		pED = new PointerEventData(ES);
		WPlayer = TPlayer.GetComponent<Weapon> ();
	}

	void Start()
	{
		ChangeTypeFire (1);
	}

	public void Init()
	{
		if(WPlayer.PW.CPS!=null) WPlayer.PW.CPS.gameObject.SetActive (false);
		WPlayer.PW.CPS = GameController.Instance.InMiniGame;
		lastTypeFire = (int)TF;
		ChangeTypeFire (0);
		countInCage = WPlayer.PW.CurCountAmmoInCage;
		countAmmo = WPlayer.PW.CountAmmo;
		WPlayer.PW.CurrentCountAmmoInCage = 0;
		WPlayer.PW.CountAmmo = 0;
		WPlayer.PW.ChangeCountAmmo = 10000;
		WPlayer.PW.GunReloadAmmo ();
		InvokeRepeating ("UnlimitedAmmoPistol",0f,60f);
		//WPlayer.PW.GunReloadAmmo ();
	}

	public void ReturnDefaultAmmo()
	{
		if(WPlayer.PW.CPS!=null) WPlayer.PW.CPS.gameObject.SetActive (false);
		WPlayer.PW.CPS = GameController.Instance.InGame;
		ChangeTypeFire (lastTypeFire);
		WPlayer.PW.CurCountAmmoInCage = countInCage;
		WPlayer.PW.ChangeCountAmmo = countAmmo;
		//StaticData.PD.CountAmmoWeapons [WPlayer.PW.ID] = countInCage + countAmmo;
	}

	void UnlimitedAmmoPistol()
	{
		WPlayer.PW.ChangeCountAmmo = 10000;
	}

	enum TypeFire
	{
		ViewTop,//вид сверху
		View3D,// вид сзади
		ViewFirstPerson//вид от первого лица
	}
	TypeFire TF;
	
	public void ChangeTypeFire(int type)
	{
		MakeShot=null;
		switch ((TypeFire)type) {
		case TypeFire.ViewTop:
			TF=TypeFire.ViewTop;
			MakeShot+=ShotViewTop;
			GameController.Instance.TPUC.m_Cam=null;
			break;
		case TypeFire.View3D:
			TF=TypeFire.View3D;
			MakeShot+=ShotView3D;
			GameController.Instance.TPUC.m_Cam=MnCam;
			break;
		case TypeFire.ViewFirstPerson:
			TF=TypeFire.ViewFirstPerson;
			//MakeShot+=ShotViewFirstPerson;
			GameController.Instance.TPUC.m_Cam=MnCam;
			break;
		default:
			//Debug.Log ("BB");
			TF=TypeFire.View3D;
			MakeShot+=ShotView3D;
			GameController.Instance.TPUC.m_Cam=MnCam;
			break;
		}
	}
	Action MakeShot;
	public Transform TPlayer;
	Vector3 IMousePos,LookPoint;
	string tagPlayer="TPControl";
	int layerTPControl = 1 << 9;
	RaycastHit[] hits;
	bool isHit=false;
	Weapon WPlayer;
	Healther targetHit;
	int countInCage,countAmmo;

	void Shoot()
	{
		ray.origin=TPlayer.position;
		ray.direction=LookPoint-ray.origin;
		WPlayer.MakeAShot(ray);
	}

	enum StateGun
	{
		None,
		Wearing
	}
	void ShotViewTop()
	{
		#if(UNITY_STANDALONE || UNITY_EDITOR)
		if (Input.GetMouseButtonDown (1)) {
			IMousePos = Input.mousePosition;
			pED.position = IMousePos;
			list.Clear();
			GR.Raycast(pED, list);
			if(list.Count==0)
			{
				ray = TopCam.ScreenPointToRay (IMousePos);
				//Debug.Log (ray.origin);
				hits = Physics.RaycastAll (ray);
				for (num=0; num<hits.Length; num++) {
					//Debug.Log (hits [num].collider.tag);
					if (hits [num].collider.CompareTag(tagPlayer)) {
						isHit=true;
						LookPoint=hits [num].point;
					}
					if(hits [num].collider.CompareTag(enemyTag))
					{
						targetHit=hits [num].collider.GetComponent<Healther>();
					}
				}
				if(isHit)
				{
					isHit=false;
					LookPoint.y=TPlayer.position.y;
					TPlayer.LookAt(LookPoint);
					if(targetHit!=null) {
						//Debug.Log ("AA");
						WPlayer.MakeAShot(targetHit);
						targetHit=null;
					}
					//Shoot();
				}
				else targetHit=null;
			}
		}
		#endif
		#if (MOBILE_INPUT||UNITY_ANDROID)&&!UNITY_EDITOR
		for( int i=0;i<Input.touchCount;i++)
		{
			IMousePos = Input.touches[i].position;
			pED.position = IMousePos;
			list.Clear();
			GR.Raycast(pED, list);
			if(list.Count==0)
			{
				if(Input.touches[i].phase!=TouchPhase.Began) return;
				ray = TopCam.ScreenPointToRay (IMousePos);
				//Debug.Log (ray.origin);
				hits = Physics.RaycastAll (ray);
				for (num=0; num<hits.Length; num++) {
					//Debug.Log (hits [num].collider.tag);
					if (hits [num].collider.CompareTag(tagPlayer)) {
						isHit=true;
						LookPoint=hits [num].point;
					}
					if(hits [num].collider.CompareTag(enemyTag))
					{
						targetHit=hits [num].collider.GetComponent<Healther>();
					}
				}
				if(isHit)
				{
					isHit=false;
					LookPoint.y=TPlayer.position.y;
					TPlayer.LookAt(LookPoint);
					if(targetHit!=null) {
						//Debug.Log ("AA");
						WPlayer.MakeAShot(targetHit);
						targetHit=null;
					}
					//Shoot();
				}
				else targetHit=null;
			}
		}


		/*if (Input.touchCount>0) {
			IMousePos = Input.touches[0].position;
			pED.position = IMousePos;
			list.Clear();
			GR.Raycast(pED, list);
			if(list.Count==0)
			{
				if(Input.touches[0].phase!=TouchPhase.Began) return;
				ray = TopCam.ScreenPointToRay (IMousePos);
				//Debug.Log (ray.origin);
				hits = Physics.RaycastAll (ray);
				for (num=0; num<hits.Length; num++) {
					//Debug.Log (hits [num].collider.tag);
					if (hits [num].collider.CompareTag(tagPlayer)) {
						isHit=true;
						LookPoint=hits [num].point;
					}
					if(hits [num].collider.CompareTag(enemyTag))
					{
						targetHit=hits [num].collider.GetComponent<Healther>();
					}
				}
				if(isHit)
				{
					isHit=false;
					LookPoint.y=TPlayer.position.y;
					TPlayer.LookAt(LookPoint);
					if(targetHit!=null) {
						//Debug.Log ("AA");
						WPlayer.MakeAShot(targetHit);
						targetHit=null;
					}
					//Shoot();
				}
				else targetHit=null;
			}
			else{
				for(int i=0;i<list.Count;i++)
				{
					if(list[i].gameObject.CompareTag("Stick")) {
						isHit=true;
					}
				}
				if(isHit)
				{
					isHit=false;
					IMousePos = Input.touches[1].position;
					pED.position = IMousePos;
					list.Clear();
					GR.Raycast(pED, list);
					if(list.Count==0)
					{
						if(Input.touches[1].phase!=TouchPhase.Began) return;
						ray = TopCam.ScreenPointToRay (IMousePos);
						//Debug.Log (ray.origin);
						hits = Physics.RaycastAll (ray);
						for (num=0; num<hits.Length; num++) {
							//Debug.Log (hits [num].collider.tag);
							if (hits [num].collider.CompareTag(tagPlayer)) {
								isHit=true;
								LookPoint=hits [num].point;
							}
							if(hits [num].collider.CompareTag(enemyTag))
							{
								targetHit=hits [num].collider.GetComponent<Healther>();
							}
						}
						if(isHit)
						{
							isHit=false;
							LookPoint.y=TPlayer.position.y;
							TPlayer.LookAt(LookPoint);
							if(targetHit!=null) {
								//Debug.Log ("AA");
								WPlayer.MakeAShot(targetHit);
								targetHit=null;
							}
							//Shoot();
						}
						else targetHit=null;
					}
				}
				isHit=false;
			}
		}*/
		#endif
	}
	void ShotView3D()
	{
	}
	public void ShotViewFirstPerson()
	{
		//Debug.Log ("AA");
		WPlayer.MakeAShot (CurrCam.ScreenPointToRay (PosCenterCam));
		#if UNITY_EDITOR
		//if (GunCamera.enabled&&ChooseWeapon.Instance.ActiveWeaponNoHands&&Time.timeScale>0f) {
		/*if (WPlayer.PW.OneShotTime) {
			if (Input.GetMouseButtonDown (0))
				WPlayer.MakeAShot (CurrCam.ScreenPointToRay (PosCenterCam));
		} else if (Input.GetMouseButton (0))
			WPlayer.MakeAShot (CurrCam.ScreenPointToRay (PosCenterCam));
		//}*/
		#else
		WPlayer.MakeAShot (CurrCam.ScreenPointToRay (PosCenterCam));
		#endif
	}

	void LateUpdate () {
		if(WPlayer.PW!=null) if(MakeShot!=null) MakeShot ();
	}


	/*void ShotViewTop()
	{
		#if(UNITY_STANDALONE || UNITY_EDITOR)
		if (Input.GetMouseButtonDown (1)) {
			IMousePos = Input.mousePosition;
			pED.position = IMousePos;
			list.Clear();
			GR.Raycast(pED, list);
			if(list.Count==0)
			{
				ray = TopCam.ScreenPointToRay (IMousePos);
				//Debug.Log (ray.origin);
				hits = Physics.RaycastAll (ray);
				for (num=0; num<hits.Length; num++) {
					//Debug.Log (hits [num].collider.tag);
					if (hits [num].collider.CompareTag(tagPlayer)) {
						isHit=true;
						LookPoint=hits [num].point;
						break;
					}
				}
				if(isHit)
				{
					isHit=false;
					LookPoint.y=TPlayer.position.y;
					TPlayer.LookAt(LookPoint);
					Shoot();
				}
			}
		}
		#endif
#if MOBILE_INPUT||UNITY_ANDROID
		if (Input.touchCount>0) {
			IMousePos = Input.mousePosition;
			pED.position = IMousePos;
			list.Clear();
			GR.Raycast(pED, list);
			if(list.Count==0)
			{
				ray = TopCam.ScreenPointToRay (IMousePos);
				//Debug.Log (ray.origin);
				hits = Physics.RaycastAll (ray);
				for (num=0; num<hits.Length; num++) {
					//Debug.Log (hits [num].collider.tag);
					if (hits [num].collider.CompareTag(tagPlayer)) {
						isHit=true;
						LookPoint=hits [num].point;
						break;
					}
				}
				if(isHit)
				{
					isHit=false;
					LookPoint.y=TPlayer.position.y;
					TPlayer.LookAt(LookPoint);
					Shoot();
				}
			}
		}
#endif
	}*/







	/*
		if (isInitCotroller&&!pauseGame) {
#if(UNITY_STANDALONE || UNITY_EDITOR)
			if (Input.GetMouseButtonDown (1)) {
				pED.position = Input.mousePosition;
				list.Clear();
				GR.Raycast(pED, list);
				if(list.Count==0)
				{
					ray = MainCam.ScreenPointToRay (Input.mousePosition);
					hits = Physics.RaycastAll (ray);
					for (num=0; num<hits.Length; num++) {
						if (hits [num].collider.tag == "Plane") {
							ForPosition.position = hits [num].point;
							MyTank.TargetAttack = ForPosition;
							if (MyTank.MayShoot (curCountBullet)) {
								curCountBullet -= (MyTank.ATT.currentModeAttack + 1);
								MyTank.Shoot ();
							} else
								MyTank.RotateTower ();
							MyTank.TargetAttack = null;
						}
					}
				}
			}
#else
		if((Input.touchCount==1)&&(Input.touches[0].phase==TouchPhase.Began)&&((Input.touches[0].position.x >sizeX)  || (Input.touches[0].position.y > sizeY))){
			pED.position = Input.touches[0].position;
			list.Clear();
			GR.Raycast(pED, list);
			if(list.Count==0) MayShoot(0);
		}
		else if((Input.touchCount==2)&&(Input.touches[1].phase==TouchPhase.Began)&&((Input.touches[1].position.x >sizeX)  || (Input.touches[1].position.y > sizeY))){
			pED.position = Input.touches[1].position;
			list.Clear();
			GR.Raycast(pED, list);
			if(list.Count==0) MayShoot(1);
		}
#endif
		}
	}*/

	/*public void Init (int skin,int teamNum) {
		isInitCotroller = true;
		MyTank.ATT.InitMaterial = BattleController.Instance.MaterialsForTanks [0];
		MyTank.Team = teamNum;
		timeToReload/=countBullet;
		YI_reload = new WaitForSeconds (timeToReload);
		fillInTime = Time.fixedDeltaTime/timeToReload;
	}*/
}
