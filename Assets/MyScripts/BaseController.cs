﻿using UnityEngine;
using System.Collections;

public class BaseController : MonoBehaviour {

	public static BaseController Instance;
	//public GameObject DefenseBase,AttackBase;

	public GameObject[] Bases;
	int yourTeam;

	public void DestroyBase(GameObject sendBase)
	{
		if (sendBase == Bases [0]) {
			if (yourTeam == 0)
				DotaController.Instance.YouLoose ();
			else
				DotaController.Instance.YouWin ();
		} else {
			if(sendBase == Bases [1])
			{
				if (yourTeam == 1)
					DotaController.Instance.YouLoose ();
				else
					DotaController.Instance.YouWin ();
			}
		}
	}

	public void Init(int YourTeam)
	{
		yourTeam = YourTeam;

	}

	void Awake()
	{
		Instance = this;
		Bases [0].GetComponent<Healther> ().OnDeath += DestroyBase;
		Bases [1].GetComponent<Healther> ().OnDeath += DestroyBase;
		Bases [0].tag = "Team0";
		Bases [1].tag = "Team1";
	}
}
