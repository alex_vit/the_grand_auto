﻿using UnityEngine;
using System.Collections;

public class CameraController : MonoBehaviour {

	public Transform Target;
	public Transform Cam;
	public float Distance=200f;
	Transform thisTrans;
	Vector3 temp;

	void Start()
	{
		temp = Cam.position;
		temp.y = Distance;
		Cam.position = temp;
	}

	void LateUpdate()
	{
		temp.x= Target.position.x;
		temp.z = Target.position.z;
		Cam.position = temp;
	}
}
