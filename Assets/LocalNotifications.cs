﻿using UnityEngine;

public class LocalNotifications : MonoBehaviour {
	
	public int PeriodNotifyInSeconds = 43200;//период между сообщениями
	public bool RealtimeSend=true;
	//true- время будет отсчитываться от начала работы устройства
	//false - время будет отсчитываться по дате
	//рекомендуется для частых уведомлений меньше 1-10 часов использовать true
	//для 10 и более false
	public TypeSignal TypeSignalNotify;//тип сигнала
	public string[] RusTexts;
	public string[] EngTexts;
	
	
	public enum TypeSignal
	{
		Lights,
		Vibrate, // Необходим доступ в manifest <uses-permission android:name="android.permission.VIBRATE"/>
		Lights_Vibrate  // Необходим доступ в manifest <uses-permission android:name="android.permission.VIBRATE"/>
	}
	
	int typeSignal()
	{
		int tempType=0;
		switch (TypeSignalNotify) {
		case TypeSignal.Lights:
			tempType=0;
			break;
		case TypeSignal.Vibrate:
			tempType=1;
			break;
		case TypeSignal.Lights_Vibrate:
			tempType=2;
			break;
		}
		return tempType;
	}
	
	public void NewLocalNotifications()
	{
		string[] tempstr=new string[2];
		string[] Texts;
		if (Application.systemLanguage == SystemLanguage.Russian) Texts=RusTexts;
		else Texts=EngTexts;
		switch (Texts.Length) {
		case 0:
			Texts = new string[3];
			Texts [0] = Application.productName;
			Texts [1] = Application.productName;
			Texts [2] = "Go to play!";
			break;
		case 1:
			tempstr [0] = Texts [0];
			Texts = new string[3];
			Texts [0] = tempstr [0];
			Texts [1] = Application.productName;
			Texts [2] = "Go to play!";
			break;
		case 2:
			tempstr [0] = Texts [0];
			tempstr [1] = Texts [1];
			Texts = new string[3];
			Texts [0] = tempstr [0];
			Texts [1] = tempstr [1];
			Texts [2] = "Go to play!";
			break;
		}
		#if !UNITY_EDITOR &&UNITY_ANDROID
		using (var pluginClass = new AndroidJavaClass ("com.aottanks.localnotifications.LocalNotificationReceiver")) {
			if (pluginClass != null) pluginClass.CallStatic ("ShowNotification", new object[] {Texts, PeriodNotifyInSeconds, typeSignal(),RealtimeSend});
		}
		#endif
	}
	
	void Start()
	{
		#if !UNITY_EDITOR &&UNITY_ANDROID
		NewLocalNotifications ();
		#endif
	}
}
