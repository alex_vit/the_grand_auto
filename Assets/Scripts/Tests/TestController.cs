﻿using UnityEngine;
using System.Collections;

//this is object for tests. Like a speed mission complete
public class TestController : MonoBehaviour {
	[SerializeField]
	bool isEditor;
	bool isDebuggable;
	// Use this for initialization
	void Awake(){
		CheckPlatform ();
	}

	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if (isEditor) {
			if(Input.GetKeyDown(KeyCode.F))
				CompleteCurrentMission();
			if(Input.GetKeyDown(KeyCode.G))
				RemoveAllMissionProgress();
		}
	}

	public void RemoveAllMissionProgress(){
		for(int i=0; i<StaticData.CountMissions; i++){
			StaticData.PD.CompletedMissions [i] = 0;
		}
		StaticData.PD.SaveOnDisk ();
		MissionController.Instance.Restart ();
	}

	public void CompleteCurrentMission(){
		int id = MissionController.Instance.GetCurrentMissionID ();
		MissionController.Instance.MissionCompleted ();
		//StaticData.PD.CompletedMissions [id] = 0;
		//StaticData.PD.SaveOnDisk ();
		//MissionController.Instance.Restart ();
	}

	void CheckPlatform(){
		#if UNITY_EDITOR
		isEditor = true;
		#endif
	}
}
