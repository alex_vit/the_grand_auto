using System;
using UnityEngine;

namespace UnityStandardAssets.Characters.ThirdPerson
{
    [RequireComponent(typeof (NavMeshAgent))]
    [RequireComponent(typeof (ThirdPersonCharacter))]
    public class AICharacterControl : MonoBehaviour
    {
        public NavMeshAgent agent { get; private set; } // the navmesh agent required for the path finding
        public ThirdPersonCharacter character { get; private set; } // the character we are controlling
        public Transform target; // target to aim for
		Vector3 zero=Vector3.zero;



		void Awake()
		{
			agent = GetComponent<NavMeshAgent>();
			character = GetComponent<ThirdPersonCharacter>();
			
			agent.updateRotation = false;
			agent.updatePosition = true;
			agent.stoppingDistance=(agent.radius)* transform.localScale.x+0.6f;
			agent.speed = 1.75f;
		}

        private void Update()
        {
			character.Move (agent.desiredVelocity, false, false);
			//if (target != null) character.Move (agent.desiredVelocity, false, false);
			//else character.Move(zero, false, false);
        }

		public bool EndPoint
		{
			get{return agent.remainingDistance<=agent.stoppingDistance;}
		}

		public void Stop()
		{
			agent.Stop ();
		}
		public void Move()
		{
			agent.Resume ();
		}


		public float StopingDistance
		{
			get{return agent.stoppingDistance;}
			set{if(!(value<0)) agent.stoppingDistance=value;}
		}

		public Transform Target {
			get{ return target;}
			set {if (value != null) {
					target = value;
					agent.SetDestination(target.position);
				}
			}
		}
    }
}
