﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Xml;

public class ItemInfo : XmlInfo {

	private int _x = 0;
	public int x{
		get {return _x;}
	}
	private int _y = 0;
	public int y{
		get {return _y;}
	}
	private int _level = 0;
	public int level{
		get {return _level;}
	}
	private int _rows = 0;
	public int rows{
		get {return _rows;}
	}
	private int _cols = 0;
	public int cols{
		get {return _cols;}
	}
	public ItemInfo() { }
	
	public ItemInfo(string assetName) : base(assetName) {	}
	
	public ItemInfo(XmlDocument xml) : base(xml){	}
	
	public override void Parse (XmlNode xml)
	{
		base.Parse (xml);

		_x = ParseInt (xml.Attributes.GetNamedItem ("x"));
		_y = ParseInt (xml.Attributes.GetNamedItem ("y"));
		_level = ParseInt (xml.Attributes.GetNamedItem ("level"));
		_rows = ParseInt (xml.Attributes.GetNamedItem ("rows"));
		_cols = ParseInt (xml.Attributes.GetNamedItem ("cols"));



	 
	}


}
