﻿using UnityEngine;
using System.Linq;
using System.Collections;
using System.Collections.Generic;

public class ObjectPull : MonoBehaviour {
	public static ObjectPull Instance; //ref for other classes
	public List<PullElement> pull;

	public Transform AICharStore;
	public Transform carStore;

	void Awake(){
		if (Instance == null)
			Instance = this;
		else
			Destroy (gameObject);

		pull = GetComponentsInChildren<PullElement> ().ToList();
		//Debug.Log ("pull count " + pull.Count);

		foreach (PullElement em in pull) {
			em.gameObject.SetActive (false);
		}

		SignAIChars ();
	}

	public bool IsEmpty(string tag){
		return pull.Where(x=> x.CompareTag(tag)).Count() == 0;
	}

	public PullElement GetObjectFromPull(string tag){
		List<PullElement> currList = pull.Where (x => x.CompareTag (tag)).ToList (); 
		PullElement obj = currList [Random.Range (0, currList.Count)];
		//Debug.Log ("Get from pull " + obj);

		obj.gameObject.SetActive (true);
		obj.transform.SetParent (transform);
		pull.Remove(obj);
		return obj;
	}

	public void ReturnObjectToPull(PullElement sender){
		//Debug.Log ("Return from pull " + sender);
		sender.transform.SetParent (transform);
		sender.gameObject.SetActive (false);
		pull.Add (sender);
	}

	void SignAIChars(){
		List<PullElement> currList = pull.Where (x => x.CompareTag ("AI")).ToList ();

		foreach (PullElement em in currList) {
			em.GetComponent<Healther> ().OnDeath += LifeController.Instance.CharDeath;
		}
	}
}
