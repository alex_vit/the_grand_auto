﻿using UnityEngine;
using System.Collections;

public class Player : MonoBehaviour {
	Transform selfTrans;
	// Use this for initialization
	void Awake(){
		selfTrans = transform;
	}

	void Start () {
		EnableOnDeath ();
	}

	public void DisableOnDeath()
	{
		GetComponent<Healther> ().OnDeath -= LifeController.Instance.CharDeath;
	}

	public void EnableOnDeath()
	{
		GetComponent<Healther> ().OnDeath += LifeController.Instance.CharDeath;
	}


	public void SetPlace(Transform place){
		selfTrans.position = new Vector3 (place.position.x, place.position.y, place.position.z);
	}
}
