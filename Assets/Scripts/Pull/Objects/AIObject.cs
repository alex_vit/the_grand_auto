﻿using UnityEngine;
using System.Collections;

public class AIObject : PullElement {
	Rigidbody selfRigid;
	Animator selfAnim;
	CapsuleCollider selfCollider;

	// Use this for initialization
	void Awake(){
		selfAnim = GetComponent<Animator> ();
		selfCollider = GetComponent<CapsuleCollider> ();
		selfRigid = GetComponent<Rigidbody> ();

		if(selfAnim)
			selfAnim.enabled = false;
		if(selfCollider)
			selfCollider.enabled = false;
		if(selfRigid)
			selfRigid.isKinematic = true;
	}

	public override void Die(){
		SetDefault ();
		ObjectPull.Instance.ReturnObjectToPull (this);
	}

	protected override void SetDefault(){
		if(selfAnim)
			selfAnim.enabled = false;
		if(selfCollider)
			selfCollider.enabled = false;
		if(selfRigid)
			selfRigid.isKinematic = true;

		if (currPoint != null)
			currPoint.isUsed = false;
	}

	public override void SetActive(){
		if (selfAnim) {
			selfAnim.enabled = true;
			selfAnim.Rebind ();
		}
		if(selfCollider)
			selfCollider.enabled = true;
		if(selfRigid)
			selfRigid.isKinematic = false;

		transform.rotation = new Quaternion ();
	}

	public override void BindSpawnPoint(SpawnArea area){
		currPoint = area;	
		currPoint.isUsed = true;
	}
}
