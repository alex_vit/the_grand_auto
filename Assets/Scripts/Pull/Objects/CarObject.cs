﻿using UnityEngine;
using System.Collections;

public class CarObject : PullElement {

	Rigidbody selfRigid;
	CapsuleCollider selfCollider;
	NavMeshAgent selfNavMesh;

	// Use this for initialization
	void Awake(){
		selfCollider = GetComponent<CapsuleCollider> ();
		selfRigid = GetComponent<Rigidbody> ();
		selfNavMesh = GetComponent<NavMeshAgent> ();

		if(selfCollider)
			selfCollider.enabled = false;
		if(selfRigid)
			selfRigid.isKinematic = true;
	}
	void Start () {

	}

	// Update is called once per frame
	void Update () {

	}

	public override void Die(){
		SetDefault ();
		ObjectPull.Instance.ReturnObjectToPull (this);
	}

	protected override void SetDefault(){
		if(selfCollider)
			selfCollider.enabled = false;
		if(selfRigid)
			selfRigid.isKinematic = true;
		if (selfNavMesh && selfNavMesh.enabled) {
			selfNavMesh.Stop ();
			selfNavMesh.enabled = false;
		}

		if (currPoint != null)
			currPoint.isUsed = false;
	}

	public override void SetActive(){
		if(selfCollider)
			selfCollider.enabled = true;
		if(selfRigid)
			selfRigid.isKinematic = false;
		//if (selfNavMesh)
			//selfNavMesh.enabled = true;
		/*if(selfRigid)
			selfRigid.useGravity = false;*/
	}

	public override void BindSpawnPoint(SpawnArea area){
		currPoint = area;	
		currPoint.isUsed = true;
	}
}
