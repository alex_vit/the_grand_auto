﻿using UnityEngine;
using System.Collections;

public class AdGenerator : MonoBehaviour {
	public static AdGenerator Instance;

	public float minTime = 15f;
	public float maxTime = 25f;

	float timer;

	bool isAdShown= false;
	// Use this for initialization
	void Awake(){
		if (Instance == null)
			Instance = this;
		else
			Destroy (gameObject);

		timer = 0f;
		isAdShown = false;
	}

	void Start () {
		if (!isAdShown) {
			StartCoroutine (ShowPeriodAd ());
		}
	}

	public void ShowCarAd(bool value){
		if (value) {
			//UserAdvertise.ShowFullScreenAd ();
			UserAdvertise.ShowInterstitialNoImage ();
		}
	}

	void ShowTimeAd(){
		UserAdvertise.ShowFullScreenAd ();	
	}

	public void ShowShopAd(){
		//UserAdvertise.ShowFullScreenAd ();
		UserAdvertise.ShowInterstitialNoImage ();
	}

	IEnumerator ShowPeriodAd(){
		float currTime = Random.Range (minTime, maxTime);
		isAdShown = true;

		while (true) {
			yield return new WaitForSeconds (currTime);
			UserAdvertise.ShowFullScreenAd ();
			break;
		}
	}
}
