﻿using UnityEngine;
using System.Collections;

public class MenuScript : MonoBehaviour {

	public bool SoundEnabled = true;

	public GameObject SoundButtonOn, SoundButtonOff;
	void Awake () 
	{
		//PlayerPrefs.DeleteAll();
		UserAdvertise.Init ();
		//Debug.Log ("aaaa");
		//UserAdvertise.ShowBanner(AdType.BANNER_BOTTOM);
		if(PlayerPrefs.HasKey("sound")){
			if(PlayerPrefs.GetInt("sound")==0)
			{
				SoundEnabled = false;
				AudioListener.volume = 0;
				
				SoundButtonOff.SetActive(true);
				SoundButtonOn.SetActive(false);
			}
			else
			{
				SoundEnabled = true;
				AudioListener.volume = 1;
				
				SoundButtonOff.SetActive(false);
				SoundButtonOn.SetActive(true);
			}
		}
		else
		{
			if(SoundEnabled)
			{
				PlayerPrefs.SetInt("sound", 1);
				AudioListener.volume = 1;
				
				SoundButtonOff.SetActive(false);
				SoundButtonOn.SetActive(true);
			}
			else
			{
				PlayerPrefs.SetInt("sound", 0);
				AudioListener.volume = 0;
				
				SoundButtonOff.SetActive(true);
				SoundButtonOn.SetActive(false);
			}
		}
	}

	void Start(){
		
	}

	public void SoundButton () 
	{
		SoundEnabled = !SoundEnabled;
		
		if (SoundEnabled) 
		{
			PlayerPrefs.SetInt("sound", 1);
			PlayerPrefs.Save();
			AudioListener.volume = 1;

			SoundButtonOff.SetActive(false);
			SoundButtonOn.SetActive(true);
	//		Debug.Log("a");
		} 
		if (!SoundEnabled)
		{
			PlayerPrefs.SetInt("sound", 0);
			PlayerPrefs.Save();
			AudioListener.volume = 0;

			SoundButtonOff.SetActive(true);
			SoundButtonOn.SetActive(false);
	//		Debug.Log("b");
		}
	}

	public void StartGame()
	{
		Application.LoadLevel(2);
	}
}
