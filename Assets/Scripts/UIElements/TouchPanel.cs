﻿using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;

public class TouchPanel : MonoBehaviour {
	public TouchPad moveTouchPad;
	public MoveTouchPad lookTouchPad;

	public void Show(){
		//moveTouchPad.Show ();
		lookTouchPad.Show ();
	}


	public void Hide(){
		//moveTouchPad.Hide ();
		lookTouchPad.Hide ();
	}
}
