﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class HealthIcon : MonoBehaviour {
	public Image healthBar;
	public Healther currTarget;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		healthBar.fillAmount = (float)currTarget.HP / (float)currTarget.Health;
	}


}
