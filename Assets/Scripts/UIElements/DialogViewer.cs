﻿using UnityEngine;
using System.Linq;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class DialogViewer : MonoBehaviour {
	public static DialogViewer Instance;
	public event EmptyEvent OnDialogWasShown;

	public Text upperText;


	WaitForSeconds waitForNextPhrase;
	Dictionary<SystemLanguage, List<DialogInfo>> MissionContent; 

	void Awake(){
		if (Instance == null)
			Instance = this;
		else
			Destroy (gameObject);
		waitForNextPhrase = new WaitForSeconds (5f);
		Init ();
	}

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void Init(){
		MissionContent = new Dictionary<SystemLanguage, List<DialogInfo>> ();
		//Dictionary<int, string[]> tmp = new Dictionary<int, string[]> ();
		//List<Dictionary<int,string[]>> tmpList = new List<Dictionary<int, string[]>> ();
		List<DialogInfo> dialogList = new List<DialogInfo>();

		//fill #1
		DialogInfo info = new DialogInfo();
		info.ID = 1;
		info.replics = new string[]{"Джейк, дружище, я рад, что ты наконец-то вернулся. дела обстоят сейчас не важно", 
									"Вкратце город поделен на несколько банд. У всех можно урвать кусок. Я наладил со всеми связи.",
									"Скоро мы займемся настоящим делом, но сначала тебе стоит отдохнуть",
									"Отправляйся на стоянку, что возле больницы, и забери 'подарочек', это тебе пригодится"};
		dialogList.Add (info);

		info = new DialogInfo();
		info.ID = 2;
		info.replics = new string[]{"Отлично, но без оружия ты долго здесь не продержишься", 
									"Есть один парень, зовут Мич. У него можно купить все что нужно",
									"Я отмечу тебе это место на карте",
									"Иди, и купи у этого пройдохи что нибудь"};

		dialogList.Add (info);

		info = new DialogInfo();
		info.ID = 3;
		info.replics = new string[]{"Неплохое приобретение, пора испытать тебя в деле. Я откопал имя того свидетеля, который тебя засадил", 
									"Вот его адрес, подумай как будет возможность. Когда закончишь, возвращайся ко мне, есть дело"};
		dialogList.Add (info);

		info = new DialogInfo();
		info.ID = 4;
		info.replics = new string[]{"Чистая работа, как в старые времена. А теперь о деле, здесь есть место которое приносит неплохие деньги.", 
									"Но его владелец уперся рогом и надеется на свою крышу. Нам нужно объяснить ему серьезность наших намерений",
									"Недавно он купил себе новую тачку, и паркует ее возле дома. У него будет повод подумать прежде чем отказывать нам",
									"Если она внезапно сгорит, или если ее изрешетят пули. Отправляйся туда, и уладь это дело"};
		dialogList.Add (info);

		info = new DialogInfo();
		info.ID = 5;
		info.replics = new string[]{"Отлично, он запаниковал, и просит помощи у своей крыши. Есть идея как избавиться от этой проблемы", 
									"Нил платит им каждый месяц, оставляет деньги в багажнике машины на старой стоянке",
									"Мой человечек сказал что за машинка это будет, неплохо бы ее отогнать в более уютное место",
									"Убьем двух зайцев, а заодно поднимем неплохую сумму. Чего ты еще ждешь?"};
		dialogList.Add (info);

		info = new DialogInfo ();
		info.ID = 6;
		info.replics = new string[] {"Все, толстяк лишился защиты, и нам он больше не нужен", 
									"Отправляйся к нему домой, и закончи начатое, надеюсь ты  еще помнишь адрес.",
									"Как уладишь, возвращайся, буду ждать"};

		dialogList.Add (info);

		info = new DialogInfo ();
		info.ID = 6;
		info.replics = new string[] {"Все, толстяк лишился защиты, и нам он больше не нужен", 
									 "Отправляйся к нему домой, и закончи начатое, надеюсь ты  еще помнишь адрес.",
									 "Как уладишь, возвращайся, буду ждать"};

		dialogList.Add (info);

		info = new DialogInfo ();
		info.ID = 7;
		info.replics = new string[] {"Отлично, теперь его точка наша, и она будет приносить доход. Ты много сделал для этого, поэтому ты в доле", 
									"Будет время, съезди посмотри, на новое приобретение."};
		dialogList.Add (info);

		info = new DialogInfo ();
		info.ID = 8;
		info.replics = new string[] {"Посмотри, твою точку хотят отбить, иди и покажи этим недотепам \"Кто здесь хозяин!\""}; 

		dialogList.Add (info);
		//info = new DialogInfo ();
		//info.ID = 2;
		//info.replics = new string[]{"} 
		MissionContent.Add (SystemLanguage.Russian, dialogList);

	}

	public void Show(MissionInfo mission){
		GameMenu.Instance.state = GameMenu.State.Pause;
		GameMenu.Instance.pausePanel.SetActive (false);
		GameMenu.Instance.dialogPanel.SetActive (true);

		//Debug.Log ("mission.ID " + mission.ID);
		DialogInfo currInfo = MissionContent [SystemLanguage.Russian].Find (x => x.ID == mission.ID);
		StartCoroutine(ShowDialog(currInfo.replics));
	}

	public void Hide(){
		
	}

	IEnumerator ShowDialog(string[] text){
		int currIndex = 0;
		while (true) {
			if (currIndex == text.Length) {
				//dialog was shown all
				//Debug.Log("on dialog shown");
				if (OnDialogWasShown != null)
					OnDialogWasShown ();
				GameMenu.Instance.OnBack();
				break;
			}
			upperText.text = text [currIndex];
			currIndex++;
			yield return waitForNextPhrase;
		}
	}
}

public struct DialogInfo{
	public int ID;
	public string[] replics;
}