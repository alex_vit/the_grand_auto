﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class MoneyUILabel : MonoBehaviour {
	public Text moneyLabelText;
	//int _money = 0;
	public MoneyOwner player;
	int currMoney;

	void Awake(){
		//get saved money
		//Cash = 0;
	}

	// Use this for initialization
	void Start () {

	}

	// Update is called once per frame
	void Update () {

	}

	void OnEnable(){
		player.OnValueChanged += ChangeValue;
	}

	void OnDisable(){
		if(enabled)
			player.OnValueChanged -= ChangeValue;
	}

	void ChangeValue(int value){
		StopAllCoroutines ();
		if (value > currMoney)
			StartCoroutine (IncreaseCash (value));
		else
			StartCoroutine (DecreaseCash (value));
	}

	int CalculateStep(int value){
		Debug.Log ("cash value :" + value);
		value = Mathf.Abs (value);
		if (value / 10000 >= 1) 
			return (int)CashStep.THOUSANDS;
		if (value / 1000 >= 1)
			return (int)CashStep.HUNDREDS;
		if (value / 100 >= 1)
			return (int)CashStep.DECIMAL;
		if (value / 10 >= 1)
			return (int)CashStep.ONES;
		else
			return 0;  	
	}

	IEnumerator IncreaseCash(int currCash){
		int step = CalculateStep (currMoney - currCash);
		while (true) {
			if (currMoney >= currCash) {
				currMoney = currCash;
				moneyLabelText.text = currMoney.ToString ();
				break;
			}
			currMoney += step;
			moneyLabelText.text = currMoney.ToString ();
			yield return new WaitForEndOfFrame ();
		}
	}


	IEnumerator DecreaseCash(int currCash){
		int step = CalculateStep (currMoney - currCash);
		while (true) {
			if (currMoney <= currCash) {
				currMoney = currCash;
				moneyLabelText.text = currMoney.ToString ();
				break;
			}
			currMoney -= step;
			moneyLabelText.text = currMoney.ToString ();
			yield return new WaitForEndOfFrame ();
		}
	}

	enum CashStep{
		ONES = 1,
		DECIMAL = 10,
		HUNDREDS = 100,
		THOUSANDS = 1000
	}
}
