﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class FadeText : MonoBehaviour {
	public string Text{
		get{ 
			return _text.text;
		}set{ 
			_text.text = value;
		}
	}

	Text _text;
	void Awake(){
		_text = GetComponent<Text> ();
		_text.gameObject.SetActive (false);
	}


	public void Show(){
		_text.gameObject.SetActive (true);		
	}

	public void Hide(){
		gameObject.SetActive (false);
	}
}
