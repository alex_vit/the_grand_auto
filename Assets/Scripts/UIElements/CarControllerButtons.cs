﻿using UnityEngine;
using System.Collections;

public class CarControllerButtons : MonoBehaviour {
	Transform selfTrans;
	int childCount;
	// Use this for initialization
	void Awake(){
		selfTrans = transform;
		childCount = selfTrans.childCount;
	}

	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void Show(){
		for (int i = 0; i < childCount; i++) {
			selfTrans.GetChild (i).gameObject.SetActive (true);
		}
	}

	public void Hide(){
		for (int i = 0; i < childCount; i++) {
			selfTrans.GetChild (i).gameObject.SetActive (false);
		}
	}
}
