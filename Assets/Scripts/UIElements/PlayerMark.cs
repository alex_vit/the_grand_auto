﻿using UnityEngine;
using System.Collections;

public class PlayerMark : MonoBehaviour {
	public Transform miniMapCamera;
	public Transform currTarget;
	public Transform icon;

	Transform selfTrans;
	Vector2 renderPos;
	float rad;
	// Use this for initialization
	void Awake(){
		selfTrans = transform;
	}
	void Start () {
		Init ();
	}
	
	// Update is called once per frame
	void Update () {
		selfTrans.position = new Vector3 (miniMapCamera.position.x, selfTrans.position.y, miniMapCamera.position.z);
		if (currTarget)
			icon.localRotation = Quaternion.Euler (90f, currTarget.rotation.eulerAngles.y, 0f);
	}

	void Init(){
		rad = miniMapCamera.GetComponent<Camera> ().orthographicSize/2;
	}

	public void SetTarget(Transform target){
		currTarget = target;
	}
}
