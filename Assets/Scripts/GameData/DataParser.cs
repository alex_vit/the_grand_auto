﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System;

public class DataParser
{
	const char outSeparanor = ';';
	const char innerSeparanor = '|';
	public static string WriteIntMass (int[] incomeMass)
	{
		StringBuilder tmpString = new StringBuilder ();
		foreach (var _value in incomeMass) {
			tmpString.Append (_value);
			tmpString.Append (innerSeparanor);
		}
		tmpString.Remove (tmpString.Length - 1, 1);
		return tmpString.ToString ();
	}
	public static int[] ReadIntMass (string inputString)
	{
		string[] stringMass = inputString.Split (innerSeparanor);
		int[] tmpMass = new int[stringMass.Length];
		//string[] tmpString;
		for (int i = 0; i < stringMass.Length; i++) {
			tmpMass [i] = Convert.ToInt32 (stringMass [i]);
		}
		return tmpMass;
	}
	/*public static string WriteFloatMass (float[] incomeMass)
	{
		StringBuilder tmpString = new StringBuilder ();
		foreach (var _value in incomeMass) {
			tmpString.Append (_value);
			tmpString.Append (innerSeparanor);
		}
		tmpString.Remove (tmpString.Length - 1, 1);
		return tmpString.ToString ();
	}*/
	public static float[] ReadFloatMass (string inputString)
	{
		string[] stringMass = inputString.Split (innerSeparanor);
		float[] tmpMass = new float[stringMass.Length];
		//string[] tmpString;
		for (int i = 0; i < stringMass.Length; i++) {
			tmpMass [i] = Convert.ToSingle (stringMass [i]);
		}
		return tmpMass;
	}



	
}
