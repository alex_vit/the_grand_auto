﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public static class StaticData {

	public static Dictionary<string, GameObject> Prefabs;
	public static Dictionary<string, Material> Materials;
	public static Dictionary<string, Sprite> Sprites;
	public static Dictionary<string, Texture> Textures;
	public static PlayerData PD=new PlayerData();
	public static bool isInit = false;

	public static int CountMissions=10;
	public static int CountWeapons=4;
	public static int ObjectsCount = 3;

	public static int[] MaxCountAmmoWeapons;
	public static int[] AddCountAmmo;

	public struct PlayerData
	{
		public int[] CompletedMissions;
		public int[] OpenWeapons;
		public int[] CountAmmoWeapons;
		public int[] AvailableObjects;

		public int Vibration;
		public float Sound,Music;
		int money;

		public int Money
		{
			get{return money;}
			set{
				if(value<9999999) money=value;
				else if(value<0) money=0;
				else money=9999999;
				PlayerPrefs.SetInt ("Money",Money);
				//ChangePlayerPrefs.Instance.IsChange=true;
			}
		}
		public void Load()
		{
			int i = 0;
			if (PlayerPrefs.HasKey ("Missions"))
				PD.CompletedMissions = DataParser.ReadIntMass (PlayerPrefs.GetString ("Missions"));
			else {
				PD.CompletedMissions=new int[CountMissions];
				for(i=0;i<CountMissions;i++) 
					PD.CompletedMissions[i]=0;
			}

			if (PlayerPrefs.HasKey ("OpenObjects")) {
				PD.AvailableObjects = DataParser.ReadIntMass (PlayerPrefs.GetString ("OpenObjects"));
				PD.AvailableObjects [0] = 1; //hospital always available
			}
			else {
				PD.AvailableObjects = new int[ObjectsCount];
				for(i=0;i<ObjectsCount;i++) 
					PD.AvailableObjects[i]=0;
				PD.AvailableObjects [0] = 1;
			}


			if (PlayerPrefs.HasKey ("CountAmmoWeapons")) {
				PD.CountAmmoWeapons = DataParser.ReadIntMass (PlayerPrefs.GetString ("CountAmmoWeapons"));
				for(i=0;i<CountWeapons;i++) 
					if(PD.CountAmmoWeapons[i]>MaxCountAmmoWeapons[i]) 
						PD.CountAmmoWeapons[i]=MaxCountAmmoWeapons[i];
				else if(PD.CountAmmoWeapons[i]<0) PD.CountAmmoWeapons[i]=0;
			}
			else {
				PD.CountAmmoWeapons=new int[CountWeapons];
				for(i=0;i<CountWeapons;i++) PD.CountAmmoWeapons[i]=0;
			}

			if (PlayerPrefs.HasKey ("OpenWeapons")) {
				PD.OpenWeapons = DataParser.ReadIntMass (PlayerPrefs.GetString ("OpenWeapons"));
			}
			else {
				PD.OpenWeapons=new int[CountWeapons];
				for(i=0;i<CountWeapons;i++) PD.OpenWeapons[i]=0;
			}
				
			if (PlayerPrefs.HasKey ("Money"))
				PD.Money = PlayerPrefs.GetInt ("Money");
			else
				PD.Money = 0;


			if (PlayerPrefs.HasKey ("Sound"))
				PD.Sound = PlayerPrefs.GetFloat ("Sound");
			else
				PD.Sound = 1f;

			if (PlayerPrefs.HasKey ("Music"))
				PD.Music = PlayerPrefs.GetFloat ("Music");
			else
				PD.Music = 1f;
		}

		public void SaveAll()
		{
			PlayerPrefs.SetInt ("Money",Money);
			PlayerPrefs.SetFloat ("Sound",Sound);
			PlayerPrefs.SetString ("Missions",DataParser.WriteIntMass(CompletedMissions));
			PlayerPrefs.SetString ("CountAmmoWeapons",DataParser.WriteIntMass(CountAmmoWeapons));
			PlayerPrefs.SetString ("OpenWeapons", DataParser.WriteIntMass (OpenWeapons));
			PlayerPrefs.SetString ("OpenObjects", DataParser.WriteIntMass (AvailableObjects));


		}

		public void SaveOnDisk()
		{
			PlayerPrefs.Save ();
		}
	}

	public static void Init()
	{
		InitData ();
		PD.Load ();
		//InitOther ();
		InitPrefabs ();
		//InitMaterials ();
		//InitMaterials ();
		//InitTextures ();
		//InitPointsGame ();
		//InitSprites ();

		isInit = true;
	}

	static void InitData()
	{
		AddCountAmmo=new int[CountWeapons];
		AddCountAmmo [0] = 34;
		AddCountAmmo [1] = 16;
		AddCountAmmo [2] = 60;
		AddCountAmmo [3] = 10;
		MaxCountAmmoWeapons = new int[CountWeapons];
		MaxCountAmmoWeapons [0] = 1003;
		MaxCountAmmoWeapons [1] = 1000;
		MaxCountAmmoWeapons [2] = 1020;
		MaxCountAmmoWeapons [3] = 1000;

	}

	static void InitOther()
	{
	}

	static void InitPrefabs()
	{
		Prefabs = new Dictionary<string, GameObject> ();
		Prefabs.Add ("AIUnit", LoadPrefab ("Prefab/AIUnit"));
		Prefabs.Add ("SliderHP", LoadPrefab ("Prefab/SliderHP"));
		Prefabs.Add ("MiniGame", LoadPrefab ("Prefab/MiniGame"));
	}
	static void InitMaterials()
	{
		Materials = new Dictionary<string, Material> ();
	}

	static void InitTextures()
	{
		Textures = new Dictionary<string, Texture> ();
	}

	static void InitSprites()
	{
		Sprites = new Dictionary<string, Sprite> ();
	}

	static void InitPointsGame()
	{
	}
	
	static GameObject LoadPrefab (string name)
	{
		return Resources.Load (name) as GameObject;
	}
}

public enum ObjectsInGame{
	GUN_STORE = 1,
	REALTY = 2
}
