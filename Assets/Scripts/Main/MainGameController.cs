﻿using UnityEngine;
using System.Collections;

public sealed class MainGameController{
	static MainGameController _instance = null;
	public GameState State;

	public MainGameController(){
		State = GameState.WALK;
	}

	public static MainGameController Instance {
		get { 
			if (_instance == null) {
				_instance = new MainGameController ();
			}
			return _instance;
		}
		private set{ }
	}
	// Use this for initialization

}

public enum GameState{
	MINI_GAME, //current state is Mini missions playing;
 	WALK //current state is walk in 3D world
}
