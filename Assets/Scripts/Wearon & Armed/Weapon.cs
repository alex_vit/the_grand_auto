﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public class Weapon : MonoBehaviour {
	//public int ID;
	public WearonType type;
	public Camera GunCamera;
	public PropertiesWeapon PW;
	List<string> TagsTarget;


	//Transform selfTRans;
	RaycastHit hit;
	//Ray tempRay;
	Collider tempCol;


	public void Init(PropertiesWeapon pw)
	{
		if (pw != null) {
			PW = pw;
			PW.ActivePW ();
		}
	}

	public void AcceptChange()
	{
		PW.ActivePW ();
	}

	public void ActiveWeapon()
	{
		GunCamera.enabled = true;
	}
	public void InActiveWeapon()
	{
		GunCamera.enabled = false;
	}

	public Camera SetGunCam
	{
		set{ GunCamera = value;}
	}

	public WearonType GetType(){
		return type;
	}
		
	public int Accuracy=50;
	public void MakeAShot(Ray ray){
		if (PW.Shot) {
			if (Physics.Raycast (ray, out hit, PW.MaxDistance)) {
				tempCol=hit.collider;
				if (tempCol.CompareTag ("AI")) {
					if(UnityEngine.Random.Range(0,101)>=Accuracy) tempCol.GetComponent<Healther> ().TakeDamage (PW.DamageInBody);
				
				} else if (tempCol.GetComponentInParent<CarLife>()) {
						if(UnityEngine.Random.Range(0,101)>=Accuracy) 
							tempCol.GetComponentInParent<CarLife> ().SetDamage (20);
				}
				
			}
		}
	}

	public void MakeAShot(Healther targetShot){
		if (targetShot.isLived) {
			if (PW.Shot) {
				if (UnityEngine.Random.Range (0, 101) >= Accuracy)
					targetShot.TakeDamage (PW.DamageInOther);
			}
		}
	}

	/*public void MakeAShot(){
		if (PW.Shot) {
			tempRay = CurrCam.ScreenPointToRay (PosCenterCam);
			if (Physics.Raycast (tempRay, out hit, PW.MaxDistance)) {
				if (hit.collider.CompareTag("AI")) {
					hit.collider.GetComponent<Healther> ().TakeDamage (PW.DamageInBody);
				}else if (hit.collider.CompareTag ("Car")) {
					if(hit.collider.GetComponent<CarLife>())
						hit.collider.GetComponent<CarLife> ().SetDamage (100);
				}
				
			}
		}
	}*/

	public void MakeAQueueShots(){
		
	}
}

public enum WearonType{
	ONE_HANDED,
	TWO_HANDED,
	SPECIFIC
}
