﻿using UnityEngine;
using System.Collections;
using System;

public class PropertiesWeapon : MonoBehaviour {

	public AudioSource AS_Shot;
	public AudioSource AS_Reload;
	public AudioSource AS_Empty;
	public float MaxDistance;
	public int CurrentCountAmmoInCage;
	public int MaxCountAmmoInCage;
	public int CountAmmo;
	public int MaxCountAmmo;
	public bool OneShotTime;
	public bool EnableAudio;
	public Action<int> LabelsValue,ChangeGlobalState;//,ActionAfterReload,ActionBeforeShot,ActionAfterShot;
	public float ReloadTime;
	public float BetweenShots;
	public ControlPS CPS;
	public int ID;
	public float DamageInBody=3f;
	public float DamageInOther=1f;
	YieldInstruction YI_ttbs,YI_ttra;
	bool isInit=false;
	enum StateGun
	{
		ready,
		reload,
		betweenshots,
		shot,
		empty
	}
	StateGun SG;

	public int OverAllAmmo
	{
		get{ return CountAmmo + CurrentCountAmmoInCage;}
	}

	public bool CheckStateGun{
		get{
			if(SG==StateGun.ready){
				if(CPS!=null) CPS.Active();
				PlayShot ();
				if(ChangeGlobalState!=null) ChangeGlobalState(ID);
				CurCountAmmoInCage--;
				StartCoroutine ("TimeToBetweenShot");
				return true;
			}
			else{
				if(SG==StateGun.empty) PlayEmpty ();
			}
			return false;
		}
	}

	public int ChangeCountAmmo
	{
		get{return CountAmmo;}
		set{
			if(value>MaxCountAmmo) CountAmmo=MaxCountAmmo;
			else if(value<0) CountAmmo=0;
			else CountAmmo=value;
			ChangeValueLabels();
			//if () StartCoroutine ("ReloadAmmo");

		}
	}

	public void ActivePW()
	{
		StopAllCoroutines ();
		if (SG != StateGun.ready) {
			if (IsEmptyCage){
				if(!IsEmpty) StartCoroutine ("ReloadAmmo");
				else SG = StateGun.empty;
			}
			else
				SG = StateGun.ready;
		}
		ChangeValueLabels ();
		//if (SG == StateGun.ready) ChangeValueLabels ();
	}
	public void ChangeValueLabels()
	{
		if(LabelsValue!=null) LabelsValue(ID);
	}

	public void Init()
	{
		if (!isInit) {
			if(CountAmmo>0) GunReloadAmmo ();
			else SG=StateGun.empty;
			YI_ttbs = new WaitForSeconds (BetweenShots);
			YI_ttra = new WaitForSeconds (ReloadTime);
			isInit=true;
			//gameObject.SetActive (false);
		}
	}


	public int CurCountAmmoInCage
	{
		get{return CurrentCountAmmoInCage;}
		set{
			CurrentCountAmmoInCage=value;
			ChangeValueLabels();
		}
	}

	public void PlayShot()
	{
		if(EnableAudio){
			if(AS_Shot!=null){
				if (AS_Shot.isPlaying) AS_Shot.Stop ();
				AS_Shot.Play ();
			}
		}
	}
	public void PlayReload()
	{
		if(EnableAudio){
			if(AS_Reload!=null){
				if (AS_Reload.isPlaying) AS_Reload.Stop ();
				AS_Reload.Play ();
			}
		}
	}
	public void PlayEmpty()
	{
		if(EnableAudio){
			if(AS_Empty!=null){
				if (AS_Empty.isPlaying) AS_Empty.Stop ();
				AS_Empty.Play ();
			}
		}
	}
	
	public bool IsEmpty
	{
		get{return CountAmmo <= 0;}
	}
	public bool IsEmptyCage
	{
		get{ return CurrentCountAmmoInCage <= 0;}
	}
	
	public bool Shot
	{
		get{return CheckStateGun;}
	}

	public bool FullAmmo
	{
		get{ return CountAmmo == MaxCountAmmo;}
	}

	public void GunReloadAmmo()
	{
		if (CountAmmo < MaxCountAmmoInCage)
			CurCountAmmoInCage = CountAmmo;
		else if (CountAmmo < 0)
			CurCountAmmoInCage = 0;
			else CurCountAmmoInCage=MaxCountAmmoInCage;
		CountAmmo-=CurrentCountAmmoInCage;
		ChangeValueLabels();
		if(CurCountAmmoInCage==0) SG = StateGun.empty;
		else SG = StateGun.ready;
	}

	IEnumerator ReloadAmmo()
	{
		PlayReload ();
		SG = StateGun.reload;
		yield return YI_ttra;
		GunReloadAmmo ();
		StopCoroutine ("ReloadAmmo");
	}
	
	IEnumerator TimeToBetweenShot()
	{
		SG = StateGun.betweenshots;
		yield return YI_ttbs;
		if (IsEmptyCage) {
			if(IsEmpty) SG = StateGun.empty;
			else StartCoroutine ("ReloadAmmo");
		}
		else SG = StateGun.ready;
		StopCoroutine ("TimeToBetweenShot");
	}

}
