﻿using UnityEngine;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityStandardAssets.Characters.ThirdPerson;

public class Damager : MonoBehaviour {
	public event GameObjectEvent OnTargetDefined;

	bool fightMode;

	public bool FightMode{
		get{ 
			return fightMode;
		}
		set{ 
			fightMode = value;
		}
	}

	bool isPlayer;
	float speed = 0.3f;

	Animator anim;
	ThirdPersonCharacter character;

	Transform targetPos;
	Rigidbody target;

	Transform selfTrans;
	List<Collider> cldrs;

	void Awake(){
		anim = GetComponent<Animator> ();
		character = GetComponent<ThirdPersonCharacter> ();
		selfTrans = transform;

		isPlayer = gameObject.CompareTag ("Player");
	}

	void FixedUpdate(){
		DetectEnemy ();	
	}

	void SetTarget(Transform t){
		targetPos = t;
	}

	void SetTarget(GameObject obj){
		
		target = obj.GetComponent<Rigidbody>();
		targetPos = obj.transform;
		Debug.Log ("set target " + target);
	}

	void DetectEnemy(){
		if (fightMode) {
			//if it's AI
			if (!isPlayer) {
				//find the Player and do attack him
				if (target == null) {
					//if 

					cldrs = Physics.OverlapSphere (selfTrans.position, 5).ToList ();

					foreach (Collider cldr in cldrs) {
						if (cldr.transform.root.CompareTag ("Player")) {
							//cldr.transform.root.GetComponent<AIChar>().FigthingOn();
							SetTarget (cldr.gameObject);
							selfTrans.LookAt (cldr.gameObject.transform);
							Vector3 v = targetPos.position - selfTrans.position;
							v.y = 0;

							character.Move (v.normalized * speed, false, false);
						}
					}
				} else {
					if (Vector3.Distance (targetPos.position, selfTrans.position) < 1.5f && target.GetComponent<Healther> ().isLived) {
						character.Attake (1);
						//Debug.Log ("MAKE ATTACK");
						//target.GetComponent<Healther> ().TakeDamage (2);
					} else {
						//Debug.Log ("STOP ATTACK");
						character.Attake (0);
						target = null;
						FightMode = false;
					}
				}
			}else{
				//Debug.Log ("find AI target");
				//if it's player, find nearest AI and turn on him fightMode
				//Debug.Log (target);
				if (target == null) {
					cldrs = Physics.OverlapSphere (selfTrans.position, 5).ToList ();
					foreach (Collider cldr in cldrs) {
						if (cldr.transform.CompareTag ("AI") || cldr.gameObject.CompareTag("Victim")) {
							if (Vector3.Distance (cldr.transform.position, selfTrans.position) < 2) {
								SetTarget (cldr.transform.gameObject);
								cldr.transform.GetComponent<Damager> ().FightMode = true;
							}
						}
					}
				} else {
					if (!target.GetComponent<Healther> ().isLived) {
						target = null;
						FightMode = false;
					}
				}
			
			}
		}
	}

	public void FightOn(){
		FightMode = true;
	}

	public void MakeUdar()
	{
		//print ("Udar");

		/*RaycastHit rh;
		//Debug.DrawRay (transform.position + Vector3.up, transform.forward);
		if (Physics.Raycast (transform.position + Vector3.up, transform.forward, out rh, 1)) {


			if(rh.rigidbody != null)
			{

				//if(rh.rigidbody.GetComponent<CharLife>())
				//rh.rigidbody.GetComponent<CharLife>().SetDamage(2);

				if(rh.rigidbody == null)
				if (Physics.Raycast (transform.position + Vector3.up, transform.forward, out rh, 1.5f) && rh.rigidbody != null)
					rh.rigidbody.AddForce ((transform.forward + Vector3.up *1.0f).normalized * 15000);

				//print ("Udar");

			}

		}*/

		if (target != null && Vector3.Distance (targetPos.position, selfTrans.position) < 1.5f) {
			target.GetComponent<Healther> ().TakeDamage (2);
			//target.GetComponent<Rigidbody> ().AddForce ((transform.forward + Vector3.up * 1.0f).normalized * 15000);
		}
	}
}
