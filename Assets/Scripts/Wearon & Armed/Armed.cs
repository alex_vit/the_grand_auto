using UnityEngine;
using System.Linq;
using System.Collections;
using System.Collections.Generic;

public class Armed : MonoBehaviour {
	public event GameObjectEvent OnWearonChanged;

	List<Weapon> wearonInArms;
	public Weapon  currentWearon;

	public Transform rightHand;
	GameObject leftHand;
	int currIndex;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	   
	public void TakeGun(){
		
		if(currentWearon != null){
			
			//currentWearon.transform.SetParent (currentWearon.transform);
			switch (currentWearon.GetType ()) 
			{
			case WearonType.TWO_HANDED:
				Debug.Log ("aaa");
				currentWearon.gameObject.SetActive (true);
				currentWearon.transform.SetParent (rightHand.transform);
				currentWearon.transform.localPosition = new Vector3 (0.21f, -0.327f, 0.194f);
				currentWearon.transform.localEulerAngles = new Vector3 (18, 218, 103);
				break;
			}
				
		}
	}

	void SelectNextWearon(){
		currIndex = (wearonInArms.IndexOf (currentWearon)) + 1;
		currentWearon = wearonInArms[Mathf.Clamp(currIndex, 0,wearonInArms.Count-1)];
	}

	void SelectPrevWearon(){
		currIndex = (wearonInArms.IndexOf (currentWearon)) - 1;
		currentWearon = wearonInArms[Mathf.Clamp(currIndex, 0,wearonInArms.Count-1)];
	}

	void AddWearon(GameObject sender){
		Weapon t = sender.GetComponent<Weapon> ();

		if (t != null && !wearonInArms.Contains (t))
			wearonInArms.Add (t);
	}

	void RemoveWearon(GameObject sender){
		Weapon t = sender.GetComponent<Weapon> ();

		if (t != null && !wearonInArms.Contains (t))
			wearonInArms.Remove (t);
	}

	void RemoveAllWearon(){
		wearonInArms.Clear ();
	}
}
