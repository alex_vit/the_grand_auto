﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ChooseWeapon : MonoBehaviour {

	public static ChooseWeapon Instance;
	public GameObject[] ListWeapons;
	public Image[] ImageWeapons;
	public PropertiesWeapon[] PropWeapons;
	public Weapon WPlayer;
	public GameObject[] Guns3D;
	public Text[] LabelCountAmmoChooseGun;
	public Image Weapon;
	public Sprite Hands;
	public GameObject ButtonAttack;
	public GameObject ButtonAttackHands;
	public GameObject[] ButtonsChooseGun;
	public CameraSwitcher CS;
	int activeWeapon;
	Color32 colorWhite = new Color32 (255, 255, 255, 255);
	Color32 colorGreen =  new Color32 (0, 255, 0, 255);
	public bool ActiveWeaponNoHands
	{
		get{return activeWeapon!=-1;}
		//set{if(activeWeapon!=value) SwitchWeapon(value);}
	}

	public int ActiveWeapon
	{
		get{return activeWeapon;}
		//set{if(activeWeapon!=value) SwitchWeapon(value);}
	}

	public void ViewButtonAttack()
	{
		if (activeWeapon != -1)
			ButtonAttack.SetActive (true);
		else ButtonAttackHands.SetActive (false);
	}

	public void HideButtonAttack()
	{
		ButtonAttack.SetActive (false);
		if(activeWeapon==-1) ButtonAttackHands.SetActive (true);
	}

	void Awake()
	{
		Instance = this;
	}



	public void Init()
	{
		ImageWeapons [0].color = colorGreen;
		activeWeapon = -1;
		GameController.Instance.LabelCountAmmoInGame.enabled=false;
		Weapon.sprite = Hands;
		ButtonAttack.SetActive (false);
		ButtonAttackHands.SetActive (true);

		for (int i = 0; i < StaticData.CountWeapons; i++)
			if (StaticData.PD.OpenWeapons [i] == 1)
				ButtonsChooseGun [i].SetActive (true);
			else 
				ButtonsChooseGun [i].SetActive (false);
	}

	public void UpdateWeapons(){
		for (int i = 0; i < StaticData.CountWeapons; i++)
			if (StaticData.PD.OpenWeapons [i] == 1)
				ButtonsChooseGun [i].SetActive (true);
			else 
				ButtonsChooseGun [i].SetActive (false);
	}

	void ActiveHands()
	{
		ListWeapons [activeWeapon].SetActive (false);
		Guns3D [activeWeapon].SetActive (false);
		ImageWeapons [activeWeapon+1].color = colorWhite;
		activeWeapon = -1;
		ImageWeapons [0].color = colorGreen;
		Weapon.sprite = Hands;
		ButtonAttack.SetActive (false);
		ButtonAttackHands.SetActive (true);
		GameController.Instance.LabelCountAmmoInGame.enabled=false;
	}
	
	public void SwitchWeapon(int idGun)
	{
		if (idGun != activeWeapon) {
			if (idGun == -1) {
				ActiveHands();
			} else {
				if (activeWeapon != -1) {
					Guns3D [activeWeapon].SetActive (false);
					ListWeapons [activeWeapon].SetActive (false);
					ImageWeapons [activeWeapon+1].color = colorWhite;
				}
				else
				{
					ImageWeapons [0].color = colorWhite;
				}
				if (CS.IsPersonCamera)
					ButtonAttack.SetActive (true);
				ButtonAttackHands.SetActive (false);
				Weapon.sprite = StoreGun.Instance.AllGun [idGun];
				GameController.Instance.LabelCountAmmoInGame.enabled=true;
				Guns3D [idGun].SetActive (true);
				activeWeapon = idGun;
				ImageWeapons [activeWeapon + 1].color = colorGreen;
				ListWeapons [activeWeapon].SetActive (true);
				if (PropWeapons [activeWeapon] != null)
					WPlayer.Init (PropWeapons [activeWeapon]);
			}
		}
	}

	public void ChangeImageGunInGame()
	{
		if (activeWeapon == -1)
			Weapon.sprite = Hands;
		else Weapon.sprite=StoreGun.Instance.AllGun [activeWeapon];
	}

}
