﻿using UnityEngine;
using System.Collections;

public class ControlPS : MonoBehaviour {


	public ParticleSystem PS;
	//public AudioSource[] AS;
	int tempInt;
	float timeLifePS,tempTLPS,timeLifeAS,tempTLAS,fNull=0f,fixTime;
	bool isStopPS,isStopAS,bFalse=false;
	GameObject thisGO;

	void Awake()
	{
		if (PS == null) PS = GetComponent<ParticleSystem> ();
		if(PS != null) timeLifePS = PS.duration;
		else timeLifePS = 0f;
		if(thisGO==null) thisGO = gameObject;
		if (transform.childCount != 0) {
			ParticleSystem tempPS;
			Transform temptrans = transform;
			for (int i=0; i<temptrans.childCount; i++) {
				tempPS = temptrans.GetChild (i).GetComponent<ParticleSystem> ();
				if (tempPS != null) if (tempPS.duration > timeLifePS) timeLifePS = tempPS.duration;
			}
		}
		fixTime = Time.fixedDeltaTime;
	}

	public void Active()
	{
		if (thisGO == null) gameObject.SetActive (true);
		else thisGO.SetActive (true);
	}

	void OnEnable()
	{
		tempTLPS = 0f;
		//timeLifeAS = fNull;
		isStopPS = false;
		//isStopAS = bFalse;
		if(PS!=null) PS.Play ();
		/*if (AS.Length!=0) {
			tempInt=UnityEngine.Random.Range(0,AS.Length);
			timeLifeAS = AS[tempInt].clip.length;
			AS[tempInt].Stop();
			AS[tempInt].Play();
		}*/
	}

	void FixedUpdate()
	{
		if (!isStopPS) {
			if (PS != null)
				if (tempTLPS < timeLifePS)
					tempTLPS += fixTime;
			else if (!isStopPS)
				isStopPS = true;
		}
		/*if (!isStopAS) {
			if (AS.Length != 0)
				if (tempTLAS < timeLifeAS)
					tempTLAS += Time.fixedDeltaTime;
			else if (!isStopAS)
				isStopAS = true;
		}*/
	}

	void Update () {
		if (isStopPS) gameObject.SetActive (false);
	}
}
