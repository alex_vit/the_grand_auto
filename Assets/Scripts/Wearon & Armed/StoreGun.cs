﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class StoreGun : MonoBehaviour {

	public static StoreGun Instance;
	public GameObject RightButton,LeftButton;
	public Text LabelCostModel;
	//public Text[] LabelCountAmmoChooseGun;
	//public Text LabelCountAmmoInGame;
	public Text Say;
	public GameObject[] Guns;
	public GameObject ButtonBuy;
	//public Text LabelMoneyInGame;
	public PropertiesWeapon[] PW;
	public Image CurrentGunInStore;
	public Sprite[] AllGun;
	public GameObject StoreUI;
	public EntryStoreGun EntrySG;
	public Button Weapon;
	public DetectPlayerAboutStore DPAS;
	public Transform BackPointStore;
	public Image SwitchCam;
	public GameObject PauseButton;

	public Money playerMoney;
	public GameObject ButtonAttackHands;
	public Dictionary<int,PropertiesWeapon> PlayerWeapons=new Dictionary<int,PropertiesWeapon>();


	int currentSkin=0;
	int[] OpenModels;
	int[] costModel;
	string moneyS;
	int i;

	#if UNITY_EDITOR
	void Update()
	{
		if (Input.GetKeyDown (KeyCode.C))
			//PlayerPrefs.DeleteAll ();
		if (Input.GetKeyDown (KeyCode.M)) {
			StaticData.PD.Money+=100000;
		}
	}
	#endif
	
	void Awake()
	{
		Instance = this;
	}

	void ChangeLabelsValue(int id)
	{
		GameController.Instance.LabelCountAmmoInGame.text=PlayerWeapons[id].CurCountAmmoInCage+"/"+PlayerWeapons[id].CountAmmo;
		ChooseWeapon.Instance.LabelCountAmmoChooseGun [id].text = GameController.Instance.LabelCountAmmoInGame.text;
	}

	void ChangeValue(int id)
	{
		StaticData.PD.CountAmmoWeapons[id]--;
	}

	public void Init()
	{
		//StaticData.PD.OpenWeapons [0] = 1;
		costModel=new int[StaticData.CountWeapons];
		costModel [0] = 500;
		costModel [1] = 1000;
		costModel [2] = 1500;
		costModel [3] = 2000;
		for (int i = 0; i < PW.Length; i++) {
			PW [i].ChangeCountAmmo = StaticData.PD.CountAmmoWeapons [PW [i].ID];
			PW [i].Init ();
			PW [i].gameObject.SetActive(false);
			PlayerWeapons.Add(PW [i].ID,PW[i]);
			PW [i].LabelsValue+=ChangeLabelsValue;
			PW [i].ChangeGlobalState+=ChangeValue;
			PW [i].ChangeValueLabels();
		}
		//Money = StaticData.PD.Money;
	}

	public void DisableChangeValue()
	{
		for (int i = 0; i < PW.Length; i++) {
			PW [i].ChangeGlobalState-=ChangeValue;
		}
	}

	public void EnableChangeValue()
	{
		for (int i = 0; i < PW.Length; i++) {
			PW [i].ChangeGlobalState+=ChangeValue;
		}
	}


	public void ViewStore()
	{
		PauseButton.SetActive (false);
		SwitchCam.enabled = false;
		Time.timeScale = 0f;
		Weapon.enabled = false;
		StoreUI.SetActive (true);
		//Money = StaticData.PD.Money;
		LeftButton.SetActive (false);
		RightButton.SetActive (true);
		currentSkin=0;
		ViewBuyModel ();
		ChooseWeapon.Instance.ButtonAttack.SetActive (false);
		ButtonAttackHands.SetActive (false);
	}

	public void CloseStore()
	{
		PauseButton.SetActive (true);
		SwitchCam.enabled = true;
		Time.timeScale = 1f;
		Weapon.enabled = true;
		StoreUI.SetActive (false);
		EntrySG.StoreGunCam.SetActive (false);
		EntrySG.IsEntry = false;
		DPAS.Target.position = BackPointStore.position;
		Guns [currentSkin].SetActive (false);
		//ChooseWeapon.Instance.ButtonAttack.SetActive (true);
		ChooseWeapon.Instance.ChangeImageGunInGame ();
		if (ChooseWeapon.Instance.ActiveWeapon == -1) {
			GameController.Instance.LabelCountAmmoInGame.enabled = false;
			ButtonAttackHands.SetActive (true);
		} else {
			ChooseWeapon.Instance.WPlayer.AcceptChange ();
			ChooseWeapon.Instance.ButtonAttack.SetActive (true);
		}

	}
	
	public void ChangeSkinRight()
	{
		if (!LeftButton.activeSelf)
			LeftButton.SetActive (true);
		Guns [currentSkin].SetActive (false);
		currentSkin++;
		if (currentSkin == StaticData.CountWeapons - 1) {
			if (RightButton.activeSelf)
				RightButton.SetActive (false);
		}
		ViewBuyModel ();
	}
	public void ChangeSkinLeft()
	{
		if (!RightButton.activeSelf)
			RightButton.SetActive (true);
		Guns [currentSkin].SetActive (false);
		currentSkin--;
		if (currentSkin == 0) {
			if (LeftButton.activeSelf)
				LeftButton.SetActive (false);
		}
		ViewBuyModel ();
	}
	/*public int Money
	{
		get{ return StaticData.PD.Money;}
		set{
			StaticData.PD.Money=value;
			LabelMoneyInGame.text=StaticData.PD.Money.ToString();
		}
	}*/
	
	void IsMoney()
	{
		if(Application.systemLanguage==SystemLanguage.Russian) Say.text="Продам за";
		else Say.text="Selling for";
		LabelCostModel.enabled=true;
		LabelCostModel.text=costModel [currentSkin].ToString();
		if(!ButtonBuy.activeSelf) ButtonBuy.SetActive (true);
		GameController.Instance.LabelCountAmmoInGame.enabled = true;
	}
	void CheckMoney()
	{
		CurrentGunInStore.sprite=AllGun[currentSkin];
		ChooseWeapon.Instance.LabelCountAmmoChooseGun[currentSkin].text=PW[currentSkin].CurrentCountAmmoInCage+"/"+PW[currentSkin].CountAmmo;
		GameController.Instance.LabelCountAmmoInGame.text = ChooseWeapon.Instance.LabelCountAmmoChooseGun [currentSkin].text;
		if (playerMoney.Cash >= costModel [currentSkin])
			IsMoney ();
		else
			NoMoney ();
		
	}
	void Unavailable()
	{
		if(Application.systemLanguage==SystemLanguage.Russian) Say.text="Еще не завезли";
		else Say.text="Unavailable";
		LabelCostModel.enabled=false;
		if(ButtonBuy.activeSelf) ButtonBuy.SetActive (false);
		GameController.Instance.LabelCountAmmoInGame.enabled = false;
	}

	void NoMoney()
	{
		if(Application.systemLanguage==SystemLanguage.Russian) Say.text="Бабла маловато";
		else Say.text="Not enough money";
		LabelCostModel.enabled=false;
		if(ButtonBuy.activeSelf) ButtonBuy.SetActive (false);
		GameController.Instance.LabelCountAmmoInGame.enabled = true;
	}


	
	void ViewBuyModel()
	{
		if (StaticData.PD.OpenWeapons [currentSkin] == 1)
			CheckMoney ();
		else
			Unavailable ();
		Guns [currentSkin].SetActive (true);
	}
	
	public void BuyModel()
	{
		if (playerMoney.Cash >= costModel [currentSkin]) {
			playerMoney.Cash -= costModel [currentSkin];
			//StaticData.PD.CountAmmoWeapons[currentSkin]+=StaticData.AddCountAmmo[currentSkin];
			PW [currentSkin].ChangeCountAmmo += StaticData.AddCountAmmo [currentSkin];
			StaticData.PD.CountAmmoWeapons [currentSkin] = PW [currentSkin].OverAllAmmo;
			//ChooseWeapon.Instance.LabelCountAmmoChooseGun [currentSkin].text = PW [currentSkin].CurrentCountAmmoInCage + "/" + PW[currentSkin].CountAmmo;
			//GameController.Instance.LabelCountAmmoInGame.text = ChooseWeapon.Instance.LabelCountAmmoChooseGun [currentSkin].text;
			if (playerMoney.Cash < costModel [currentSkin]) NoMoney ();
			//StaticData.PD.OpenWeapons[currentSkin]=1;
			//PlayerPrefs.SetInt ("StateModels" + currentSkin, 1);
			//ChangePlayerPrefs.Instance.IsChange=true;

			StaticData.PD.SaveOnDisk ();
		}
		if (PW [currentSkin].FullAmmo) {
			if(Application.systemLanguage==SystemLanguage.Russian) Say.text="Тебе хватит";
			else Say.text="Limit ammo";
			LabelCostModel.enabled=false;
			if(ButtonBuy.activeSelf) ButtonBuy.SetActive (false);
		}
	}
}

