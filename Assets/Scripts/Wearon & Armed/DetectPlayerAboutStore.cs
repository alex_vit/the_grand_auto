﻿using UnityEngine;
using System.Collections;

public class DetectPlayerAboutStore : MonoBehaviour {

	public Transform Target;
	public float DistanceOnViewStore;
	public GameObject Store;
	Transform thisTrans;
	bool isView=false;
	Vector2 posTarget,posStore;
	Vector3 temp;

	void Awake()
	{
		posStore.x = transform.position.x;
		posStore.y = transform.position.z;
	}

	void Update () {
		temp = Target.position;
		//Debug.Log ((posTarget - posStore).sqrMagnitude);
		posTarget.x = temp.x;
		posTarget.y = temp.z;
		//Debug.Log ((posTarget - posStore).sqrMagnitude);
		if ((posTarget - posStore).sqrMagnitude <= DistanceOnViewStore) {
			if(!isView) {
				Store.SetActive (true);
				isView=true;
			}
		} else {
			if(isView)
			{
				Store.SetActive (false);
				isView=false;
			}
		}


	
	}
}
