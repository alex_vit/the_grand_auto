﻿using UnityEngine;
using System.Collections;

public class Walker : MonoBehaviour {
	Transform selfTrans;
	Damager selfDamager;

	Transform target;

	bool isDamager;

	void Awake(){
		selfTrans = transform;
		selfDamager = GetComponent<Damager> ();

		isDamager = selfDamager != null;

		if (isDamager)
			selfDamager.OnTargetDefined += SetTarget;
	}

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void FixedUpdate(){
		
	}

	void SetTarget(Transform t){
	
	}

	void SetTarget(GameObject sender){
		target = sender.transform;
	}
}
