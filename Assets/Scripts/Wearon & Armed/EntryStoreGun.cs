﻿using UnityEngine;
using System.Collections;

public class EntryStoreGun : MonoBehaviour {
	
	public GameObject StoreGunCam;
	public GameObject Mark;

	bool isEntry = false;

	bool isHighlighted = false;
	bool isAvailable = false;

	void Start(){
		GetComponent<BoxCollider>().enabled=false;
		CheckAvailbale ();
	}

	void Update(){
		
	}

	public bool IsEntry
	{
		get{return isEntry;}
		set{isEntry=value;}
	}

	public void OnStore()
	{
		isEntry=true;
		if(!StoreGunCam.activeSelf) StoreGunCam.SetActive(true);
		StoreGun.Instance.ViewStore();
		AdGenerator.Instance.ShowShopAd ();
	}

	void OnTriggerEnter(Collider cld)
	{
		if (!isEntry) if (cld.CompareTag ("Player")) OnStore();
	}

	public void CheckAvailbale(){
		isAvailable = StaticData.PD.AvailableObjects [1] == 1;
		if (isAvailable) {
			GetComponent<BoxCollider>().enabled=true;
			Mark.SetActive (true);
		}
	}


}
