﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityStandardAssets.CrossPlatformInput;
using UnityStandardAssets.Characters.ThirdPerson;

public class GameController : MonoBehaviour {

	public static GameController Instance;
	public Text LabelMoneyInGame;
	public GameObject GunCamera;
	public GameObject MnCameraFPC;
	public GameObject MnCameraTPC;
	public GameObject ChooseGunUI;
	public Text LabelCountAmmoInGame;
	public GameObject PlayerMesh;
	public Animator PlayerAnim;
	public Weapon PlayerWearon;
	public BaseMobileJoystick DynamicJoy, StaticJoy;
	public ThirdPersonUserControl TPUC;
	public GameObject[] ObjectThatNeedInActive,ObjectThatNeedActive;
	public IKGun IkGun;
	public Transform ResPlayer;
	public GameObject MMMCAMERA;
	public ControlPS InMiniGame,InGame;

	Transform tempPlace;
	GameObject tempGO;
	/*public ThirdPersonUserControl TPUC;
	public ThirdPersonCharacter TPC;*/
	Camera gunCam;

	public void ActiveWeapon()
	{
		if (gunCam)
		if (GunCamera)
			gunCam = GunCamera.GetComponent<Camera> ();
		gunCam.enabled = true;
	}
	public void InActiveWeapon()
	{
		if (gunCam)
			if (GunCamera)
				gunCam = GunCamera.GetComponent<Camera> ();
		gunCam.enabled = false;
	}
	
	public Camera SetGunCam
	{
		set{ if(value!=null) gunCam = value;}
	}

	public int Money
	{
		get{ return StaticData.PD.Money;}
		set{
			StaticData.PD.Money=value;
			LabelMoneyInGame.text=StaticData.PD.Money.ToString();
		}
	}

	public void SetStaticJoystick()
	{
		TPUC.CMJ.gameObject.SetActive (false);
		TPUC.CMJ = StaticJoy;
		TPUC.CMJ.gameObject.SetActive (true);
	}

	public void SetDynamicJoystick()
	{
		TPUC.CMJ.gameObject.SetActive (false);
		TPUC.CMJ = DynamicJoy;
		TPUC.CMJ.gameObject.SetActive (true);
	}

	public void EntryMiniGame(GameObject go)
	{
		tempPlace = LifeController.Instance.hospitalPlace;
		LifeController.Instance.hospitalPlace = ResPlayer;
		IkGun.ikActive = true;
		tempGO = Instantiate (StaticData.Prefabs ["MiniGame"]) as GameObject;
		for (int i=0; i<ObjectThatNeedInActive.Length; i++)
			if (ObjectThatNeedInActive [i] != null)
				ObjectThatNeedInActive [i].SetActive (false);
		for (int i=0; i<ObjectThatNeedActive.Length; i++)
			if (ObjectThatNeedActive [i] != null)
				ObjectThatNeedActive [i].SetActive (true);
	}
	public void ExitMiniGame()
	{
		LifeController.Instance.hospitalPlace = tempPlace;
		IkGun.ikActive = false;
		Destroy (tempGO);
		for (int i=1; i<ObjectThatNeedInActive.Length; i++)
			if (ObjectThatNeedInActive [i] != null)
				ObjectThatNeedInActive [i].SetActive (true);
		for (int i=0; i<ObjectThatNeedActive.Length; i++)
			if (ObjectThatNeedActive [i] != null)
				ObjectThatNeedActive [i].SetActive (false);
	}


	enum StateCam{
		TPC,//third pers control
		FPC //first pers control
	}

	StateCam SC;


#if UNITY_EDITOR
	void Update()
	{
		if (Input.GetKeyDown (KeyCode.R)) {
			for(int i=1;i<8;i++) Debug.Log (StaticData.PD.CompletedMissions[i]);
		}
		if (Input.GetKeyDown (KeyCode.A)) {
			for(int i=1;i<7;i++) StaticData.PD.CompletedMissions[i]=1;
		}
		if (Input.GetKeyDown (KeyCode.G)) {
			for(int i=1;i<8;i++) StaticData.PD.CompletedMissions[i]=0;
		}
		if (Input.GetKeyDown (KeyCode.C)) {
			PlayerPrefs.DeleteAll();
		}
		if (Input.GetKeyDown (KeyCode.H)) {
			StaticData.PD.OpenWeapons[0]=1;
			//EntryMiniGame();
		}
		if (Input.GetKeyDown (KeyCode.E)) {
			StaticData.PD.CompletedMissions[8]=0;
			//EntryMiniGame();
		}
		if (Input.GetKeyDown (KeyCode.P)) {
			if(PlayerController.Instance.TPlayer.GetComponent<Weapon>().PW!=null){
				PlayerController.Instance.TPlayer.GetComponent<Weapon>().PW.ChangeCountAmmo=100;
				PlayerController.Instance.TPlayer.GetComponent<Weapon>().PW.ActivePW();
			}
			//EntryMiniGame();
		}

	}
#endif


	void Awake()
	{
		Screen.sleepTimeout = SleepTimeout.NeverSleep;
		Instance = this;
	}

	public void SwitchStateCam()
	{
		if (SC == StateCam.FPC) {
			SC = StateCam.TPC;
			MnCameraFPC.SetActive (false);
			MnCameraTPC.SetActive (true);
			GunCamera.SetActive (false);
			PlayerMesh.SetActive(true);
			PlayerAnim.enabled=true;
			PlayerController.Instance.SetGunCam=MnCameraTPC.GetComponent<Camera>();
			/*TPC.enabled=true;
			TPUC.enabled=true;*/
		} else if (SC == StateCam.TPC) {
			SC = StateCam.FPC;
			MnCameraFPC.SetActive (true);
			MnCameraTPC.SetActive (false);
			GunCamera.SetActive (true);
			PlayerMesh.SetActive(false);
			PlayerAnim.enabled=false;
			PlayerController.Instance.SetGunCam=MnCameraFPC.GetComponent<Camera>();
			/*TPC.enabled=false;
			TPUC.enabled=false;*/
		}

	}

	public void OnChooseGun()
	{
		Time.timeScale = 0f;
		ChooseGunUI.SetActive (true);

	}
	public void OffChooseGun()
	{
		Time.timeScale = 1f;
		ChooseGunUI.SetActive (false);
		
	}

	public void ViewStore()
	{
		GunCamera.SetActive (false);
		if(SC==StateCam.FPC) MnCameraFPC.SetActive (false);
		else if(SC==StateCam.TPC) MnCameraTPC.SetActive (false);
	}
	public void CloseStore()
	{
		GunCamera.SetActive (true);
		if (SC == StateCam.FPC)
			MnCameraFPC.SetActive (true);
		else if (SC == StateCam.TPC)
			MnCameraTPC.SetActive (true);
	}




	// Use this for initialization
	void Start () {
		StoreGun.Instance.Init ();
		ChooseWeapon.Instance.Init ();
		//for(int i=1;i<7;i++) StaticData.PD.CompletedMissions[i]=1;
		//StaticData.PD.CountAmmoWeapons [0] = 100;
	}

	void OnApplicationQuit(){
		StaticData.PD.SaveAll ();
	}

}
