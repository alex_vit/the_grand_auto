﻿using UnityEngine;
using System.Collections;

public class EscCatcher : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
#if UNITY_EDITOR
		if (Input.GetKeyDown(KeyCode.Escape)) 
			Application.Quit();
		if (Input.GetKeyDown (KeyCode.D)) 
			PlayerPrefs.DeleteAll ();
#endif

	}
}
