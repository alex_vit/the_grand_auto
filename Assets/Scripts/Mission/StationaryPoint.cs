﻿using UnityEngine;
using System.Collections;

public delegate void GameObjectEvent(GameObject sender);

public class StationaryPoint : CheckPoint {
	/*public int ID;
	public int Index; 
	public CheckPointType type;
	public bool isCompleted;*/

	/*bool isActive;


	public bool isEnabled{
		get{ 
			return isActive;	
		}
		set{
			isActive = value;

			if (isActive) {
				MakeActive ();
			} else {
				MakeInactive ();
			}
		}
	}*/
	public bool isChecked;
	public Transform pointTransform;
	public override event GameObjectEvent OnChecked;

	GameObject mark;
	// Use this for initialization
	/*void Awake () {
		//mark = transform.GetChild (0).gameObject;
		Debug.Log("AWAKE");
		Debug.Log(gameObject.activeSelf);
		Debug.Log(transform.GetChild (0).gameObject.activeSelf);
		Debug.Log(transform.GetChild (0).GetComponent<ParticleSystem>().IsAlive());
	}

	void Start()
	{
		Debug.Log("START");
		Debug.Log(gameObject.activeSelf);
		Debug.Log(transform.GetChild (0).gameObject.activeSelf);
		Debug.Log(transform.GetChild (0).GetComponent<ParticleSystem>().IsAlive());
	}

	void Update()
	{
		Debug.Log("UPDATE");
		Debug.Log(gameObject.activeSelf);
		Debug.Log(transform.GetChild (0).gameObject.activeSelf);
		Debug.Log(transform.GetChild (0).GetComponent<ParticleSystem>().IsAlive());
	}*/


	/*public override Transform GetTransform(){
		return base.GetTransform ();
	}*/
		
	public override Transform GetPointPosition(){
		return pointTransform;
	}

	public override void SetActive(){
		gameObject.SetActive (true);
		//mark.SetActive (true);
	}

	void OnTriggerEnter(Collider coll){
	if (coll.gameObject.tag == "Player" || coll.gameObject.GetComponentInParent<CheckPoint>() != null) {
			if (OnChecked != null) {
				OnChecked (gameObject);
				gameObject.SetActive (false);
			}
		}
	}

	
}

