﻿using UnityEngine;
using System.Linq;
using System;
using System.Collections;
using System.Collections.Generic;

public class Mission {
	public int misID;
	MissionType type;
	string description;
	bool isCompleted;

	public Mission(){
	
	}

	public Mission(MissionInfo info){
		//misID = info.MisID;
		//type = info.type;
		//description = info.descr;
		isCompleted = info.isCompleted;
	}

	public MissionType GetType(){
		return type;
	}
}

public enum MissionType{
	TRIP, // u need walk from A to B 
	MURDER, // u need kill the target
	CAR_JACK,
	STASH,
	CATCH
}

public enum RewindType{
	NONE,
	MONEY,
	RESPECT
}

//struct for info about concrete mission
public class MissionInfo{
	public int ID;
	public MissionType type;
	public bool isCompleted;

	public RewindType rewindType;
	public int rewind;

	List<CheckPoint> checkPoints;

	public MissionInfo(int id, MissionType mistype, RewindType rewtype, int count, Action misAction){
		//stub
		ID = id;
		type = mistype;
		isCompleted = StaticData.PD.CompletedMissions[id] == 1;
		rewindType = rewtype;
		rewind = count;
	}

	public void SetCheckPoints(List<CheckPoint> list){
		checkPoints = list;	
	}

	public List<CheckPoint> GetCheckPoints(){
		return checkPoints;
	}

	public CheckPoint GetNext(int currIndex){
		return checkPoints.Find (x => x.Index == (currIndex + 1));
	}

	public CheckPoint GetPrev(int currIndex){
		return checkPoints.Find (x => x.Index == (currIndex - 1));
	}

	public CheckPoint GetBegin(){
		//Debug.Log (checkPoints.Count);
		return checkPoints.Find (x => x.Index == 0 && x.type == CheckPointType.BEGIN);
	}
}
