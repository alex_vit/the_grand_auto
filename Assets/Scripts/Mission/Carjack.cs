﻿using UnityEngine;
using System.Collections;

public class Carjack : CheckPoint {
	public override event GameObjectEvent OnChecked;
	public override event GameObjectEvent OnFailChecked;

	public GameObject selficon;

	CarLife selfLife;
	Vector3 rootPlace;
	//public int ID;

	//public System.Action OnGetIn;
	//public System.Action OnGetOut;
	//public System.Action OnDead;
	// Use this for initialization

	/*public override Transform GetTransform(){
		//Debug.Log (gameObject);
		return base.GetTransform ();
	}*/

	void Awake(){
		//Debug.Log ("awake");
		selfLife = GetComponent<CarLife> ();
		rootPlace = transform.position;
	}

	void OnEnable(){
		//Debug.Log (UIControlViewer.inst);
		UIControlViewer.inst.OnGetInCar += TakeInTheCar;
		UIControlViewer.inst.OnGetOutOfCar += TakeOutTheCar;

		if(MissionController.Instance)
		selfLife.OnDeath += MissionController.Instance.MissionFailed;
	}

	void OnDisable(){
		if (this.enabled) {
			UIControlViewer.inst.OnGetInCar -= TakeInTheCar;
			UIControlViewer.inst.OnGetOutOfCar -= TakeOutTheCar;

			if(MissionController.Instance)
			selfLife.OnDeath -= MissionController.Instance.MissionFailed;
		}
	}

	public override Transform GetPointPosition(){
		return transform;
	}

	public override void SetActive ()
	{
		gameObject.SetActive (true);
		GetComponent<Rigidbody> ().useGravity = true;
		SetDefault ();
	}

	void SetDefault(){
		transform.position = rootPlace;
	}

	void TakeInTheCar(GameObject sender){
		//Debug.Log ("take in the car");
		if (OnChecked != null && sender.GetComponent<CheckPoint>() != null) {
			//Debug.Log ("Carjack");
			OnChecked (gameObject);

			selficon.SetActive (false);
		}
	}

	void TakeOutTheCar(GameObject sender){
		if (OnFailChecked != null && sender.GetComponent<CheckPoint>() != null) {
			OnFailChecked (gameObject);

			selficon.SetActive (true);
		}
	}


}
