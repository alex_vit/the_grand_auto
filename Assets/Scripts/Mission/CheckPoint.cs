﻿using UnityEngine;
using System.Collections;

public class CheckPoint : MonoBehaviour {
	public virtual event GameObjectEvent OnChecked;
	public virtual event GameObjectEvent OnFailChecked; 

	public int ID;
	public int Index; 
	public CheckPointType type;
	public bool isCompleted;
	public Texture icon;

	Transform selfTrans;
	bool isActive;
	// Use this for initialization
	void Start () {
		//Debug.Log (gameObject);
		selfTrans = transform;

		//gameObject.SetActive (false);
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public virtual Transform GetTransform(){
		return selfTrans;
	}

	public virtual void SetActive(){
		gameObject.SetActive (true);
		isActive = true;
	}

	public virtual void SetInActive(){
		gameObject.SetActive (false);
		isActive = false;
	}

	public virtual bool IsActive(){
		return isActive;
	}

	public virtual void Check(){
		isCompleted = true;
	}

	public virtual Transform GetPointPosition(){
		return null;
	}

	public virtual Texture GetIcon(){
		return icon;
	}
}

public enum CheckPointType{
	BEGIN,
	CHECK,
	END
}
