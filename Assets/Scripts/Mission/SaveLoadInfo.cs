﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public static class SaveLoadInfo{
	static bool inited;

	public static void Init(){
		if (!inited) {
			Load ();
			inited = true;
		}
	}

	static void Load(){
		LoadInfo ();
	}

	public static void Save(){
		SaveInfo ();	
	}

	//need dictionary that contains refs for info about missions
	public static Dictionary<CityType, List<MissionInfo>> MissionsList;

	static void LoadInfo(){
		MissionsList = new Dictionary<CityType, List<MissionInfo>> ();

		List<MissionInfo> tmp = new List<MissionInfo> ();
		//filling info about missions
		tmp.Add(new MissionInfo(1, MissionType.TRIP, RewindType.MONEY, 500, null));
		tmp.Add(new MissionInfo(2, MissionType.TRIP, RewindType.NONE, 0, null));
		tmp.Add(new MissionInfo(3, MissionType.MURDER, RewindType.MONEY, 500, null));
		tmp.Add(new MissionInfo(4, MissionType.MURDER, RewindType.NONE, 0, null));
		tmp.Add(new MissionInfo(5, MissionType.CAR_JACK, RewindType.MONEY, 2000, null));
		tmp.Add(new MissionInfo(6, MissionType.MURDER, RewindType.NONE, 0, null));
		tmp.Add(new MissionInfo(7, MissionType.TRIP, RewindType.NONE, 0, null));
		tmp.Add(new MissionInfo(8, MissionType.CATCH, RewindType.MONEY, 3000, null));
		//tmp.Add(new MissionInfo(002,MissionType.TRIP, isMissionCompleted[2], RewindType.NONE, 0));
		
		//tmp.Add (new MissionInfo (002,MissionType.TRIP,false));

		MissionsList.Add (CityType.ONE, tmp);



	}

	static void SaveInfo(){
				
	}
}

public enum CityType{
	ONE,
	TWO
}
