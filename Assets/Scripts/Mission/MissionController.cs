﻿using UnityEngine;
using System.Linq;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class MissionController : MonoBehaviour{
	enum MissionStep{
		IDLE, //if we doesn't have any activemission
		START, //if we began a mission
		READY, //if we saw all diloag for current mission
		PROGRESS, //if we are in progress
		END //if we ended mission
	}


	public static MissionController Instance;
	public MiniMapDrawer mapDrawer;
	public GameObject[] MissionInfoObjects;

	//public CheckPoint[] checkPoints;
	//public Victim[] targetsForKill;
	//public Carjack[] targetForJack;

	//public TargetIndicator arrow;
	public Player selfPlayer;
	public TargetMark targetMark;
	public Healther player;

	public FadeText missionCompletedLabel;
	public FadeText missionFailedLabel;

	public EntryStoreGun gunStore;
	public Estate cashPoint;

	public GameObject buttonHands;

	List<CheckPoint> CheckPoints;
	CheckPoint activeCheckPoint;
	Victim activeVictim;

	CheckPoint activeTarget;

	List<MissionInfo> Missions;
	MissionInfo activeMission;
	Dictionary<int, List<CheckPoint>> CheckPointsForMission;

	MissionStep Step;
	event EmptyEvent OnStepChanged;

	void Awake(){
		if (Instance == null) {
			Instance = this;
			InitCheckPoints ();
		} else
			Destroy (gameObject);
	}

	// Use this for initialization
	void Start () {
		//Application.targetFrameRate = 60;

		//SetNextMission ();
		CheckPoints.Where(x=> x.ID == 8 && x.type == CheckPointType.BEGIN).First().OnChecked += GameController.Instance.EntryMiniGame;
		Step = MissionStep.IDLE;
		OnStepChanged ();
	}

	void OnEnable(){
		//if(DialogViewer.Instance)
		//Debug.Log("DialogViewer="+DialogViewer.Instance);
		DialogViewer.Instance.OnDialogWasShown += GetReadyForMission;
		OnStepChanged += ChangeStep;
		player.OnDeath += MissionFailed;

		foreach(CheckPoint point in CheckPoints){
			point.OnChecked += CompleteCheckPoint;
			point.OnFailChecked += FailCheckPoint;
		}
		/*if (DotaController.Instance) {
			DotaController.Instance.OnWin += MissionCompleted;
			DotaController.Instance.OnLose += MissionFailed;
		}*/
	}

	public void DisableMissionFailedOnDeath()
	{
		player.OnDeath -= MissionFailed;
	}
	public void EnableMissionFailedOnDeath()
	{
		player.OnDeath -= MissionFailed;
		player.OnDeath += MissionFailed;
	}
#if UNITY_EDITOR
	void Update()
	{
		//if (Input.GetKeyDown (KeyCode.M)&&Input.GetKeyDown (KeyCode.C)) {
			//MissionCompleted();
		//}
	}
#endif
	void OnDisable(){
		if (this.enabled) {
			if(DialogViewer.Instance)
				DialogViewer.Instance.OnDialogWasShown -= GetReadyForMission;
			OnStepChanged -= ChangeStep;
			player.OnDeath -= MissionFailed;

			foreach (CheckPoint point in CheckPoints) {
				point.OnChecked -= CompleteCheckPoint;
				point.OnFailChecked -= FailCheckPoint;
			}
			CheckPoints.Where(x=> x.ID == 8 && x.type == CheckPointType.BEGIN).First().OnChecked -= GameController.Instance.EntryMiniGame;
			/*if (DotaController.Instance) {
				DotaController.Instance.OnWin -= MissionCompleted;
				DotaController.Instance.OnLose -= MissionFailed;
			}*/
		}
	}

	#region инициализация контрольных точек
	void InitCheckPoints(){
		Missions = SaveLoadInfo.MissionsList [CityType.ONE];
		CheckPoints = new List<CheckPoint> ();
		//List<StationaryPoint> check_new = new List<StationaryPoint> ();

		if (Missions.Count != MissionInfoObjects.Count()) {
			Debug.LogError ("Logic and physical parts of mission are not identical");
			return;
		}
		for (int i = 0; i < MissionInfoObjects.Count(); i++) {
			List<CheckPoint> checks = MissionInfoObjects [i].GetComponentsInChildren<CheckPoint> ().ToList();
			//List<StationaryPoint> check_new = MissionInfoObjects [i].GetComponentsInChildren<StationaryPoint> ().ToList();
			//Debug.Log (Missions[i].ID +  " check count " + checks.Count); 
			Missions [i].SetCheckPoints (checks);
			CheckPoints.AddRange (checks);
		}

		for(int i=0; i< CheckPoints.Count; i++){
			CheckPoints [i].SetInActive ();
		}
		//Debug.Log ("CheckPoints.Count " + CheckPoints.Count);
	}
	#endregion


	#region проверка checkpoint'ов на выполнение-невыполнение
	void CompleteCheckPoint(GameObject sender){
		//Debug.Log ("checkpoint completed");
		activeCheckPoint.isCompleted = true;

		if (sender.GetComponent<CheckPoint>().type == CheckPointType.BEGIN) {
			//if it's a begin point we send event that will be show dialogs;
			Step = MissionStep.START;
			OnStepChanged ();
		}
		if (sender.GetComponent<CheckPoint>().type == CheckPointType.CHECK && Step == MissionStep.PROGRESS) {

			SetNextCheckPoint ();
		}
		if (sender.GetComponent<CheckPoint>().type == CheckPointType.END && Step == MissionStep.PROGRESS) {
			Step = MissionStep.END;
			OnStepChanged ();
		}
	}

	void FailCheckPoint(GameObject sender){
		//Debug.Log ("FailCheckPoint");
		if (activeMission.ID == 8) {
			activeCheckPoint.SetInActive ();
			activeCheckPoint.isCompleted = false; 
			
			
			SetPrevCheckPoint ();
		} else {
			if (activeMission != null && !activeMission.isCompleted && activeMission.ID == sender.GetComponent<CheckPoint> ().ID) {
				activeCheckPoint.SetInActive ();
				activeCheckPoint.isCompleted = false; 

				SetPrevCheckPoint ();
			}
		}
	}
	#endregion

	public void Restart(){
		InitCheckPoints ();
		SetNextMission ();
	}

	public Transform GetCurrentTargetPoint(){
		//Debug.Log (activeTarget.GetTransform ());
		return activeTarget.GetTransform();
	}

	public int GetCurrentMissionID(){
		if (activeMission != null && !activeMission.isCompleted)
			return activeMission.ID;
		else
			return 0;
	}

	void GetReadyForMission(){
		Step = MissionStep.PROGRESS;
		OnStepChanged ();
	}

	void GetRewind(){
		switch (activeMission.rewindType) {
		case RewindType.MONEY:
			if (activeMission.rewind != 0) {
				missionCompletedLabel.Text = "Mission completed\n +" + (activeMission.rewind.ToString ());
				//playerMoney.Cash += activeMission.rewind;
				selfPlayer.GetComponent<MoneyOwner>().Cash += activeMission.rewind;
			}
			break;
		case RewindType.NONE:
			missionCompletedLabel.Text = "Mission completed\n";
			break;
		}
	}

	public void MissionCompleted(){
		//Debug.Log ("success");
		//mark mission as completed, get rewind, and save it
		activeMission.isCompleted = true;
		mapDrawer.RemoveTarget ();
		StaticData.PD.CompletedMissions [activeMission.ID] = 1;
		if (activeMission.ID == 2) {
			StaticData.PD.OpenWeapons [0] = 1;
			StaticData.PD.AvailableObjects [1] = 1;
			//Debug.Log ("gunStore.OnStore ()");
			gunStore.OnStore ();
			//Debug.Log ("gunStore.CheckAvailbale ()");
			gunStore.CheckAvailbale ();
			//Debug.Log ("buttonHands.SetActive (false)");
			buttonHands.SetActive (false);
		}
		if (activeMission.ID == 7) {
			StaticData.PD.AvailableObjects [2] = 1;
			cashPoint.CheckIfAvailable ();
		}
		StaticData.PD.SaveAll ();
		StaticData.PD.SaveOnDisk ();
		ChooseWeapon.Instance.UpdateWeapons ();
		GetRewind ();
		missionCompletedLabel.gameObject.SetActive (true);
		missionCompletedLabel.Show ();

		List<CheckPoint> points = activeMission.GetCheckPoints ();
		for (int i = 0; i < points.Count; i++) {
			points [i].SetInActive ();
		}


		Step = MissionStep.IDLE;
		//Debug.Log ("Step = MissionStep.IDLE");
		OnStepChanged ();
		//Debug.Log ("OnStepChanged ()");
	}

	public void MissionFailed(GameObject sender){
		//Debug.Log ("fail");
		if (activeMission != null && !activeMission.isCompleted && Step == MissionStep.PROGRESS) {
			missionFailedLabel.Text = "Mission failed";
			missionFailedLabel.gameObject.SetActive (true);
			missionFailedLabel.Show ();
			FailCheckPoint (sender);
			Step = MissionStep.IDLE;
			OnStepChanged ();
		}
	}

	void SetNextMission(){
		int currMissionsCount = Missions.Where (x => !x.isCompleted).Count ();

		if (currMissionsCount != 0) {
			activeMission = Missions.Where (x => !x.isCompleted).Aggregate ((c, d) => c.ID < d.ID ? c : d);
			//Debug.Log ("activeMissionID " + activeMission.ID);


			activeCheckPoint = activeMission.GetBegin ();
			activeCheckPoint.SetActive ();
			mapDrawer.SetTarget (activeCheckPoint.GetPointPosition ().position, activeCheckPoint.GetIcon ());

			//Debug.Log (activeCheckPoint);
			//activeTarget = activeCheckPoint;
			//activeTarget.SetActive ();
			//Debug.Log (activeCheckPoint.gameObject.activeSelf);
		} else {
			//Debug.Log ("all missions completed");
		}
		//arrow.SetActive ();
	}

	void SetNextCheckPoint(){
		CheckPoint next = activeMission.GetNext (activeCheckPoint.Index);
		//Debug.Log ("next " + next.enabled);
		if (next) {
			activeCheckPoint = next;
			activeCheckPoint.SetActive ();
			mapDrawer.SetTarget (activeCheckPoint.GetPointPosition ().position, activeCheckPoint.GetIcon());
		}
	}

	void SetPrevCheckPoint(){
		CheckPoint prev = activeMission.GetPrev (activeCheckPoint.Index);
		//Debug.Log ("next " + prev.enabled);
		if (prev) {
			activeCheckPoint = prev;
			activeCheckPoint.SetActive ();
			mapDrawer.SetTarget (activeCheckPoint.GetPointPosition ().position, activeCheckPoint.GetIcon());
		}
	}

	void ChangeStep(){
		switch (Step) {
		case MissionStep.IDLE:
			SetNextMission ();
			break;
		case MissionStep.START:
			DialogViewer.Instance.Show (activeMission);
			break;
		case MissionStep.READY:
			GetReadyForMission ();
			break;
		case MissionStep.PROGRESS:
			//Debug.Log ("progress");
			SetNextCheckPoint ();
			break;
		case MissionStep.END:
			MissionCompleted ();
			break;
		}
	}

	

}

