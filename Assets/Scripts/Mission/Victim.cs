﻿using UnityEngine;
using System.Collections;

public class Victim : CheckPoint  {
	public override event GameObjectEvent OnChecked;
	public override event GameObjectEvent OnFailChecked;
	//public int ID;
	Healther charLife;
	CarLife carLife;
	PullElement pullElem;

	Vector3 rootPlace;
	// Use this for initialization
	void Awake(){
		charLife = GetComponent<Healther> ();
		carLife = GetComponent<CarLife> ();
		pullElem = GetComponent<PullElement> ();

		rootPlace = transform.position;
	}

	void OnEnable(){
		if (charLife) {
			charLife.OnDeath += WasKilled;
			if(LifeController.Instance)
			charLife.OnDeath += LifeController.Instance.CharDeath;
		}
		if (carLife) {
			//Debug.Log ("yyy");
			carLife.OnDeath += WasKilled;
		}
	}

	void OnDisable(){
		if (charLife && enabled) {
			charLife.OnDeath -= WasKilled;
			charLife.OnDeath -= LifeController.Instance.CharDeath;
		}
		if (carLife && enabled) {
			carLife.OnDeath -= WasKilled;
		}
	}

	void SetDefault(){
		transform.position = rootPlace;

		if (charLife != null)
			charLife.SetDefaultHealth ();
		if (carLife != null)
			carLife.SetDefault ();
	}

	void WasKilled(GameObject sender){
		//Debug.Log ("was killed");
		if (OnChecked != null)
			OnChecked (gameObject);	
	}

	public override void SetActive(){
		//Debug.Log ("set active");

		gameObject.SetActive (true);
		//IsActive = true;
		if(pullElem != null)
			pullElem.SetActive ();
		SetDefault ();
	}

	public override void SetInActive(){
		//gameObject.SetActive (false);
		Invoke("Die",3f);
	}

	public override Transform GetPointPosition(){
		return transform;
	}

	public void Show(){
		gameObject.SetActive (true);
	}

	public void Hide(){
		gameObject.SetActive (false);
	}

	void Die(){
		gameObject.SetActive (false);
	}
} 

