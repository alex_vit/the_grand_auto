﻿using System;
using System.Collections;
using System.Collections.Generic;

public class ActionManager{
	Dictionary<ActionType, Action> ActionList;

	public ActionManager(){
		FillList ();
	}

	void FillList(){
		ActionList = new Dictionary<ActionType, Action> ();

		Action weaponAction = null;
		Action rieltyAction = null;

		ActionList.Add(ActionType.WEAPON, weaponAction);
		ActionList.Add (ActionType.RIELTY, rieltyAction);
	}

	public System.Action GetAction(ActionType type){
		return ActionList [type];
	}

	public void InvokeAction(ActionType type){
		if (ActionList [type] != null)
			ActionList [type]();
	}

	public void InvokeAction(ActionType type, int param){
		if (ActionList [type] != null)
			ActionList [type] ();
	}
}

public enum ActionType{
	WEAPON,
	RIELTY
}
