﻿using UnityEngine;
using System.Collections;

public class CranRotate : MonoBehaviour {

	float speed = 0;

	// Use this for initialization
	void Start () {
		InvokeRepeating ("Work", 0, 10);

	}

	void Work()
	{
		speed = Random.Range (-10f, 10f);
	}

	// Update is called once per frame
	void FixedUpdate () {
		transform.Rotate (Vector3.up, speed*Time.fixedDeltaTime);
	}
}
