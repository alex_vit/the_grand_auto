﻿using UnityEngine;
using System.Collections;
using System.Runtime.InteropServices;
using System;
using System.Collections.Generic;

public class PushNotifications : MonoBehaviour {

	#if UNITY_IPHONE && !UNITY_EDITOR
	[DllImport ("__Internal")]
	private static extern void _CreateNotification();
	#endif
	List<string> list = new List<string> ();
	public int Hours = 12;
	int Minutes = 3;
	public bool test = false;
	// Use this for initialization
	void Start () 
	{
		list.Clear ();
		TextAsset text = Resources.Load("notification") as TextAsset;
		string[] strings = text.text.Split ('\n');
		list.AddRange (strings);
		#if UNITY_IPHONE && !UNITY_EDITOR
		_CreateNotification ();
		NotificationServices.CancelAllLocalNotifications ();
		for (int i = 0; i<30; i++)
		{
			var n = new LocalNotification ();
			if(!test)
				n.fireDate = DateTime.Now.AddHours(Hours*(i+1));
			else
				n.fireDate = DateTime.Now.AddMinutes(Minutes*(i+1));
			n.alertBody = list [UnityEngine.Random.Range (0, list.Count)];
			n.alertAction = "Can Knock Down";
			n.hasAction = true;
			n.soundName = LocalNotification.defaultSoundName;
			NotificationServices.ScheduleLocalNotification (n);
		}
		#endif
	}
	
	// Update is called once per frame
	void Update () 
	{
		#if UNITY_IPHONE && !UNITY_EDITOR
		if (NotificationServices.localNotificationCount > 0) 
		{
			//Здесь пишите обработчик события пуш сообщения
			//пример : getLabel("Label").text = NotificationServices.localNotifications[NotificationServices.localNotificationCount-1].alertBody;
			//Код ниже оставляете без изменений
			NotificationServices.CancelAllLocalNotifications ();
			for (int i = 0; i<30; i++)
			{
				var n = new LocalNotification ();
				if(!test)
					n.fireDate = DateTime.Now.AddHours(Hours*(i+1));
				else
					n.fireDate = DateTime.Now.AddMinutes(Minutes*(i+1));
				n.alertBody = list [UnityEngine.Random.Range (0, list.Count)];
				n.alertAction = "Can Knock Down";
				n.hasAction = true;
				n.soundName = LocalNotification.defaultSoundName;
				NotificationServices.ScheduleLocalNotification (n);
			}
		}
		#endif
	}
}
