﻿using UnityEngine;
using System.Collections;
using UnityStandardAssets.Characters.ThirdPerson;
using UnityStandardAssets.Cameras;

public class LifeController : MonoBehaviour {
	public static LifeController Instance;

	public GameObject WatchVideo,CharCtrl_,CarCtrl_;

	public Healther player;
	public FreeLookCam CameraChar;
	public AutoCam CameraCar;

	public Transform hospitalPlace;

	int countVideo = 0;
	int maxCountVideo = 3;

	bool isWatchVideo;
	// Use this for initialization
	void Awake(){
		if (Instance == null)
			Instance = this;
		else
			Destroy (this.gameObject);

		isWatchVideo = false;
	}

	void Start () {

	}

	// Update is called once per frame
	void Update () {

	}

	public void RespawnPlayer(){
		if(!gameObject.activeSelf) 
			gameObject.SetActive(true);

		player.GetComponent<Damager> ().FightMode = false;
		player.SetDefaultHealth ();
		player.GetComponent<Player> ().SetPlace (hospitalPlace);

		CharCtrl_.SetActive(true);
		CarCtrl_.SetActive(false);
		GameMenu.Instance.userPanel.SetActive(true);
		GameMenu.Instance.touchPanel.Show ();
		GameMenu.Instance.gamePanel.SetActive (true);
		GameMenu.Instance.DeathText.SetActive(false);
		GameMenu.Instance.RestartButtonDeath.SetActive(false);
		player.GetComponent<Collider>().enabled = true;
		player.GetComponent<ThirdPersonUserControl>().enabled=true;
		player.GetComponent<ThirdPersonCharacter>().enabled=true;

		player.GetComponent<Animator>().enabled = true;
		player.GetComponent<Rigidbody>().isKinematic=false;

		if(player.GetComponentInChildren<RagdollEnabler>())
			player.GetComponentInChildren<RagdollEnabler>().DisableRagdoll ();
		player.GetComponent<ThirdPersonCharacter> ().Move (Vector3.zero, false, false);
		CameraChar.enabled = true;

		Time.timeScale = 1f;

		if (!isWatchVideo)
			player.GetComponent<MoneyOwner> ().Cash -= 200;
		else {
			isWatchVideo = false;
			UserAdvertise.ShowRewardedVideo();
		}
		GameMenu.Instance.state=GameMenu.State.Normal;
		MissionController.Instance.Restart ();		
	}

	public void RespawnPlayerWithWatchVideoOrNot(bool value)
	{
		UIControlViewer.inst.DeathInvoke ();
		isWatchVideo = value;
		RespawnPlayer ();
	}

	public void CharDeath(GameObject sender){
		Debug.Log ("sender " + sender.name);

		bool is_player = false;

		if(sender.GetComponent<Player>() != null)
			is_player = true;

		if (sender.activeSelf) {
			sender.GetComponent<Collider> ().enabled = false;
			//GetComponent<ThirdPersonUserControl>().enabled=false;
			sender.GetComponent<ThirdPersonCharacter> ().enabled = false;

			sender.GetComponent<Animator> ().SetBool ("KickHand", false);
			sender.GetComponent<Animator> ().enabled = false;
			sender.GetComponent<Rigidbody> ().isKinematic = true;

			Rigidbody root_bone = sender.GetComponentInChildren<RagdollEnabler> ().ActivateRagdoll ();
		}

		if (is_player) {
			Debug.Log ("player");
			if (countVideo < maxCountVideo) {
				countVideo++;
				WatchVideo.SetActive (true);
			}
			GameMenu.Instance.state = GameMenu.State.Pause;
			GameMenu.Instance.pausePanel.SetActive (false);
			GameMenu.Instance.DeathText.SetActive (true);
			GameMenu.Instance.RestartButtonDeath.SetActive (true);
			GameMenu.Instance.userPanel.SetActive (false);
			GameMenu.Instance.touchPanel.Hide ();

			Time.timeScale = 0.2f;

			CameraChar.enabled = false;
		} else {
			if (sender.GetComponent<PullElement> () != null && sender.CompareTag ("AI")) {
				StartCoroutine (Death (sender.GetComponent<PullElement> ()));
				sender.GetComponent<MoneyOwner> ().DropMoney ();
			} else {
				sender.GetComponent<CheckPoint> ().SetInActive ();
			} 
		}
	}

	public void CharDeathNoEffects(GameObject sender){

		GameMenu.Instance.PlayerIsDead();
		Time.timeScale = 0f;
	}

	IEnumerator Death(PullElement em){
		while (true) {
			yield return new WaitForSeconds (3);
			em.Die ();
			break;
		}
	}
}
