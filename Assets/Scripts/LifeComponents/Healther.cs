﻿using UnityEngine;
using System.Collections;

public class Healther : MonoBehaviour {
	public int Health;
	public event GameObjectEvent OnDeath;
	public bool isLived;
	ParticleSystem Blood;
	SliderForUnit SFU;

	float _healthPoints,maxHealth;
	public float HP{
		get{ 
			return _healthPoints;
		}set{
			_healthPoints = Mathf.Clamp (value, 0, Health);

			if (_healthPoints <= 0f) {
				isLived = false;
				if (OnDeath != null)
					OnDeath (gameObject);
			}
		}
	}

	void Awake()
	{
		Blood = transform.GetComponentInChildren<ParticleSystem>();
		if(Blood) Blood.Stop ();
		SFU = GetComponent<SliderForUnit> ();
	}

	void OnEnable()
	{
		SetDefaultHealth ();
	}

	public void SetDefaultHealth(){
		isLived = true;
		HP = Health;
		maxHealth = Health;
		if (SFU) SFU.Value = _healthPoints / maxHealth;
	}

	public void TakeDamage(float dmg){
		if (isLived) {
			//Debug.Log (Blood);
			HP -= dmg;
			if (SFU) SFU.Value = _healthPoints / maxHealth;
			if(Blood) {if (Blood.isPlaying)
				Blood.Stop ();
				Blood.Play ();
			}
		}
	}
}
