﻿using UnityEngine;
using System.Collections;

public class MiniMapDrawer : MonoBehaviour {

	public RenderTexture miniMapTexture;
	public Material miniMapMaterial;
	public Transform miniMapCamera;

	public Transform player;

	[Header("InteractiveObjects")]
	public Transform[] interactiveObjectsPos;
	public Transform gunShop;
	public Transform hospitalPlace;
	[Header("NPCObjects")]
	public Transform[] NPCObjectsPos;
	public Transform zedPos;


	[Header("Icons")]
	public Texture[] iconsForObjects;
	public Texture iconForTarget;

	Vector2 playerPos;
	Vector2 targetPos = Vector2.zero;

	Vector2[] objectsPos;
	Vector2[] objectsPosOnMap;
	Vector2 targetPosOnMap;
	MiniMapObject[] objectsRect;
	MiniMapObject currTarget;


	Transform selfTrans;
	Vector2 renderPos;
	float rad;
	float mapRadius;
	float mapWorldRadius;

	float offset = 10;
	Rect rectMap,rectGun;

	void Awake(){
		SetMapSize ();
	}
	// Use this for initialization
	void Start () {
		init ();
		Init ();
	}

	void Update(){
		playerPos.x = player.position.x;
		playerPos.y = player.position.z;

		for (int i = 0; i < objectsPosOnMap.Length; i++) {
			objectsPosOnMap [i] = objectsPos[i] - playerPos;

			if (objectsPosOnMap [i].sqrMagnitude > 1200 && StaticData.PD.AvailableObjects[i] == 1) {
				objectsRect [i].isVisible = true;

				objectsPosOnMap [i] = objectsPosOnMap [i].normalized;
				objectsPosOnMap [i].y = -objectsPosOnMap [i].y;
				objectsPosOnMap [i] *= mapRadius/2;

				objectsRect[i].icon.center = rectMap.center + objectsPosOnMap [i];
				//Graphics.DrawTexture (objectsRect[i], iconsForObjects[i]);
			}else
				objectsRect [i].isVisible = false;
		}

		targetPosOnMap = targetPos - playerPos;

		if (targetPos != Vector2.zero  && targetPosOnMap.sqrMagnitude > 1200) {
			currTarget.isVisible = true;
			targetPosOnMap = targetPosOnMap.normalized;
			targetPosOnMap.y = -targetPosOnMap.y;
			targetPosOnMap *= mapRadius/2;

			currTarget.icon.center = rectMap.center + targetPosOnMap;
			//Graphics.DrawTexture (objectsRect[i], iconsForObjects[i]);
		}else
			currTarget.isVisible = false;
	}

	void init()
	{
		
		/*//Vector3 temp = miniMapCamera.GetComponent<Camera> ().ScreenToWorldPoint (new Vector3(Screen.width/2,Screen.height/2,0f));
		//Vector3 temp2 = miniMapCamera.GetComponent<Camera> ().ScreenToWorldPoint (new Vector3(Screen.width/2,Screen.height,0f));
		Vector3 temp = miniMapCamera.GetComponent<Camera> ().ViewportToWorldPoint (new Vector3(0.5f,0.5f,0f));
		Vector3 temp2 = miniMapCamera.GetComponent<Camera> ().ViewportToWorldPoint (new Vector3(0.5f,1f,0f));
		Debug.Log (temp2);
		//temp2.z = temp2.y;
		temp2.y = 0f;
		Debug.Log (temp2);
		//Vector3 temp = miniMapCamera.position;
		Debug.Log (temp);
		temp.y=0f;
		Debug.Log (temp);
		rad = (temp2 - temp).sqrMagnitude;
		Debug.Log (rad);*/
	}
	
	// Update is called once per frame
	void OnGUI() {
		if (Event.current.type == EventType.Repaint) {
			//rectMap = new Rect (offset, offset, mapRadius, mapRadius);
			Graphics.DrawTexture (rectMap, miniMapTexture, miniMapMaterial);

			for (int i = 0; i < objectsPosOnMap.Length; i++) {
					//objectsRect [i].center = rectMap.center + objectsPosOnMap [i];
			if (objectsRect [i].isVisible)
				Graphics.DrawTexture (objectsRect [i].icon, objectsRect [i].image);
			}

			if (currTarget.isVisible)
				Graphics.DrawTexture (currTarget.icon, currTarget.image);
		}
	}


	void SetMapSize(){
		mapRadius = Screen.height / 4;
		rectMap = new Rect (offset, offset, mapRadius, mapRadius);
		rectGun = new Rect (0, 0, 15, 15);
		rectGun.center = rectMap.center;
		mapWorldRadius = 40;
	}

	void Init(){
		playerPos.x = player.position.x;
		playerPos.y = player.position.z;

		int size = interactiveObjectsPos.Length + NPCObjectsPos.Length;
		objectsPos = new Vector2[size];
		objectsRect = new MiniMapObject[size];
		objectsPosOnMap = new Vector2[size];
		currTarget = new MiniMapObject ();

		for (int i = 0; i < objectsRect.Length; i++) {
			objectsPos[i].x = interactiveObjectsPos[i].position.x;
			objectsPos[i].y = interactiveObjectsPos[i].position.z;

			objectsRect [i].isVisible = false;
			objectsRect[i].icon = new Rect (0, 0, 20, 20);
			objectsRect [i].image = iconsForObjects [i];
		}

		currTarget.isVisible = false;
		currTarget.icon = new Rect (0, 0, 20, 20);
	}

	public void SetPlayer(Transform target){
		player = target;
	}

	public void SetTarget(Vector3 target, Texture image){
		//Debug.Log ("set target");
		targetPos.x = target.x;
		targetPos.y = target.z;

		currTarget.image = image;

		//currTarget.isVisible = true;
	}

	public void RemoveTarget(){
		currTarget.isVisible = false;
		targetPos = Vector2.zero;
	}
}

public struct MiniMapObject{
	public bool isVisible;
	public Rect icon;
	public Texture image;
}
