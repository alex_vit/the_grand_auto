﻿using UnityEngine;
using System.Collections;

public class MiniMapCameraFollow : MonoBehaviour {
	public Transform Target;
	public PlayerMark playerMark;
	Transform selfTrans;


	public Transform playerIcon;
	// Use this for initialization
	void Awake () {
		selfTrans = transform;
	}
	
	// Update is called once per frame
	void LateUpdate () {
		selfTrans.position = new Vector3 (Target.position.x, selfTrans.position.y, Target.position.z);
	}

	public void SetTarget(Transform newTarget){
		Target = newTarget;
		playerMark.SetTarget (Target);
	}

	public Transform GetTarget(){
		return Target;
	}
}
