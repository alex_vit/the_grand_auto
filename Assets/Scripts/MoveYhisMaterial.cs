﻿using UnityEngine;
using System.Collections;

public class MoveYhisMaterial : MonoBehaviour {
	
	Material arrow;
	
	
	
	// Use this for initialization
	void Start () {

		arrow = GetComponent<Renderer> ().material;

		arrow.mainTextureOffset = new Vector2(0, 0);
		
	}
	
	
	
	// Update is called once per frame
	void FixedUpdate () {
		
		if(arrow.mainTextureOffset.y>1)
			arrow.mainTextureOffset = new Vector2(0, 0);
		else
			arrow.mainTextureOffset = new Vector2(0,arrow.mainTextureOffset.y + Time.fixedDeltaTime);
		
	}
}