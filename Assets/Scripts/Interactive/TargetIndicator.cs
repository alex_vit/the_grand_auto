﻿using UnityEngine;
using System.Collections;

public class TargetIndicator : MonoBehaviour {
	bool isActive;
	Vector3 axe = new Vector3(0,1,0);
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void LateUpdate () {
		if (isActive) {
			//Debug.Log (MissionController.Instance.GetCurrentTargetPoint().GetTransform());
			transform.LookAt (MissionController.Instance.GetCurrentTargetPoint(), axe);
		}
	}

	public void SetActive(){
		gameObject.SetActive (true);
		isActive = true;

	}

	public void SetInActive(){
		gameObject.SetActive (false);
		isActive = false;
	}
}
