﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class TreesLod : MonoBehaviour {

	public float blockSize = 100;

	public float dist = 300;
	const float worldSize = 1500;


	Transform target;

	static Transform parent_obj;

	float intr = 0.3f;
	float t1 = 0;

	Transform[] trees;

	List<TreesBlock> blocks;

	struct TreesBlock{


	
		Transform this_t;

		public TreesBlock(Vector3 p, Transform[] all, float size)
		{
			this_t = (new GameObject().transform);

			this_t.position = p;

			this_t.SetParent(parent_obj);

			foreach(Transform t in all)
			{
				Vector3 v = t.position - this_t.position;

				if(Mathf.Abs(v.x) <= size && Mathf.Abs(v.z) <= size)
					t.SetParent(this_t);
			}


			SetActive(false);
			this_t.gameObject.name = "treesBlocs pos = " + this_t.position;

			if(GetCount() == 0)
				DestroyObj();

		}

		public void SetActive(bool v)
		{
			this_t.gameObject.SetActive(v);

		}

		public Vector3 GetPos()
		{
			return this_t.position;
		}

		public int GetCount()
		{
			return this_t.childCount;
		}

		public void DestroyObj()
		{
			Destroy(this_t.gameObject);
		}
	}


	// Use this for initialization
	void Start () {

		intr = 0.3f * (dist / 50);

		t1 = intr * Random.value;

		parent_obj = (new GameObject().transform);
		parent_obj.name = "TreesLod_" + transform.name;
		blockSize *= QualitySettings.lodBias;
		dist *= QualitySettings.lodBias;

		target = Camera.main.transform;

		blocks = new List<TreesBlock>();


		trees = new Transform[transform.childCount];


		for(int i = 0; i < transform.childCount; i++)
		{
			trees[i] = transform.GetChild(i);
		}


		for(float ix = -worldSize; ix < worldSize; ix += blockSize * 2)
			for(float iy = -worldSize; iy < worldSize; iy += blockSize * 2)
		{
			TreesBlock b = new TreesBlock(new Vector3(ix, 0, iy), trees, blockSize);

			if(b.GetCount()>0)
				blocks.Add(b);

		}

		//foreach(TreesBlock tb in 

		//print ("length = " + blocks.Count);
	}
	
	// Update is called once per frame
	void Update () {

		if(Time.deltaTime < intr)
		   t1 += Time.deltaTime;

		if(t1 < intr)
			return;

		t1 = 0;

		foreach(TreesBlock tb in blocks)
				tb.SetActive( Vector3.Distance(tb.GetPos(), target.position) < dist );

	
	}
}
