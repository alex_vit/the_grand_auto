﻿using UnityEngine;
using System.Collections;
using UnityStandardAssets.Effects;
using UnityStandardAssets.Vehicles.Car;

public class CarLife : MonoBehaviour {
	[SerializeField]
	float lifes = 100;
	[SerializeField]
	bool pozhar = false;

	public Transform vzriv;
	public Transform sparks;
	public Transform fire; 
	Transform rootPlace;

	float destroy_time = 10;

	public event GameObjectEvent OnDeath;

	CheckPoint selfPoint;

	// Use this for initialization
	void Awake(){
		SetDefault ();
	}

	void Start () {
		selfPoint = GetComponent<CheckPoint> ();
		//fire = transform.GetComponentInChildren<ParticleSystemMultiplier>().transform;
	}
	
	// Update is called once per frame
	void Update () {

	}

	void OnEnable(){
		SetDefault ();
	}

	public void SetDefault(){
		lifes = 100;
		pozhar = false;
		fire.gameObject.SetActive(false);
	}

	void OnCollisionEnter(Collision c)
	{
		//print ();

		foreach(ContactPoint cp in c.contacts)
		{
			float damage_vol = 0;

			if(cp.otherCollider.GetComponent<Rigidbody>())
				damage_vol = ((cp.otherCollider.GetComponent<Rigidbody>().velocity - GetComponent<Rigidbody>().velocity).magnitude * 
				              (cp.otherCollider.GetComponent<Rigidbody>().mass) / 1000);
			else
				damage_vol = (GetComponent<Rigidbody>().velocity.magnitude/2);

			SetDamage(damage_vol);

			if(damage_vol > 5)
				Destroy ((Instantiate(sparks, cp.point, sparks.rotation) as Transform).gameObject, 0.5f);
			return;
		}
	}

	public void SetDamage(float v)
	{
		//Debug.Log ("Set car damage");
		lifes -= v;
		if(lifes<=0)
		{
			pozhar = true;
			fire.gameObject.SetActive(true);
			StartCoroutine (Die ());
		}
	}

	IEnumerator Die(){
		while (true) {
			yield return new WaitForSeconds (3f);
			Transform vz = (Transform)Instantiate(vzriv, transform.position, transform.rotation);
			pozhar = false;

			if (selfPoint != null) {
				selfPoint.SetInActive ();

				if (OnDeath != null)
					OnDeath (gameObject);
			} else {
				gameObject.SetActive (false);
			}
			/*if (GetComponent<CheckPoint>() != null) {
				if (OnDeath != null)
					OnDeath (gameObject);
				GetComponent<CheckPoint> ().SetInActive ();
			}else
				Destroy (gameObject);*/
			break;
		}
	}
}
