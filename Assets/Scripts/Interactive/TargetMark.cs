﻿using UnityEngine;
using System.Collections;

public class TargetMark : MonoBehaviour {
	// Use this for initialization
	void Start () {
		Clear ();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void SetMark(GameObject target){
		gameObject.SetActive (true);
		transform.SetParent (target.transform);
		transform.localPosition = new Vector3 (0, 5, 0);

	}

	public void Clear(){
		transform.SetParent (null);
		gameObject.SetActive (false);
	}
}
