﻿using UnityEngine;
using System.Collections;

public class Hospital : MonoBehaviour {
	Animator selfAnim;

	void Awake(){
		selfAnim = GetComponent<Animator> ();
	}

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnCollisionEnter(Collision coll){
		if (coll.gameObject.tag == "Player") {
			coll.gameObject.GetComponent<Healther> ().HP += 50;
		}		
	}
}
