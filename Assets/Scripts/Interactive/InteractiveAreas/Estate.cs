﻿using UnityEngine;
using System.Collections;

public class Estate : MonoBehaviour {
	public int ID;
	public event GameObjectEvent OnBought;
	public Money playerMoney;
	public float intervalTime;
	public int cashForInterval;
	 
	public GameObject pointForStash;
	public TextMesh cashLabel;

	int currentCash = 0; 

	bool enabledForSale;

	EstateState State;

	WaitForSeconds waitForNextStash;

	// Use this for initialization
	void Start () {
		waitForNextStash = new WaitForSeconds (intervalTime);
		CheckIfAvailable ();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void Buy(){
		if (enabledForSale) {
			//isBought = true;
			if (OnBought != null)
				OnBought (gameObject);
		}
	}

	public void SetEnabledForBuy(bool state){
		enabledForSale = state;
	}

	int GetCash(){
		StopAllCoroutines ();
		int res = currentCash;
		currentCash = 0;
		cashLabel.text = "Profit : " + currentCash.ToString ();
		StartCoroutine (Stash ());
		return res;
	}

	public void CheckIfAvailable(){
		//isBought = StaticData.PD.AvailableObjects [2] == 1;
		State = (EstateState)StaticData.PD.AvailableObjects[ID];

		if (State == EstateState.AVAILABLE){
			pointForStash.SetActive (true);
			cashLabel.gameObject.SetActive (true);
			cashLabel.text = "Profit : " + currentCash.ToString ();
			StartCoroutine (Stash ());
		}
	}

	void OnTriggerEnter(Collider coll){
		if (coll.gameObject.CompareTag ("Player")) { 
			
			if (currentCash > 0)
				playerMoney.Cash += GetCash ();
		}
	}

	IEnumerator Stash(){
		while (true) {
			yield return waitForNextStash;
			currentCash += cashForInterval;
			cashLabel.text = "Profit : " + currentCash.ToString ();
		}
	}
}

public enum EstateState{
	MOOT,
	AVAILABLE,
	UNAVAILABLE
}
