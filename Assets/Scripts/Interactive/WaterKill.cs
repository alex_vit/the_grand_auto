﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityStandardAssets.Vehicles.Car;
using UnityStandardAssets.Characters.ThirdPerson;

public class WaterKill : MonoBehaviour {
    public Healther player;
    public GameObject DeathText, RestartButtonDeath, WatchVideo;
    int countVideo = 0;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void OnTriggerEnter(Collider c)
    {

        if (c.transform.root.GetComponent<CarUserMover>() && c.transform.root.GetComponent<CarUserMover>().IsPlayerControlled())
        {

            if (player.isLived)
                player.TakeDamage(1000);

            Time.timeScale = 0f;
            //CameraFollowChar.inst.enabled = false;

            GameMenu.Instance.userPanel.SetActive(false);
            GameMenu.Instance.state = GameMenu.State.Pause;

            //GameObject.Find("UserPanel").gameObject.SetActive(false);
        }

        if (c.transform.root.GetComponent<Healther>() != null)
        {
            if (c.transform.root.GetComponent<Healther>().isLived)
                c.transform.root.GetComponent<Healther>().TakeDamage(1000);
        }

        //if (c.transform.root.GetComponent<CharLife> () && c.transform.root.GetComponent<CharLife> ().enabled) {
        //c.transform.root.GetComponent<CharLife> ().SetDamage(10000);
        //}
    }
}
