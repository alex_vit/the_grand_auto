﻿using UnityEngine;
using System.Collections;

public class ExplosionDamage : MonoBehaviour {

	// Use this for initialization
	void Awake () {
	
		foreach (Collider cl in Physics.OverlapSphere(transform.position, 10)) {
			if (cl.transform.root.GetComponent<CarLife> ())
				cl.transform.root.GetComponent<CarLife> ().SetDamage (2000 / Vector3.Distance (cl.transform.position, transform.position));

			//if (cl.transform.root.GetComponent<CharLife> ())
				//cl.transform.root.GetComponent<CharLife> ().SetDamage (500 / Vector3.Distance (cl.transform.position, transform.position));
		}

	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
