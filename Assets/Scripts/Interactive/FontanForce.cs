﻿using UnityEngine;
using System.Collections;

public class FontanForce : MonoBehaviour {

	// Use this for initialization
	void Start () {
		RaycastHit rch;
		if (Physics.Raycast (transform.position, Vector3.down, out rch, 1)) 
		{

			transform.position = rch.point;

		}
	}
	
	// Update is called once per frame
	void FixedUpdate () {
	
		RaycastHit rch;

		if (Physics.Raycast (transform.position, Vector3.up, out rch, 2)) {

			if(rch.collider.transform.root.GetComponent<Rigidbody>())
				rch.collider.transform.root.GetComponent<Rigidbody>().AddForceAtPosition(Vector3.up * 1000, rch.point, ForceMode.Impulse);

		}



	}
}
