﻿using UnityEngine;
using System.Collections;

public class EscCatcherGame : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown (KeyCode.Escape)) {
			if (GameMenu.Instance.state != GameMenu.State.Pause)
				GameMenu.Instance.OnPause ();
			else GameMenu.Instance.OnBack();
		}
	}
}
