﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Money : MonoBehaviour {
	public Text moneyLabelText;
	int _money = 0;

	public int Cash{
		get{
			return _money;
		}set{
			//_oldMoney = _money;
			//_money = value;
			//StaticData.PD.Money = _money;
			//StaticData.PD.SaveOnDisk ();
			/*if (_money != 0) {
				if (gameObject.activeSelf) {
					StartCoroutine (AnimateCash (value));
				}
			}
			else
				moneyLabelText.text = _money.ToString ();*/
			StopAllCoroutines ();
			if (value > _money)
				StartCoroutine (IncreaseCash (value));
			else
				StartCoroutine (DecreaseCash (value));
		}
	}
	void Awake(){
		//get saved money
		//Cash = 0;
	}

	// Use this for initialization
	void Start () {
		Cash = StaticData.PD.Money;
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	int CalculateStep(int value){
		//Debug.Log ("cash value :" + value);
		value = Mathf.Abs (value);
		if (value / 10000 >= 1) 
			return (int)CashStep.THOUSANDS;
		if (value / 1000 >= 1)
			return (int)CashStep.HUNDREDS;
		if (value / 100 >= 1)
			return (int)CashStep.DECIMAL;
		if (value / 10 >= 1)
			return (int)CashStep.ONES;
		else
			return 0;  	
	}

	IEnumerator IncreaseCash(int currCash){
		int step = CalculateStep (_money - currCash);
		while (true) {
			if (_money >= currCash) {
				_money = currCash;
				StaticData.PD.Money = _money;
				moneyLabelText.text = _money.ToString ();
				break;
			}
			_money += step;
			moneyLabelText.text = _money.ToString ();
			yield return new WaitForEndOfFrame ();
		}



		/*while (_oldMoney != _money) {
			_oldMoney += 10;
			moneyLabelText.text = _oldMoney.ToString (); 
			yield return new WaitForSeconds (0.04f);
		}*/
	}


	IEnumerator DecreaseCash(int currCash){
		int step = CalculateStep (_money - currCash);
		while (true) {
			if (_money <= currCash) {
				_money = currCash;
				StaticData.PD.Money = _money;
				moneyLabelText.text = _money.ToString ();
				break;
			}
			_money -= step;
			moneyLabelText.text = _money.ToString ();
			yield return new WaitForEndOfFrame ();
		}



		/*while (_oldMoney != _money) {
			_oldMoney += 10;
			moneyLabelText.text = _oldMoney.ToString (); 
			yield return new WaitForSeconds (0.04f);
		}*/
	}

	enum CashStep{
		ONES = 1,
		DECIMAL = 10,
		HUNDREDS = 100,
		THOUSANDS = 1000
	}
}
