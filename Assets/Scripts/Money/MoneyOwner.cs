﻿using UnityEngine;
using System.Collections;

public class MoneyOwner : MonoBehaviour {
	public System.Action<int> OnValueChanged;
	public bool isPlayer;
	int _money = 0;

	public int Cash{
		get{
			return _money;
		}set{
			if (isPlayer) {
				if (value < 0)
					value = 0;
				StaticData.PD.Money = value;
				_money = value;
				if (OnValueChanged != null)
					OnValueChanged (_money);
			} else
				_money = value;
		}
	}

	void Awake(){

	}

	// Use this for initialization
	void Start () {
		Cash = StaticData.PD.Money;
		//Cash = 1000;
	}

	// Update is called once per frame
	void Update () {

	}

	public void DropMoney(){
		//MoneyCash obj = Instantiate (StaticData.LoadPrefab ("MoneyChange")).GetComponent<MoneyCash>();
		//obj.SetPosition (transform.position);
	}
}
