﻿using UnityEngine;
using System.Collections;

public class MoneyCash : MonoBehaviour {
	// Use this for initialization
	void Start () {

	}

	// Update is called once per frame
	void Update () {

	}

	public void SetPosition(Vector3 pos){
		transform.position = new Vector3 (pos.x, -0.5f, pos.z);
	}

	void OnTriggerEnter(Collider coll){
		if (coll.GetComponent<MoneyOwner> () && coll.GetComponent<Player>()) {
			coll.GetComponent<MoneyOwner> ().Cash += Random.Range(20,40);
			Destroy (gameObject);
		}
	}
}
