﻿using UnityEngine;
using System.Collections;

public class Tracker : MonoBehaviour {
    [SerializeField]
    Transform target;
    Transform selfTrans;
    // Use this for initialization
    void Awake()
    {
        selfTrans = transform;
    }

    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        Track();
    }

    public void SetTarget(Transform t)
    {
        target = t;
    }

    void Track()
    {
        if (target)
            selfTrans.position = target.position;
    }
}
