﻿using UnityEngine;
using System.Collections;

public class CarUserMover : MonoBehaviour {
    [SerializeField]
    bool isPlayerControlled;
	[SerializeField]
	bool isRiding;

    TSNavigation selfNavigation;
    TSTrafficAI selfAI;
    TSSimpleCar selfCar;
    AudioListener selfAudio;
    AudioSource selfSound;
    Rigidbody selfRigid;

    void Awake()
    {
        selfNavigation = GetComponent<TSNavigation>();
        selfAI = GetComponent<TSTrafficAI>();
        selfCar = GetComponent<TSSimpleCar>();
        selfRigid = GetComponent<Rigidbody>();
        selfAudio = GetComponent<AudioListener>();
        selfSound = GetComponent<AudioSource>();
    }

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void GetInCar()
    {
        selfNavigation.enabled = false;
        selfAI.enabled = false;
        selfCar.playerControlled = true;
        selfAudio.enabled = true;
        selfSound.volume = 1f;
        //selfCarController.enabled = true;
        //selfUserControl.enabled = true;

        isPlayerControlled = true;
    }

    public void GetOutOfCar()
    {
        selfNavigation.enabled = false;
        selfAI.enabled = false;
        selfCar.playerControlled = false;
        selfRigid.velocity = Vector3.zero;
        selfAudio.enabled = false;
        selfSound.volume = 0.2f;
        //selfCarController.enabled = false;
        //selfUserControl.enabled = false;

        isPlayerControlled = false;
    }

    public bool IsPlayerControlled()
    {
        return isPlayerControlled;
    }

	public bool IsCarRiding(){
		isRiding = (selfRigid.velocity.magnitude > 1);
		return isRiding;
	}
}
