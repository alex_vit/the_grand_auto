﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityStandardAssets.CrossPlatformInput;

public class CarMotionController : BaseMotionController {
	public static CarMotionController Instance;
	public Text stub;

	//Transform playerTrans;

	//float screenWidth;
	//float screenHeight;

	/*float deltaX;
	float deltaY;

	Vector3 startPosMovement;

	float xCoord = 0;

	float angular = 0;

	float minAngle = -50;
	float maxAngle = 50;*/
	float angular = 0;
	bool playerJump;

	bool isMoving;

	CrossPlatformInputManager.VirtualAxis verticalAxis;
	CrossPlatformInputManager.VirtualAxis horizontalAxis;

	// Use this for initialization
	void Awake(){
		//playerBody = Player.GetComponent<Rigidbody> ();
		//currentAngle = playerTrans.eulerAngles;

		if (Instance == null)
			Instance = this;
		else
			Destroy (gameObject); 
		
	}

	void Start () {		
		SetScreenSize ();
	}

	// Update is called once per frame
	/*void Update () {
		if (!playerJump)
		{
			playerJump = CrossPlatformInputManager.GetButtonDown("Jump");
		}
	}*/

	void FixedUpdate(){
		//if(isActive)
			//ParseTouches ();
	}

	void OnEnable(){
		isMoving = false;
		RegisterVirtualAxis ();
	}

	void OnDisable(){
		UnRegisterVirtualAxis ();
	}

	public override void SetTarget(Transform target){
		currentTarget = target;
	}

	protected override void RegisterVirtualAxis(){
		//if (!CrossPlatformInputManager.AxisExists (verticalAxisName)) {
			//verticalAxis = new CrossPlatformInputManager.VirtualAxis (verticalAxisName);
			//CrossPlatformInputManager.RegisterVirtualAxis (verticalAxis);
		//} else
			//verticalAxis = CrossPlatformInputManager.VirtualAxisReference (verticalAxisName);
	}


	protected override void UnRegisterVirtualAxis(){
		//verticalAxis.Remove ();
	}

	protected override void UpdateVirtualAxis(Vector3 value){
		//delta = startPosMovement - value;
		//delta = new Vector3 (delta.x, Mathf.Abs (delta.y), delta.z);
		//delta.y = -delta.y;
		//delta.y = Mathf.Clamp (delta.y, 0, 100000);
		//delta /= MovementRange;
		//verticalAxis.Update (delta.y);
	}

	protected virtual void ParseTouches(){
		//stub.text = ("x : " + Input.GetTouch (0).position.x + " " + "y : " + Input.GetTouch (0).position.y);

		foreach (Touch t in Input.touches) {
			//find area where touched
			if (t.position.x > screenWidth /2 && t.position.y < screenHeight*  2/3) {

				if (t.phase == TouchPhase.Began)
					xCoord = t.position.x;
				else if (t.phase == TouchPhase.Moved) {
					deltaX = t.position.x - xCoord;

					xCoord = t.position.x;
					angular += deltaX;
					RotateTarget ();
				} else if (t.phase == TouchPhase.Ended) {
					xCoord = t.position.x;
					angular = 0;
				} else if (t.phase == TouchPhase.Stationary) {
					RotateTarget ();
				}
			} else if (t.position.x < screenWidth / 2 && t.position.y < screenHeight *2/ 3){
				if (t.phase == TouchPhase.Began) {
					isMoving = true;
					startPosMovement = t.position;
				}
				else if (t.phase == TouchPhase.Moved) {
					isMoving = true;
					UpdateVirtualAxis(t.position);
				} else if (t.phase == TouchPhase.Stationary) {
					UpdateVirtualAxis (t.position);
				}
				else if (t.phase == TouchPhase.Ended) {
					isMoving = false;
					UpdateVirtualAxis (startPosMovement);
				}
			}
		}
	} 


	void RotateTarget(){
		if(isMoving){
			stub.text = ("ang :" + Mathf.Clamp (angular, minAngle, maxAngle));
			currentTarget.RotateAround (currentTarget.position, new Vector3 (0, 1, 0), Mathf.Clamp (angular, minAngle, maxAngle) * Time.deltaTime);
		}
	}
}
