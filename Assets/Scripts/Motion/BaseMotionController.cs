﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System;
using UnityStandardAssets.Characters.ThirdPerson;
using UnityStandardAssets.CrossPlatformInput;

public class BaseMotionController : MonoBehaviour {
	[SerializeField]
	bool _standAloneInput;
	[SerializeField]
	bool _mobileInput;


	protected Transform currentTarget;

	protected float screenWidth;
	protected float screenHeight;

	protected float deltaX;
	protected float deltaY;

	protected Vector3 startPosMovement;

	protected float xCoord = 0;

	protected float minAngle = -75;
	protected float maxAngle = 75;

	protected string verticalAxisName = "Vertical";
	protected string horisontalAxisName = "Horizontal";
	protected Vector3 delta;
	public  int MovementRange = 100;

	public bool isActive;

	// Use this for initialization
	void Awake(){
		//SetTarget (Player.transform);
		//playerBody = Player.GetComponent<Rigidbody> ();
		//currentAngle = playerTrans.eulerAngles;
	}

	void Start () {		
		SetScreenSize ();
	}
	
	// Update is called once per frame
	void Update () {

	}
		
	void FixedUpdate(){
		ParseTouches ();
	}

	void OnEnable(){
		RegisterVirtualAxis ();
	}

	void OnDisable(){
		UnRegisterVirtualAxis ();
	}


	public virtual void SetTarget(Transform target){
		
	}

	protected  void SetScreenSize(){
		screenWidth = Screen.width;
		screenHeight = Screen.height;
	}

	protected virtual void RegisterVirtualAxis(){
	}


	protected virtual void UnRegisterVirtualAxis(){
	}

	protected virtual void UpdateVirtualAxis(Vector3 value){
	}
		
	protected virtual void ParseTouches(){
	} 
}
