﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityStandardAssets.CrossPlatformInput;

public class PlayerMotionController : BaseMotionController {
	public static PlayerMotionController Instance;
	public Text stub;

	//Transform playerTrans;

	//float screenWidth;
	//float screenHeight;

	/*float deltaX;
	float deltaY;

	Vector3 startPosMovement;

	float xCoord = 0;

	float angular = 0;

	float minAngle = -50;
	float maxAngle = 50;*/
	float angular = 0;
	bool playerJump;
	Touch[] touches;

	CrossPlatformInputManager.VirtualAxis verticalAxis;
	CrossPlatformInputManager.VirtualAxis horisontAxis;

	// Use this for initialization
	void Awake(){
		//SetTarget (Player.transform);
		//playerBody = Player.GetComponent<Rigidbody> ();
		//currentAngle = playerTrans.eulerAngles;

		if (Instance == null)
			Instance = this;
		else
			Destroy (gameObject);
	}

	void Start () {		
		SetScreenSize ();
	}

	// Update is called once per frame
	void Update () {
		if (!playerJump)
		{
			playerJump = CrossPlatformInputManager.GetButtonDown("Jump");
		}
		if(isActive)
			ParseTouches ();
	}

	void FixedUpdate(){
		
	}

	void OnEnable(){
		RegisterVirtualAxis ();
	}

	void OnDisable(){
		UnRegisterVirtualAxis ();
	}

	public override void SetTarget(Transform target){
		currentTarget = target;
	}

	protected override void RegisterVirtualAxis(){
		/*if (!CrossPlatformInputManager.AxisExists (verticalAxisName)) {
			verticalAxis = new CrossPlatformInputManager.VirtualAxis (verticalAxisName);
			CrossPlatformInputManager.RegisterVirtualAxis (verticalAxis);
		} else
			verticalAxis = CrossPlatformInputManager.VirtualAxisReference (verticalAxisName);

		if (!CrossPlatformInputManager.AxisExists (horisontalAxisName)) {
			horisontAxis = new CrossPlatformInputManager.VirtualAxis (horisontalAxisName);
			CrossPlatformInputManager.RegisterVirtualAxis (horisontAxis);
		} else
			horisontAxis = CrossPlatformInputManager.VirtualAxisReference (horisontalAxisName);*/
	}


	protected override void UnRegisterVirtualAxis(){
		//verticalAxis.Remove ();
		//horisontAxis.Remove ();
	}

	protected override void UpdateVirtualAxis(Vector3 value){
		/*delta = startPosMovement - value;
		//delta = new Vector3 (delta.x, Mathf.Abs (delta.y), delta.z);
		delta.y = -delta.y;
		//delta.y = Mathf.Clamp (delta.y, 0, 100000);
		delta /= MovementRange;

		verticalAxis.Update (delta.y);
		horisontAxis.Update (-delta.x);*/
	}

	protected virtual void ParseTouches(){
		//stub.text = ("x : " + Input.GetTouch (0).position.x + " " + "y : " + Input.GetTouch (0).position.y);
		touches = Input.touches;

		for (int i=0; i < touches.Length; i++) {
			Touch t = touches [i];
			//find area where touched
			/*if (t.position.x > screenWidth /2 && t.position.y <= screenHeight*  1/2) {

				if (t.phase == TouchPhase.Began)
					xCoord = t.position.x;
				else if (t.phase == TouchPhase.Moved) {
					deltaX = t.position.x - xCoord;

					xCoord = t.position.x;
					angular += deltaX;
					RotateTarget ();
				} else if (t.phase == TouchPhase.Ended) {
					xCoord = t.position.x;
					angular = 0;
				} else if (t.phase == TouchPhase.Stationary) {
					RotateTarget ();
				}
			} else*/ 
			if (t.position.x < screenWidth / 2 && t.position.y <= screenHeight * 1 / 2) {
				if (t.phase == TouchPhase.Began)
					startPosMovement = t.position;
				else if (t.phase == TouchPhase.Moved) {
					UpdateVirtualAxis (t.position);
				} else if (t.phase == TouchPhase.Stationary) {
					UpdateVirtualAxis (t.position);
				} else if (t.phase == TouchPhase.Ended) {
					UpdateVirtualAxis (startPosMovement);
				}
			} else if (t.position.x > screenWidth / 2 && t.position.y <= screenHeight * 1 / 2) {
				if (t.phase == TouchPhase.Began)
					startPosMovement = t.position;
				else if (t.phase == TouchPhase.Moved) {
					UpdateVirtualAxis (t.position);
				} else if (t.phase == TouchPhase.Stationary) {
					UpdateVirtualAxis (t.position);
				} else if (t.phase == TouchPhase.Ended) {
					UpdateVirtualAxis (startPosMovement);
				}
			}
		}
	} 


	void RotateTarget(){
		stub.text = ("ang :" + Mathf.Clamp(angular*10, minAngle, maxAngle));
		currentTarget.RotateAround (currentTarget.position, new Vector3 (0, 1, 0),Mathf.Clamp(angular*10, minAngle, maxAngle) * Time.deltaTime);
	}
}
