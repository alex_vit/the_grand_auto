﻿using UnityEngine;
using System.Collections;

public class MotionController : MonoBehaviour {
	public static MotionController Instance;

	BaseMotionController currentController;
	//refrences for different controllers

	public BaseMotionController playerCtrl;
	public BaseMotionController carCtrl;

	public GameObject player;

	void Awake(){
		if (Instance == null)
			Instance = this;
		else
			Destroy (gameObject);


	}

	// Use this for initialization
	void Start () {
		SelectPlayerController (player);
		//it's default controller;
		UIControlViewer.inst.OnGetInCar += SelectCarController;
		UIControlViewer.inst.OnGetOutOfCar += SelectPlayerController;
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnEnable(){
		if (UIControlViewer.inst) {
			
		}
	}

	void OnDisable(){
		if (this.enabled) {
			UIControlViewer.inst.OnGetInCar -= SelectCarController;
			UIControlViewer.inst.OnGetOutOfCar -= SelectPlayerController;		
		}
	}

	public BaseMotionController GetCurrentController(){
		return currentController;
	}

	public MotionType GetMotionType(){
		if (currentController == playerCtrl)
			return MotionType.WALK;
		else if (currentController == carCtrl)
			return MotionType.CAR;
		else
			return MotionType.WALK;
	}

	void SelectPlayerController(GameObject sender){
		//carCtrl.enabled = false;
		//playerCtrl.enabled = true;
		if(currentController != null)
			currentController.isActive = false;
		currentController = playerCtrl;
		currentController.isActive = true;
		currentController.SetTarget (player.transform);
		if(SpawnMovableCarsController.Instance)
		SpawnMovableCarsController.Instance.SetPlayer (player.transform);
	}

	void SelectCarController(GameObject sender){
		//playerCtrl.enabled = false;
		//carCtrl.enabled = true;
		if(currentController != null)
			currentController.isActive = false;
		currentController = carCtrl;
		currentController.isActive = true;
		currentController.SetTarget (sender.transform);
		if(SpawnMovableCarsController.Instance)
		SpawnMovableCarsController.Instance.SetPlayer (sender.transform);
	}
}

public enum MotionType{
	CAR,
	WALK
}
