﻿using UnityEngine;
using System.Collections;

public class CarMover : MonoBehaviour {
	Transform selfTrans;
	Transform wayPoint;
	NavMeshAgent selfNavAgent;

	float distance;
	// Use this for initialization
	void Awake(){
		selfTrans = transform;
		selfNavAgent = GetComponent<NavMeshAgent> ();
	}

	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void FixedUpdate(){
		//selfNavAgent.SetDestination(
		CheckDestination();
	}

	public void SetTarget(Otmetka otmetka){
		if (otmetka != null) {
			wayPoint = otmetka.transform;

			if(selfNavAgent.isActiveAndEnabled)
				selfNavAgent.SetDestination (wayPoint.position);

			selfNavAgent.speed = 15;
		}
	}

	void CheckDestination(){
		distance = Vector3.Distance (wayPoint.position, selfTrans.position);
		//Debug.Log (name);
		//Debug.Log (distance <= selfNavAgent.speed*Time.deltaTime);
		//Debug.Log (distance);
		//Debug.Log (selfNavAgent.speed*Time.deltaTime);
		if (distance <= selfNavAgent.speed*Time.deltaTime+selfNavAgent.radius) {
			SetTarget (wayPoint.GetComponent<Otmetka> ().GetNext ());
		}			
	}

}
