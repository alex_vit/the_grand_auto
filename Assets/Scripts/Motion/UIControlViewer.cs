﻿using UnityEngine;
using System.Collections;
using UnityStandardAssets.Vehicles.Car;
using UnityStandardAssets.Cameras;

/// <summary>
/// This class for control what current UI control element needs show
/// </summary>

public class UIControlViewer : MonoBehaviour {
	public CameraSwitcher cameraSwitch;
	public MiniMapCameraFollow mapTracker;
	public MiniMapDrawer mapDrawer;

	public FreeLookCam CameraChar;
	public AutoCam CameraCar;
	public CarControllerButtons carController;

	public GameObject carUIController;
	public GameObject charUIController;
	public Rigidbody player;

    public Tracker trafficSpawner;

	public TouchPanel touchPanel;



	Transform target;

	public static UIControlViewer inst;


	public event GameObjectEvent OnGetInCar;
	public event GameObjectEvent OnGetOutOfCar;



	bool isShowAdNow;

	void Awake(){
		if (inst == null)
			inst = this;
		else
			Destroy (gameObject);

		isShowAdNow = Random.Range (0, 2) == 0;
	}

	// Use this for initialization
	void Start () {
		mapTracker.SetTarget (player.transform);
		carUIController.SetActive (false);
		charUIController.SetActive (true);


	}

	public void OnCarEnterBtnWasClicked(){
		if (CameraCar.enabled)
			GetOutCar ();
		else
			GetInCar ();
	}

	public void SetTarget(Transform t)
	{
		if (target == null) {
			target = t;
		}
		else if (t != target) {
			target = t;
		}
	}
		
	public void GetOutCar(){
        target.GetComponent<CarUserMover>().GetOutOfCar();
		carController.Hide();

		player.gameObject.SetActive(true);
		mapTracker.SetTarget (player.transform);
		mapDrawer.SetPlayer (player.transform);
		player.GetComponent<AudioListener> ().enabled = true;
		player.position = target.position + target.right * 2;

		cameraSwitch.SetTarget (player.transform);

		cameraSwitch.Mode = CameraMode.THIRD_PERSON;

		carUIController.SetActive (false);
		charUIController.SetActive (true);

		touchPanel.Show ();

		if (OnGetOutOfCar != null)
			OnGetOutOfCar (target.gameObject);

		isShowAdNow = Random.Range (0, 2) == 0;
		AdGenerator.Instance.ShowCarAd (isShowAdNow);

        trafficSpawner.SetTarget(player.transform);
	}

	private void GetInCar(){
		carController.Show();
		player.GetComponent<AudioListener> ().enabled = false;
		player.gameObject.SetActive(false);
		mapTracker.SetTarget (target);
		mapDrawer.SetPlayer (target);
		cameraSwitch.SetTarget(target);
		cameraSwitch.Mode = CameraMode.CAR_MODE;

		touchPanel.Hide ();

        target.GetComponent<CarUserMover>().GetInCar();
		//target.GetComponent<CarUserControl>().enabled = true;
		//target.GetComponent<AudioListener> ().enabled = true;
		//target.GetComponent<CarController>().ForceInitStart();

		//target.GetComponent<CarController>().Move(0,-1,0,0);
		//target.GetComponent<CarController>().Move(0,1,0,0);

		charUIController.SetActive (false);
		carUIController.SetActive (true);

		if (OnGetInCar != null)
			OnGetInCar (target.gameObject);

		AdGenerator.Instance.ShowCarAd (!isShowAdNow);

        trafficSpawner.SetTarget(target);
	}

	public void DeathInvoke(){
		if (CameraCar.enabled) {
			isShowAdNow = true;
			GetOutCar ();
		}
	}
}
