﻿using UnityEngine;
using System.Collections;
using UnityStandardAssets.Characters.ThirdPerson;

public class Character : MonoBehaviour {
    Healther selfHealth;
    ThirdPersonCharacter selfChar;
    Animator selfAnim;
    // Use this for initialization
    void Awake() {
        selfHealth = GetComponent<Healther>();
        selfChar = GetComponent<ThirdPersonCharacter>();
        selfAnim = GetComponent<Animator>();
    }

    void Start() {

    }

    // Update is called once per frame
    void Update() {

    }

    void OnCollisionEnter(Collision coll) {
        CarUserMover car = coll.gameObject.GetComponent<CarUserMover>();

        if (car != null && car.IsCarRiding()) {
            selfHealth.HP -= 20;
            FallBack();
        }
    }

    void FallBack() {
        Debug.Log("fall back");
        selfChar.enabled = false;
        selfAnim.enabled = false;
        GetComponentInChildren<RagdollEnabler>().ActivateRagdoll();
        Invoke("GetUp", 5f);
    }

    void GetUp() {
        selfChar.enabled = true;
        selfAnim.enabled = true;
        GetComponentInChildren<RagdollEnabler>().DisableRagdoll();
        selfChar.Move(Vector3.zero, false, false);
    }
}
