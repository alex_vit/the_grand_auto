﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Peshehodes : MonoBehaviour {

	public Transform[] chars_prefs;
	
	
	int limit = 10;
	
	float dist = 40;
	
	Transform[] marks;
	
	List<Transform> chars;
	
	Transform target;
	
	float intr = 1.3f;
	float t1 = 0;

	// Use this for initialization
	void Start () {
		t1 = intr * Random.value;
		//limit = 30;
		//dist = 300;

		chars = new List<Transform>();
		
		target = Camera.main.transform;
		
		marks = new Transform[transform.childCount];
		
		for(int i = 0; i < transform.childCount; i++)
		{
			marks[i] = transform.GetChild(i);
			Destroy (marks[i].GetComponent<Renderer>());
			//Destroy (marks[i].GetComponent<Renderer>());
			
		}
	}
	
	// Update is called once per frame
	void Update () {
		
		t1 += Time.deltaTime;
		
		if(t1 < intr)
			return;
		
		t1 = 0;
		
		if(chars.Count < limit)
			foreach(Transform mrk in marks)
				if(Vector3.Distance(mrk.position, target.position) < dist && Vector3.Distance(mrk.position, target.position) > dist / 2)
			{
				bool zaniato = false;
				foreach(Collider c in Physics.OverlapSphere(mrk.position, 3))
					if(c.transform.root.GetComponent<Rigidbody>())
						zaniato = true;
				
				if(!zaniato)
				{
					RaycastHit rh;

					if(Physics.Raycast(mrk.position, Vector3.down, out rh, 3))
					{
						chars.Add((Transform)Instantiate(chars_prefs[Random.Range(0, chars_prefs.Length)], rh.point, mrk.rotation));
						return;
					}
					//chars[chars.Count-1].localScale = new Vector3(15,15,15);
					//cars[cars.Count - 1].GetComponent<CarController>().Move(0,0,0,1);
					
				}
				
				//if(Physics.OverlapSphere(mrk.position, 10).Length==0)

			}
		
		for(int i = 0; i < chars.Count; i++)
		{
			
			if(chars[i] == null)
			{			
				chars.RemoveAt(i);
				
				i--;
			}
			else
				if(Vector3.Distance(chars[i].position, target.position) > dist)
				{
					if(chars[i] != null)
						Destroy (chars[i].gameObject);
					
					chars.RemoveAt(i);
					
					i--;
				}

		}
	}
}
