﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class RagdollEnabler : MonoBehaviour {
	
	private List<CharacterJointDisabler> _jointDisablers;
	
	private void Awake () 
	{
		foreach (Rigidbody rb in GetComponentsInChildren<Rigidbody>())
			rb.collisionDetectionMode = CollisionDetectionMode.ContinuousDynamic;

		CharacterJoint[] characterJoints = transform.GetComponentsInChildren<CharacterJoint>();
		
		_jointDisablers = new List<CharacterJointDisabler>();
		for(int i=0; i<characterJoints.Length; ++i)
		{
			characterJoints[i].GetComponent<Collider>().enabled = false;
			characterJoints[i].GetComponent<Rigidbody>().isKinematic = true;
			
			CharacterJointDisabler disabler = characterJoints[i].gameObject.AddComponent<CharacterJointDisabler>();
			disabler.CopyValuesAndDestroyJoint(characterJoints[i]);
			_jointDisablers.Add(disabler);
		}

		GetComponent<Rigidbody> ().useGravity = false;
		GetComponent<Collider> ().enabled = false;
	}

	public Rigidbody ActivateRagdoll ()
	{
		
		GetComponent<Rigidbody> ().useGravity = true;
		GetComponent<Rigidbody> ().isKinematic = false;
		GetComponent<Collider> ().enabled = true;

		GetComponent<Rigidbody>().constraints = RigidbodyConstraints.None;
	

		for(int i=0; i<_jointDisablers.Count; ++i)
		{
			_jointDisablers[i].GetComponent<Collider>().enabled = true;
			_jointDisablers[i].GetComponent<Rigidbody>().isKinematic = false;
			
			_jointDisablers[i].CreateJointAndDestoryThis();

		}

		return GetComponent<Rigidbody> ();
	}
	public void DisableRagdoll ()
	{
		
		GetComponent<Rigidbody> ().useGravity = false;
		GetComponent<Rigidbody> ().isKinematic = true;
		GetComponent<Collider> ().enabled = false;
		
		GetComponent<Rigidbody>().constraints = RigidbodyConstraints.None;
		
		
		for(int i=0; i<_jointDisablers.Count; ++i)
		{
			_jointDisablers[i].GetComponent<Collider>().enabled = false;
			_jointDisablers[i].GetComponent<Rigidbody>().isKinematic = true;
			
			//_jointDisablers[i].CreateJointAndDestoryThis();
			
		}
		
		//return GetComponent<Rigidbody> ();
	}
}


public class CharacterJointDisabler : MonoBehaviour 
{
	private Rigidbody ConnectedBody;
	private Vector3 Anchor;
	private Vector3 Axis;
	private bool AutoConfigureConnectedAnchor;
	private Vector3 ConnectedAnchor;
	private Vector3 SwingAxis;
	private SoftJointLimit LowTwistLimit;
	private SoftJointLimit HighTwistLimit;
	private SoftJointLimit Swing1Limit;
	private SoftJointLimit Swing2Limit;
	private float BreakForce;
	private float BreakTorque;
	private bool EnableCollision;
	
	public void CopyValuesAndDestroyJoint(CharacterJoint characterJoint)
	{
		ConnectedBody = characterJoint.connectedBody;
		Anchor = characterJoint.anchor;
		Axis = characterJoint.axis;
		AutoConfigureConnectedAnchor = characterJoint.autoConfigureConnectedAnchor;
		ConnectedAnchor = characterJoint.connectedAnchor;
		SwingAxis = characterJoint.swingAxis;
		LowTwistLimit = characterJoint.lowTwistLimit;
		HighTwistLimit = characterJoint.highTwistLimit;
		Swing1Limit = characterJoint.swing1Limit;
		Swing2Limit = characterJoint.swing2Limit;
		BreakForce = characterJoint.breakForce;
		BreakTorque = characterJoint.breakTorque;
		EnableCollision = characterJoint.enableCollision;
		
		Destroy (characterJoint);
	}
	
	public void CreateJointAndDestoryThis()
	{
		CharacterJoint characterJoint = gameObject.AddComponent<CharacterJoint>();
		
		characterJoint.connectedBody = ConnectedBody;
		characterJoint.anchor = Anchor;
		characterJoint.axis = Axis;
		characterJoint.autoConfigureConnectedAnchor = AutoConfigureConnectedAnchor;
		characterJoint.connectedAnchor = ConnectedAnchor;
		characterJoint.swingAxis = SwingAxis;
		characterJoint.lowTwistLimit = LowTwistLimit;
		characterJoint.highTwistLimit = HighTwistLimit;
		characterJoint.swing1Limit = Swing1Limit;
		characterJoint.swing2Limit = Swing2Limit;
		characterJoint.breakForce = BreakForce;
		characterJoint.breakTorque = BreakTorque;
		characterJoint.enableCollision = EnableCollision;
		characterJoint.enableProjection = enabled;
		//Destroy(this);
	}
	
	
}