﻿using UnityEngine;
using System.Collections;

public class MenuCameraRotator : MonoBehaviour {

	public float RotationAmount;
	private Vector3 RotationVector;

	void Awake ()
	{
		RotationVector = new Vector3 (0, RotationAmount, 0);
	}
	void Update () 
	{
		this.transform.Rotate(RotationVector);
	}
}
