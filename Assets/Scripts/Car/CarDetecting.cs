﻿using UnityEngine;
using System.Collections.Generic;
using System.Linq;

public class CarDetecting : MonoBehaviour {
	
	public UIControlViewer carCtrl;
	public UnityEngine.UI.Button carBtn;

	private Transform currTarget;
	private CarUserMover currCar;
	private Transform selfTrans;
	private List<Collider> cldrs;

	void Awake(){
		selfTrans = transform;
	}
	
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		DetectCar ();
	}


	void DetectCar(){
		cldrs = Physics.OverlapSphere (selfTrans.position, 2).ToList();

		if (cldrs != null && cldrs.Count > 0) {
			foreach (Collider cld in cldrs) {
				currTarget = cld.transform;
				currCar = currTarget.GetComponentInParent<CarUserMover> ();

				if (currCar && !currCar.IsPlayerControlled() && !currCar.IsCarRiding()) {
					carBtn.gameObject.SetActive (true);
					carCtrl.SetTarget (currCar.transform);
					return;
				}
			}
			carBtn.gameObject.SetActive (false);
		}
	}
}
