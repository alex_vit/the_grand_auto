using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityStandardAssets.CrossPlatformInput;

public class GameMenu : MonoBehaviour {
	public AudioSource ClickSound;
	private bool soundEnable = true;
	public static GameMenu Instance;

	public GameObject gamePanel;
	public GameObject pausePanel;
	public GameObject endPanel;
	public GameObject userPanel;
	public GameObject loadingPanel;
	public GameObject dialogPanel;

	public GameObject b_sound_off, b_sound_on;
	public Text finalLabel;
	public string winMessage = "Complete";
	public string lostMessage = "Complete";
//	public GameObject starsContainer;
//	public GameObject nextLevelButton;
	public float endDelay = 3f;
//	public Image[] stars;
	public Sprite empty;
	public Sprite fill;
	public Text scoresLabel;
	public GameObject DeathText;
	public GameObject RestartButtonDeath;
	public GameObject pauseButton,WatchVideo;

	float tScale = 1;
	public int countVideo=0;
	int scores;
	int progress;

	public TouchPanel touchPanel;
	//bool SoundEnabled=true;

	public bool debug;
	public enum State
	{
		Normal,
		Pause,
		Complete,
		Loading
	}
	public State state;
	// Use this for initialization
	void Awake()
	{
		//PlayerPrefs.DeleteAll ();
		Instance = this;

		if(PlayerPrefs.HasKey("sound")){
			if(PlayerPrefs.GetInt("sound")==0)
			{
				soundEnable = false;
				AudioListener.volume = 0;
				
				b_sound_off.SetActive(true);
				b_sound_on.SetActive(false);
				//PlayerPrefs.SetInt("sound",0);
			}
			else
			{
				soundEnable = true;
				AudioListener.volume = 1;
				
				b_sound_off.SetActive(false);
				b_sound_on.SetActive(true);
			}
		}
		else
		{
			if(soundEnable)
			{
				PlayerPrefs.SetInt("sound", 1);
				AudioListener.volume = 1;
				
				b_sound_off.SetActive(false);
				b_sound_on.SetActive(true);
			}
			else
			{
				PlayerPrefs.SetInt("sound", 0);
				AudioListener.volume = 0;
				
				b_sound_off.SetActive(true);
				b_sound_on.SetActive(false);
			}
		}
	}

	void Start () 
	{
		Time.timeScale = 1;
		soundEnable = true;
	}

//	public void ClickSoundPlay()
//	{
//		ClickSound.Play ();
//	}
	public void Sound()
	{
		soundEnable = !soundEnable;
		
		if (soundEnable) 
		{
			PlayerPrefs.SetInt("sound", 1);
			PlayerPrefs.Save();
			AudioListener.volume = 1;

			b_sound_off.SetActive(false);
			b_sound_on.SetActive(true);
		} 
		if (!soundEnable)
		{
			PlayerPrefs.SetInt("sound", 0);
			PlayerPrefs.Save();
			AudioListener.volume = 0;

			b_sound_off.SetActive(true);
			b_sound_on.SetActive(false);
		}
	}
	float myTimerAd=0f;
	bool getAd=false;

	void Update()
	{
		if (!getAd) {
			if (myTimerAd >= 20f) {
				myTimerAd = 0f;
				getAd = true;
			} else
				myTimerAd += Time.deltaTime;
		}
		/*if (soundEnable)
		{
			AudioListener.volume = 1f;
		}
		if (!soundEnable)
		{
			AudioListener.volume = 0f;
		}*/
		if (Input.GetKeyDown (KeyCode.Escape))
		{
			switch (state)
			{
			case State.Pause:
				OnBack();
				break;
			case State.Normal:
				OnPause();
				break;

			case State.Complete:
				OnMenu();
				break;
			}
		}
		if (debug)
		{
			if (Input.GetKeyDown (KeyCode.F1))
				OnEnd (true);	
			if (Input.GetKeyDown (KeyCode.F2))
				OnEnd (false);
		}
	}

	public void ClickPauseButton()
	{
		if (getAd) {
			//Debug.Log ("AA");
			getAd=false;
			UserAdvertise.ShowFullScreenAd ();
		}
	}

	public void OnPause()
	{
		//UserAdvertise.ShowFullScreenAd ();
		AudioListener.volume=0;
		tScale = Time.timeScale;
		Time.timeScale = 0;

		gamePanel.SetActive (false);
		pausePanel.SetActive (true);
		userPanel.SetActive (false);
		dialogPanel.SetActive (false);

		touchPanel.Hide ();

		state = State.Pause;
	//	MApplication.instance.pause = true;
	}
	public void OnBack()
	{
		if(soundEnable) AudioListener.volume=1;
		Time.timeScale = tScale;

		gamePanel.SetActive (true);
		pausePanel.SetActive (false);
		userPanel.SetActive (true);
		dialogPanel.SetActive (false);

		state = State.Normal;

		touchPanel.Show ();
	//	MApplication.instance.pause = false;
	}
	public void OnRestart()
	{
		//Debug.Log ("restart");
		Application.LoadLevel(Application.loadedLevel);
	}
	public void OnNextLevel()
	{
	

	}

	public void PlayerIsDead()
	{
		if (countVideo == 0) {
			countVideo++;
			WatchVideo.SetActive (true);
		}
		state = State.Pause;
		pausePanel.SetActive (false);
		DeathText.SetActive (true);
		RestartButtonDeath.SetActive (true);
		userPanel.SetActive (false);
		touchPanel.Hide ();
		pauseButton.SetActive(false);
	}

	public void PlayerIsAlive()
	{
		userPanel.SetActive(true);
		touchPanel.Show ();
		gamePanel.SetActive (true);
		DeathText.SetActive(false);
		RestartButtonDeath.SetActive(false);
		state=GameMenu.State.Normal;
		pauseButton.SetActive(true);
	}

	public void OnMenu()
	{
	
		state = State.Loading;
		//endPanel.SetActive (false);
		pausePanel.SetActive (false);
		loadingPanel.SetActive (true);
		Application.LoadLevel(1);

	}
	public void OnEnd(bool won)
	{
		if (state == State.Complete)
			return;
		StartCoroutine (WaitForEndGame (won));
		this.scores = -1;

	}
	public void OnEnd(int scores)
	{
		if (state == State.Complete)
			return;
		StartCoroutine (WaitForEndGame (true));
		this.scores = scores;
	}
	public void OnEnd(int scores, int progress)
	{
		if (state == State.Complete)
			return;
		StartCoroutine (WaitForEndGame (true));
		this.scores = scores;
		this.progress = progress;
	}
	IEnumerator WaitForEndGame(bool won)
	{
		yield return new WaitForSeconds (endDelay);
		state = State.Complete;
		gamePanel.SetActive (false);
		pausePanel.SetActive (false);
		userPanel.SetActive (false);
		endPanel.SetActive (true);
//		nextLevelButton.SetActive (!isLastLevel && LevelsInfo.HasLevels && won);
//		starsContainer.SetActive (LevelsInfo.UseStars && won);
		finalLabel.text = won ? winMessage : lostMessage;
		scoresLabel.text = scores.ToString ();
		if(scores < 0)
			scoresLabel.transform.parent.gameObject.SetActive(false);
		else
			scoresLabel.transform.parent.gameObject.SetActive(true);
		Time.timeScale = 0;
	// Int. Ad Here
		if (won)
		{
	
			// Won Logic Here //

		}
	}
}
