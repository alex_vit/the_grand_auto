﻿using UnityEngine;
using System.Collections;
using UnityStandardAssets.Cameras;

public class FirstPersonCamera : MonoBehaviour,ICameraManageable {
	Camera selfCamera;
	void Awake(){
		selfCamera = GetComponent<Camera> ();
	}

	#region implemented abstract members of PivotBasedCameraRig

	public void TurnOn ()
	{
		selfCamera.enabled = true;
	}

	public void TurnOff ()
	{
		selfCamera.enabled = false;
	}

	#endregion


}
