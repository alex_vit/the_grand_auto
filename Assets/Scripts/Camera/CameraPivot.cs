﻿using UnityEngine;
using System.Collections;

public class CameraPivot : MonoBehaviour {
	public Transform player;
	Transform selfTrans;

	void Awake(){
		selfTrans = transform;
	}
	// Use this for initialization
	void Start () {
		LookToTarget ();
	}
	
	// Update is called once per frame
	void LateUpdate () {
		selfTrans.rotation = Quaternion.Euler(selfTrans.rotation.x,0f, 0f);	
		selfTrans.localRotation = Quaternion.Euler(selfTrans.rotation.x,0f, 0f);		
	}

	public void LookToTarget(){
		selfTrans.LookAt (player);
	}
}
