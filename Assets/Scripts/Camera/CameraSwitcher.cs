﻿using UnityEngine;
using System.Collections;
using UnityStandardAssets.Cameras;
using UnityStandardAssets.Characters.FirstPerson;
using UnityStandardAssets.Characters.ThirdPerson;

public class CameraSwitcher : MonoBehaviour {
	public static CameraSwitcher Instance;

	public FreeLookCam PersonCamera;
	public AutoCam CarCamera;
	public Transform cameraPivot;
	public FirstPersonCamera FPCamera;
	public RigidbodyFirstPersonController RFPC;
	public ThirdPersonUserControl TPUC;
	public ThirdPersonCharacter TPC;
	public SkinnedMeshRenderer SMR;
	public GameObject Bones;
	public Animator PlayerAnim;
	public Camera GunCamera;
	public Weapon PlayerWearon;
	public GameObject ThirdPersonCam;
	public GameObject Cross;

	Transform currTarget;


	Vector3 carCamPivot = new Vector3 (0, 4, -6);
	Vector3 personCamPivot = new Vector3 (0, 2.5f, -3);

	ICameraManageable currCamera;

	CameraMode _mode;
	public CameraMode Mode{
		get{ 
			return _mode;
		}
		set{ 
			_mode = value;

			switch (_mode) {
				case CameraMode.FIRST_PERSON:
					SetFirstPersonCam ();
					break;
				case CameraMode.THIRD_PERSON:
					SetThirdPersonCam ();
					break;
				case CameraMode.CAR_MODE:
					SetCarCam ();
					break;
			}
		}
	}

	public bool IsPersonCamera
	{
		get{return _mode == CameraMode.FIRST_PERSON;}
	}

	public void SwitchStateCam()
	{
		if (_mode == CameraMode.FIRST_PERSON) Mode = CameraMode.THIRD_PERSON;
		else Mode = CameraMode.FIRST_PERSON;
	}
	void Awake(){
		if (Instance == null)
			Instance = this;
		else
			Destroy (gameObject);
	}

	// Use this for initialization
	void Start () {
		SetThirdPersonCam ();
	}
	  

	// Update is called once per frame
	void Update () {
	
	}

	public void SetTarget(Transform target){
		if (currTarget != target)
			currTarget = target;
	}


	void SetFirstPersonCam(){
		//currCamera.enabled = false;
		if (currCamera != null)
			currCamera.TurnOff ();
		//ThirdPersonCam.SetActive (false);
		//PersonCamera.enabled = false;
		//FPCamera.TurnOn ();


		Bones.SetActive (false);
		SMR.enabled = false;
		TPC.enabled=false;
		TPUC.enabled=false;
		//PlayerAnim.Stop ();
		RFPC.enabled = true;
		PlayerAnim.enabled = false;
		Cross.SetActive (true);

		currCamera = FPCamera;
		currCamera.TurnOn ();

		PlayerController.Instance.CurrCam = FPCamera.GetComponent<Camera>();
		PlayerController.Instance.ChangeTypeFire (2);
		
		PlayerController.Instance.ActiveWeapon ();
		ChooseWeapon.Instance.ViewButtonAttack ();
	}

	void SetThirdPersonCam(){
		GunCamera.enabled = false;
		Bones.SetActive (true);
		Cross.SetActive(false);
		SMR.enabled = true;
		TPC.enabled=true;
		TPUC.enabled=true;
		PlayerAnim.enabled = true;
		PlayerAnim.Rebind ();
		RFPC.enabled = false;
		PlayerController.Instance.InActiveWeapon ();
		PlayerController.Instance.ChangeTypeFire (1);
		ThirdPersonCam.SetActive (true);

		if (currCamera != null)
			currCamera.TurnOff ();

		PersonCamera.TurnOn();
		currCamera = PersonCamera;
		if(currTarget)
			PersonCamera.SetTarget (currTarget);
		PersonCamera.FindAndTargetPlayer ();

		cameraPivot.localPosition = personCamPivot;

		ChooseWeapon.Instance.HideButtonAttack ();
		//PlayerAnim.Play ();
		//PlayerWearon.CurrCam = ThirdPersonCam;

	}

	void SetCarCam(){
		GunCamera.enabled = false;
		if (currCamera != null)
			currCamera.TurnOff ();

		CarCamera.enabled = true;
		currCamera = CarCamera;
		CarCamera.SetTarget (currTarget);
		Cross.SetActive (false);
		cameraPivot.localPosition = carCamPivot;
	}
}

public enum CameraMode{
	THIRD_PERSON,
	FIRST_PERSON,
	CAR_MODE
}
