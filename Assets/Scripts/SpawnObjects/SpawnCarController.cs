﻿using UnityEngine;
using System.Linq;
using System.Collections;
using System.Collections.Generic;

public class SpawnCarController : MonoBehaviour {
	public Transform spawnPointsList;
	public int charsLimit;
	public float spawnInterval;
	public Transform player;

	public float maxDistance;
	public float maxSpawnDistance;
	public float minSpawnDistance;

	List<SpawnArea> spawnPoints;
	List<PullElement> spawnedPeoples;
	int spawnPointsCount;

	float currDist;
	bool isWorking;

	Vector2 playerPos;

	void Awake(){
		InitPoints ();
	}

	// Use this for initialization
	void Start () {
		Run ();
	}

	// Update is called once per frame
	void Update () {
		if(MotionController.Instance.GetMotionType() == MotionType.WALK)
			CheckDistance ();
	}

	void InitPoints(){
		if (spawnPoints == null) {
			spawnPoints = new List<SpawnArea> ();
			spawnedPeoples = new List<PullElement> ();
		} else {
			spawnPoints.Clear ();
		}
		spawnPointsCount = spawnPointsList.childCount;

		spawnPoints = spawnPointsList.GetComponentsInChildren<SpawnArea> ().ToList();

		for (int i = 0; i < spawnPoints.Count; i++) {
			//spawnPoints.Add(spawnPointsList.GetChild(i).GetComponent<SpawnArea>());
			Destroy (spawnPoints [i].GetComponent<Renderer> ());	
		}
	}

	public void Run(){
		StartCoroutine (Spawn ());
		isWorking = true;
	}

	public void Stop(){
		StopAllCoroutines ();
		isWorking = false;
	}

	void ReturnAllToPull(){
		foreach (PullElement em in spawnedPeoples) {
			em.Die ();
		}
	}

	void CheckDistance(){
		if (isWorking) {
			//Debug.Log ("spawnedPeoples count :" + spawnedPeoples.Count);
			for (int i=0; i< spawnedPeoples.Count; i++) {
				//PullElement em = spawnedPeoples [i];

				currDist = Vector3.SqrMagnitude (spawnedPeoples [i].transform.position - player.position);

				if (currDist > maxDistance) {
					PullElement em = spawnedPeoples [i];

					spawnedPeoples.RemoveAt (i);
					em.Die ();
				}
			}
		}
	}

	IEnumerator Spawn(){
		while (true) {
			if (spawnedPeoples.Count < charsLimit) {
				//Debug.Log (ObjectPull.Instance);
				if (!ObjectPull.Instance.IsEmpty ("Car")) {
					List<SpawnArea> currentVisbleAreas = FindEmptySpawnAreasNear ();

					if (currentVisbleAreas != null && currentVisbleAreas.Count != 0) {
						PullElement ch = ObjectPull.Instance.GetObjectFromPull ("Car");
						SpawnArea sp = currentVisbleAreas.First ();

						ch.transform.position = sp.transform.position;
						ch.SetActive ();
						ch.GetComponent<Rigidbody> ().useGravity = true;
						spawnedPeoples.Add (ch);
						ch.BindSpawnPoint (sp);

						//Debug.Log ("spawn car");
					}
				}
			}
			yield return new WaitForSeconds (spawnInterval);
		}			
	}

	List<SpawnArea> FindEmptySpawnAreasNear(){
		/*List<SpawnArea> tmp = spawnPoints.Where (x => !x.isUsed).ToList ();
		playerPos = new Vector2(player.position.x, player.position.z);
		int tt = tmp.Count;
		for(int i=0; i < tmp.Count; i++) {
			SpawnArea area = tmp [i];

			Vector2 arePos = new Vector2 (area.transform.position.x, area.transform.position.z);
			//Debug.Log (i + " " + Vector2.Distance (arePos, playerPos));
			currDist = Vector2.SqrMagnitude (arePos - playerPos);

			if (currDist <= maxSpawnDistance && currDist > minSpawnDistance) {
				tmp.RemoveAt (i);
				i--;
			}
		}
		return tmp;*/


		float currDist = 0;
		List<SpawnArea> res = new List<SpawnArea> ();
		playerPos = new Vector2(player.position.x, player.position.z);

		for(int i=0; i < spawnPoints.Count; i++) {
			SpawnArea area = spawnPoints [i];
			Vector2 arePos = new Vector2 (area.transform.position.x, area.transform.position.z);
			//Debug.Log (i + " " + Vector2.Distance (arePos, playerPos));

			currDist = Vector2.SqrMagnitude(arePos - playerPos);
			if (currDist <= maxSpawnDistance && currDist > minSpawnDistance && !area.isUsed) {
				res.Add (area);
				//i--;
			}
		}

		return res;
	}
}
