﻿using UnityEngine;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityStandardAssets.Vehicles.Car;

public class SpawnMovableCarsController : MonoBehaviour {
	public static SpawnMovableCarsController Instance;

	public Transform spawnPointsList;
	public int objectsLimit;
	public float spawnInterval;
	public Transform player;

	public float maxDistance;
	public float minSpawnDistance;
	public float maxSpawnDistance;

	List<Otmetka> spawnPoints;
	List<PullElement> spawnedObjects;
	int spawnPointsCount;

	float currDist;
	bool isWorking;

	Vector2 playerPos;

	void Awake(){
		if (Instance == null)
			Instance = this;
		else
			Destroy (gameObject);

		InitPoints ();
	}

	// Use this for initialization
	void Start () {
		Run ();
	}

	// Update is called once per frame
	void Update () {
		CheckDistance ();
	}

	void InitPoints(){
		if (spawnPoints == null) {
			spawnPoints = new List<Otmetka> ();
			spawnedObjects = new List<PullElement> ();
		} else {
			spawnPoints.Clear ();
		}
		spawnPointsCount = spawnPointsList.childCount;

		spawnPoints = spawnPointsList.GetComponentsInChildren<Otmetka> ().ToList();

		for (int i = 0; i < spawnPoints.Count; i++) {
			//spawnPoints.Add(spawnPointsList.GetChild(i).GetComponent<SpawnArea>());
			Destroy (spawnPoints [i].GetComponent<Renderer> ());	
		}
	}

	public void Run(){
		StartCoroutine (Spawn ());
		isWorking = true;
	}

	public void Stop(){
		StopAllCoroutines ();
		isWorking = false;
	}

	public void SetPlayer(Transform playerPos){
		player = playerPos;
	}

	void ReturnAllToPull(){
		foreach (PullElement em in spawnedObjects) {
			em.Die ();
		}
	}

	void CheckDistance(){
		if (isWorking) {
			//Debug.Log ("spawnedPeoples count :" + spawnedPeoples.Count);
			for (int i=0; i< spawnedObjects.Count; i++) {
				//PullElement em = spawnedPeoples [i];

				currDist = Vector3.SqrMagnitude (spawnedObjects [i].transform.position - player.position);

				if (currDist > maxDistance) {
					PullElement em = spawnedObjects [i];

					spawnedObjects.RemoveAt (i);
					em.Die ();

					//Debug.Log ("return");
				}
			}
		}
	}

	IEnumerator Spawn(){
		while (true) {
			if (spawnedObjects.Count < objectsLimit) {
				//Debug.Log (ObjectPull.Instance);
				if (!ObjectPull.Instance.IsEmpty ("Car")) {
					//Debug.Log ("pull non empty");
					List<Otmetka> currentVisbleAreas = FindEmptySpawnAreasNear ();
					//Debug.Log ("currentVisbleAreas.Count " + currentVisbleAreas.Count);

					if (currentVisbleAreas != null && currentVisbleAreas.Count != 0){
						PullElement ch = ObjectPull.Instance.GetObjectFromPull ("Car");
						Otmetka sp = currentVisbleAreas [Random.Range (0, currentVisbleAreas.Count)];

						SpawnObject (sp.transform, ch);

						//Debug.Log ("spawn car");
					}
				} //else
					//Debug.Log ("Pull is empty " + " Car");
			}
			yield return new WaitForSeconds (spawnInterval);
		}			
	}

	List<Otmetka> FindEmptySpawnAreasNear(){
		float currDist = 0;
		//Debug.Log ("spawn points count " + spawnPoints.Count);
		List<Otmetka> res = new List<Otmetka> ();
		playerPos = new Vector2(player.position.x, player.position.z);

		for(int i=0; i < spawnPoints.Count; i++) {
			Otmetka wayPoint = spawnPoints [i];
			Vector2 arePos = new Vector2 (wayPoint.transform.position.x, wayPoint.transform.position.z);
			//Debug.Log (i + " " + Vector2.Distance (arePos, playerPos));

			currDist = Vector2.SqrMagnitude(arePos - playerPos);
			if (currDist <= maxSpawnDistance && currDist > minSpawnDistance) {
				res.Add (wayPoint);
				//i--;
			}
		}

		return res;
		/*List<Otmetka> tmp = spawnPoints;
		Debug.Log ("spawn points count " + spawnPoints.Count);
		playerPos = new Vector2(player.position.x, player.position.z);
		int tt = tmp.Count;
		for(int i=0; i < tmp.Count; i++) {
			Otmetka wayPoint = tmp [i];

			Vector2 arePos = new Vector2 (wayPoint.transform.position.x, wayPoint.transform.position.z);
			//Debug.Log (i + " " + Vector2.Distance (arePos, playerPos));

			if (Vector2.SqrMagnitude(arePos - playerPos) > spawnDistance) {
				tmp.RemoveAt (i);
				i--;
			}
		}
		return tmp;*/
	}

	void SpawnObject(Transform otmetka, PullElement em){
		RaycastHit rh;

		if (Physics.Raycast (otmetka.position, Vector3.down, out rh, 5)) {

			//Transform car_t = (Transform)Instantiate (cars [Random.Range (0, cars.Length)], rh.point, otmetka.rotation);
			em.transform.position = otmetka.transform.position;
			em.transform.eulerAngles = new Vector3(otmetka.transform.rotation.x, otmetka.transform.rotation.y, otmetka.transform.rotation.z);
			em.SetActive ();
			em.GetComponent<NavMeshAgent> ().enabled = true;
			em.GetComponent<Rigidbody> ().useGravity = false;
			em.GetComponent<Rigidbody> ().isKinematic = true;
			em.transform.LookAt (otmetka.GetComponent<Otmetka> ().list_otm [0].transform.position);

			em.GetComponent<CarMover> ().enabled = true;
			em.GetComponent<CarMover> ().SetTarget (otmetka.GetComponent<Otmetka>());
			//em.GetComponent<CarAIControl> ().enabled = true;

			//em.GetComponent<CarAIControl> ().SetTarget (otmetka);
			//em.GetComponent<Rigidbody> ().velocity = em.transform.forward * 5;

			spawnedObjects.Add (em);
		}
	}
}
