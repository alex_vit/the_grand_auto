using System;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;

namespace UnityStandardAssets.Characters.ThirdPerson
{
    [RequireComponent(typeof (ThirdPersonCharacter))]
    public class ThirdPersonUserControl : MonoBehaviour
    {
        private ThirdPersonCharacter m_Character; // A reference to the ThirdPersonCharacter on the object
        public Transform m_Cam;                  // A reference to the main camera in the scenes transform
        private Vector3 m_CamForward;             // The current forward direction of the camera
        private Vector3 m_Move;
        private bool m_Jump;                      // the world-relative desired move direction, calculated from the camForward and user input.

		float h,v;
		string horizontalAxis;
		string verticalAxis;
		//public CustomMobileJoystick CMJ;
		public BaseMobileJoystick CMJ;


        private void Start()
        {
            // get the transform of the main camera
            if (Camera.main != null)
            {
                //m_Cam = Camera.main.transform;
            }
            else
            {
                Debug.LogWarning(
                    "Warning: no main camera found. Third person character needs a Camera tagged \"MainCamera\", for camera-relative controls.");
                // we use self-relative controls in this case, which probably isn't what the user wants, but hey, we warned them!
            }

            // get the third person character ( this should never be null due to require component )
            m_Character = GetComponent<ThirdPersonCharacter>();
			GetInput ();
        }

        // Fixed update is called in sync with physics
        private void Update()
        {
			if (!m_Jump)
			{
				m_Jump = CrossPlatformInputManager.GetButtonDown("Jump");
			}

			//Vector3 movePos = CMJ.GetPosition ();
			Vector3 movePos = CMJ.GetMotionPoint ();
			h = movePos.x;
			v = movePos.y;
            bool crouch = Input.GetKey(KeyCode.C);
			// calculate move direction to pass to character
			if (m_Cam != null) {
				// calculate camera relative direction to move:
				m_CamForward = Vector3.Scale (m_Cam.forward, new Vector3 (1, 0, 1)).normalized;
				//Debug.Log (m_Cam.forward);
				//Debug.Log (m_CamForward);
				//Debug.Log (m_Cam.right);
				m_Move = v * m_CamForward + h * m_Cam.right;
			} else {
				m_Move = v * Vector3.right + h * Vector3.back;
			}
#if !MOBILE_INPUT
		// walk speed multiplier
        if (Input.GetKey(KeyCode.LeftShift)) m_Move *= 0.5f;
#endif

			// pass all parameters to the character control script

			m_Character.Move (m_Move, crouch, m_Jump);
			m_Jump = false;
		}

		public void TakeGun(){
			//m_Character.gameObject.GetComponent<Armed>().
		}

		public void GetInput(){
			horizontalAxis = "Horizontal_j";
			verticalAxis = "Vertical_j";

			/*#if UNITY_EDITOR
			horizontalAxis = "Horizontal_editor";
			verticalAxis = "Vertical_editor";
			#else
			horizontalAxis = "Horizontal";
			verticalAxis = "Vertical";
			#endif*/
		}
    }
}
