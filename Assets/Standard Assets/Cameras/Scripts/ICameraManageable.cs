﻿namespace UnityStandardAssets.Cameras
{
	public interface ICameraManageable{
		void TurnOn ();
		void TurnOff();
	}
}
