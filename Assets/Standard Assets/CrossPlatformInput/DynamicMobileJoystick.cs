﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
namespace UnityStandardAssets.CrossPlatformInput{
	
	public class DynamicMobileJoystick : BaseMobileJoystick {
		public float fingerTimeBeforeJoystickAppears = 0.1f;
		
		// If after scaling the image you want to ensure the thumbstick
		// rotates in a tight circle you can click this option
		
		public RectTransform touchArea;
		// joystick image must contain GUITexture
		public GameObject imageJoystick;
		// bg for joystick must contain GUITexture
		public GameObject imageBG;	
		// public property that any script can get the position of the joystick and use it to move an object
		public Vector2  position;
		
		// How much the position is multiplied by
		// If using Unity Controller scripts it can be
		// used to change the speed of the player
		// makes the joystick act as a button
		public bool  isButton = false;
		public int  tapCount;
		
		// private vars no need to worry
		private Rect  isInMotionStartingArea;
		private float  rotFTime;
		private float  motionFTime;	
		private bool  isMotionPress  = false;
		private bool  movementStickShowing  = false;
		private float  maxAmount   = 0.1f;
		private int motionFingerNum  = -1;
		private int lastFingerId = -1;	
		
		
		private float imageJoystickScale;
		
		void Start ()
		{
			// hide sticks intially
			ShowHideMotion(false);							
			
		}
		
		void Update ()
		{
			//if run on mobile device
			if (!isEditor) {
				if (Input.touchCount == 0) {
					ShowHideMotion(false);
				}
				
				foreach(Touch touch in Input.touches)
				{
					if (touch.phase == TouchPhase.Canceled || touch.phase == TouchPhase.Ended) {
						
						if (motionFingerNum == touch.fingerId) {
							motionFingerNum = -1;
							lastFingerId = -1;
							position = Vector2.zero;
							ShowHideMotion(false);
						}
					}
					if (Time.time > motionFTime + fingerTimeBeforeJoystickAppears && motionFingerNum == touch.fingerId ) {
						if (movementStickShowing)
							UpdateMotionPoint(touch.position);
						else
							ShowHideMotion(true);
					}
					
					if (touch.phase == TouchPhase.Began) {
						
						if ( motionFingerNum == -1 && CheckTouchArea(touch.position)) {
							SetMotionPoint (touch.position);
							motionFTime = Time.time;
							motionFingerNum = touch.fingerId;
							lastFingerId = 1;
						}
					}
				}
			} else 
				//if run in Unity editor
			{
				if (Input.GetMouseButtonDown(0)) {
					if (CheckTouchArea(Input.mousePosition)) {
						lastFingerId = 1;
						
						isMotionPress = true;
						SetMotionPoint(Input.mousePosition);
						motionFTime = Time.time;
					} else { 
						isMotionPress = false; 
					}
					
				}
				if (Input.GetMouseButton(0) ) {
					if (isMotionPress && Time.time > motionFTime + fingerTimeBeforeJoystickAppears) {
						if (movementStickShowing) {
							UpdateMotionPoint (Input.mousePosition);
						}	
						else
							ShowHideMotion(true);
					}
					
				}
				if (Input.GetMouseButtonUp(0)) {
					lastFingerId = -1;
					position = Vector2.zero;				
					ShowHideMotion(false);
				}
			}
		}
		
		void ShowHideMotion(bool show) {
			if (show) {
				imageJoystick.GetComponent<Image> ().enabled = true;
				imageBG.GetComponent<Image> ().enabled = true;
				movementStickShowing = true;
			} else {
				imageJoystick.GetComponent<Image> ().enabled = false;
				imageBG.GetComponent<Image> ().enabled = false;
				movementStickShowing = false;
				SetDefaultMotionPoint();
			}
		}
		
		
		void OnEnable(){
			
		}
		
		void OnDisable(){
			ShowHideMotion (false);
		}
		
		void Disable()
		{
			gameObject.active = false;	
		}
		
		bool IsFingerDown()	
		{
			return (lastFingerId != -1);
		}
		
		
		void CheckPlatform(){
			#if UNITY_EDITOR
			isEditor = true;
			#else
			isEditor = false;
			#endif
		}
		
		bool CheckTouchArea(Vector2 pos){
			return RectTransformUtility.RectangleContainsScreenPoint (touchArea, pos); 
		}
		
		
		protected override void UpdateMotionPoint(Vector3 newPos) {
			if(!isButton)
			{
				innerStickRect.position = new Vector3 (newPos.x, newPos.y, 0);
				
				if(!forceCircleConstraint)
					innerStickRect.position = ClampStickInBounds(innerStickRect.position, outerStickRect.position);
				else
				{
					innerStickRect.position = ClampStickInBounds(innerStickRect.position, outerStickRect.position);
					//position.x = position.x /imageJoystickScale;
				}
				Vector3 pos = (innerStickRect.position - outerStickRect.position);
				//Debug.Log ("pos " + pos / 90f);
				position = pos / clampRadus * positionMultiplier;
			}
		}
		
		protected override void SetMotionPoint(Vector3 newPos) {
			outerStickRect.position = new Vector3 (newPos.x, newPos.y, 0);
			innerStickRect.position = new Vector3 (newPos.x, newPos.y, 0);
			position = Vector2.zero;
		}
		
		
		
		protected override void SetDefaultMotionPoint(){
			outerStickRect.localPosition = Vector3.zero;
			innerStickRect.localPosition = Vector3.zero;
			
			position = Vector3.zero;
		}
		
		protected override void SetJoystickParams(){
			innerStickRect = imageJoystick.GetComponent<RectTransform> ();
			outerStickRect = imageBG.GetComponent<RectTransform> ();
			
			imageJoystickScale =  imageJoystick.transform.localScale.x /  imageJoystick.transform.localScale.y;	
			clampRadus = (outerStickRect.rect.width/2 - innerStickRect.rect.width);
		}
		
		protected Vector3 ClampStickInBounds(Vector3 newPos, Vector3 circlePos){
			if (Vector3.Distance (newPos, circlePos) > clampRadus) {
				Vector3 dir = newPos - circlePos;
				dir.Normalize ();
				return dir * clampRadus + circlePos;
			}
			return newPos;
		}
		
		public override Vector3 GetMotionPoint(){
			return position;
		}
	}
}