﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

namespace UnityStandardAssets.CrossPlatformInput{
	public class StaticMobileJoystick : BaseMobileJoystick{
		public float fingerTimeBeforeJoystickAppears = 0.1f;
		
		// joystick image must contain GUITexture
		public GameObject imageJoystick;
		// bg for joystick must contain GUITexture
		public GameObject imageBG;	
		// public property that any script can get the position of the joystick and use it to move an object
		public Vector2  position;
		
		// makes the joystick act as a button
		public bool  isButton = false;
		public int  tapCount;
		
		private bool  isMotionPress  = false;
		
		private int motionFingerNum  = -1;
		private int lastFingerId = -1;	
		
		
		private float imageJoystickScale;
		
		void Start ()
		{
			
		}
		
		void Update ()
		{
			//if run on mobile device
			if (!isEditor) {
				foreach(Touch touch in Input.touches)
				{
					if (touch.phase == TouchPhase.Canceled || touch.phase == TouchPhase.Ended) {
						
						if (motionFingerNum == touch.fingerId) {
							motionFingerNum = -1;
							lastFingerId = -1;
							SetDefaultMotionPoint ();
						}
					}
					if (touch.phase == TouchPhase.Began && CheckTouchArea(touch.position)) {
						
						if ( motionFingerNum == -1) {
							SetDefaultMotionPoint ();
							motionFingerNum = touch.fingerId;
							lastFingerId = 1;
						}
					}
					if (motionFingerNum == touch.fingerId ) {
						UpdateMotionPoint(touch.position);
					}
				}
			} else 
				//if run in Unity editor
			{
				if (Input.GetMouseButtonDown(0) && CheckTouchArea(Input.mousePosition)) {
					lastFingerId = 1;
					isMotionPress = true;
					SetDefaultMotionPoint ();
				}
				if (Input.GetMouseButton(0) && isMotionPress) {
					UpdateMotionPoint (Input.mousePosition);
				}
				if (Input.GetMouseButtonUp(0)) {
					lastFingerId = -1;
					SetDefaultMotionPoint ();
					isMotionPress = false;
				}
			}
		}
		
		void OnEnable(){
			
		}
		
		void OnDisable(){
		}

		void CheckPlatform(){
			#if UNITY_EDITOR
			isEditor = true;
			#else
			isEditor = false;
			#endif
		}
		
		bool CheckTouchArea(Vector2 pos){
			return RectTransformUtility.RectangleContainsScreenPoint (outerStickRect, pos); 
		}
		
		#region override methods
		protected override void UpdateMotionPoint(Vector3 newPos) {
			if(!isButton)
			{
				innerStickRect.position = new Vector3 (newPos.x, newPos.y, 0);
				
				if(!forceCircleConstraint)
					innerStickRect.position = ClampStickInBounds(innerStickRect.position, outerStickRect.position);
				else
				{
					innerStickRect.position = ClampStickInBounds(innerStickRect.position, outerStickRect.position);
				}
				Vector3 pos = (innerStickRect.position - outerStickRect.position);
				position = pos / clampRadus * positionMultiplier;
			}
		}
		
		protected override void SetMotionPoint(Vector3 newPos) {
			outerStickRect.position = new Vector3 (newPos.x, newPos.y, 0);
			innerStickRect.position = new Vector3 (newPos.x, newPos.y, 0);
			position = Vector2.zero;
		}
		
		protected override void SetDefaultMotionPoint(){
			outerStickRect.localPosition = Vector3.zero;
			innerStickRect.localPosition = Vector3.zero;
			
			position = Vector3.zero;
		}
		
		protected override void SetJoystickParams(){
			innerStickRect = imageJoystick.GetComponent<RectTransform> ();
			outerStickRect = imageBG.GetComponent<RectTransform> ();
			
			imageJoystickScale =  imageJoystick.transform.localScale.x /  imageJoystick.transform.localScale.y;	
			clampRadus = (outerStickRect.rect.width/2 - innerStickRect.rect.width);
		}
		
		protected Vector3 ClampStickInBounds(Vector3 newPos, Vector3 circlePos){
			if (Vector3.Distance (newPos, circlePos) > clampRadus) {
				Vector3 dir = newPos - circlePos;
				dir.Normalize ();
				return dir * clampRadus + circlePos;
			}
			return newPos;
		}
		
		public override Vector3 GetMotionPoint(){
			return position;
		}
		
		#endregion
	}
}