﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

namespace UnityStandardAssets.CrossPlatformInput{
	public class BaseMobileJoystick : MonoBehaviour {
		public float positionMultiplier = 5;
		public bool forceCircleConstraint;
		
		protected bool isEditor;
		protected float clampRadus;
		
		protected RectTransform innerStickRect;
		protected RectTransform outerStickRect;
		// Use this for initialization
		
		void Awake(){
			CheckPlatform ();
			SetJoystickParams ();
		}
		
		void Start () {
			
		}
		
		// Update is called once per frame
		void Update () {
			
		}
		
		void CheckPlatform(){
			#if UNITY_EDITOR
			isEditor = true;
			#else
			isEditor = false;
			#endif
		}
		
		public void Show(){
			gameObject.SetActive (true);
		}
		
		public void Hide(){
			gameObject.SetActive (false);
		}
		
		protected virtual void SetJoystickParams(){
			
		}
		
		protected virtual void SetMotionPoint(Vector3 pos){
			
		}
		
		protected virtual void UpdateMotionPoint(Vector3 pos){
			
		}
		
		protected virtual void SetDefaultMotionPoint(){
			
		}
		
		protected virtual Vector3 ClampStickInBounds(){
			return Vector3.zero;
		}
		
		public virtual Vector3 GetMotionPoint(){
			return Vector3.zero;
		}
		
		
		
	}
	
}