using System;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace UnityStandardAssets.CrossPlatformInput
{
	[RequireComponent(typeof(Image))]
	public class MoveTouchPad : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
	{
		// Options for which axes to use
		public enum AxisOption
		{
			Both, // Use both
			OnlyHorizontal, // Only horizontal
			OnlyVertical // Only vertical
		}
		public enum ControlStyle
		{
			Absolute, // operates from teh center of the image
			Relative, // operates from the center of the initial touch
			Swipe, // swipe to touch touch no maintained center
		}


		public AxisOption axesToUse = AxisOption.Both; // The options for the axes that the still will use
		public ControlStyle controlStyle = ControlStyle.Absolute; // control style to use
		public string horizontalAxisName = "Horizontal"; // The name given to the horizontal axis for the cross platform input
		public string verticalAxisName = "Vertical"; // The name given to the vertical axis for the cross platform input
		public float Xsensitivity = 1f;
		public float Ysensitivity = 1f;

		//public Text outText;

		Vector3 m_StartPos;
		Vector2 m_PreviousDelta;
		Vector3 m_JoytickOutput;
		bool m_UseX; // Toggle for using the x axis
		bool m_UseY; // Toggle for using the Y axis
		CrossPlatformInputManager.VirtualAxis m_HorizontalVirtualAxis; // Reference to the joystick in the cross platform input
		CrossPlatformInputManager.VirtualAxis m_VerticalVirtualAxis; // Reference to the joystick in the cross platform input
		bool m_Dragging;
		int m_Id=0;
		Vector2 m_PreviousTouchPos; // swipe style control touch


		bool isEditor;
		private Vector3 m_Center;
		private Image m_Image;
		Vector3 m_PreviousMouse;

		Vector2 prevTouchPos;
		Vector2 currTouchPos;

		float epsilon = 5f;

		void OnEnable()
		{
			CreateVirtualAxes();
		}

		void Awake()
		{
			m_Image = GetComponent<Image>();
			m_Center = m_Image.transform.position;
		}

        void Start()
        {
			/*DetectPlatform ();

			if(!isEditor){
            	m_Image = GetComponent<Image>();
            	m_Center = m_Image.transform.position;
			}*/
        }

		void CreateVirtualAxes()
		{
			// set axes to use
			m_UseX = (axesToUse == AxisOption.Both || axesToUse == AxisOption.OnlyHorizontal);
			m_UseY = (axesToUse == AxisOption.Both || axesToUse == AxisOption.OnlyVertical);

			// create new axes based on axes to use
			if (m_UseX)
			{
				m_HorizontalVirtualAxis = CrossPlatformInputManager.VirtualAxisReference(horizontalAxisName);
				if (m_HorizontalVirtualAxis == null) {
					m_HorizontalVirtualAxis = new CrossPlatformInputManager.VirtualAxis(horizontalAxisName);
					CrossPlatformInputManager.RegisterVirtualAxis(m_HorizontalVirtualAxis);
				}
			}
			if (m_UseY)
			{
				m_VerticalVirtualAxis = CrossPlatformInputManager.VirtualAxisReference(verticalAxisName);
				if (m_VerticalVirtualAxis == null) 
				{
					m_VerticalVirtualAxis = new CrossPlatformInputManager.VirtualAxis(verticalAxisName);
					CrossPlatformInputManager.RegisterVirtualAxis(m_VerticalVirtualAxis);
				}
			}
		}

		void UpdateVirtualAxes(Vector3 value)
		{

			if (m_UseX)
			{
				m_HorizontalVirtualAxis.Update(value.x);
			}

			if (m_UseY)
			{
				m_VerticalVirtualAxis.Update(value.y);
			}
		}


		public void OnPointerDown(PointerEventData data)
		{
			m_Dragging = true;
			m_Id = data.pointerId;
			prevTouchPos = data.position;
		}

		void Update()
		{
			#if UNITY_EDITOR
			if (m_Dragging) if (Time.timeScale > 0.1f) HandleInputEditor ();
			#else
			if (m_Dragging) if (Time.timeScale > 0.1f) if(Input.touchCount>0) ParseTouches ();
			#endif
		}


		public void OnPointerUp(PointerEventData data)
		{
			m_Dragging = false;
			m_Id = 0;
			UpdateVirtualAxes(Vector3.zero);
		}

		/*void DetectPlatform(){
			#if UNITY_EDITOR
			isEditor = true;
			#else
			isEditor = false;
			#endif
		}*/

		void OnDisable()
		{
			if (CrossPlatformInputManager.AxisExists(horizontalAxisName))
				CrossPlatformInputManager.UnRegisterVirtualAxis(horizontalAxisName);

			if (CrossPlatformInputManager.AxisExists(verticalAxisName))
				CrossPlatformInputManager.UnRegisterVirtualAxis(verticalAxisName);
		}


		public void Show(){
			m_Image.enabled = true;
			this.enabled = true;
		}

		public void Hide(){
			m_Image.enabled = false;
			this.enabled = false;
		}

		void HandleInputEditor(){
			Vector2 pointerDelta;
			pointerDelta.x = (Input.mousePosition.x - m_PreviousMouse.x) * Xsensitivity;
			pointerDelta.y = (Input.mousePosition.y - m_PreviousMouse.y) * Ysensitivity;
			m_PreviousMouse = new Vector3 (Input.mousePosition.x, Input.mousePosition.y, 0f);
			UpdateVirtualAxes(new Vector3(pointerDelta.x, pointerDelta.y, 0));
		}

		void ParseTouches(){

			Touch currTouch = Input.touches [m_Id];
			Vector2 distance = currTouch.position - prevTouchPos;
			if (currTouch.phase == TouchPhase.Moved) {
				Vector2 pointerDelta = new Vector2 (distance.x, distance.y);
				float x = pointerDelta.x/350f * Xsensitivity;
				float y = pointerDelta.y/350f * Ysensitivity;
				UpdateVirtualAxes(new Vector3(x, y, 0));
			}
			if (currTouch.phase == TouchPhase.Ended || currTouch.phase == TouchPhase.Stationary) {
				UpdateVirtualAxes(Vector3.zero);
				prevTouchPos = currTouch.position;
			}

		}
	}


}