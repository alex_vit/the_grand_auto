﻿using UnityEngine;
using System.Collections;
//using AppodealAds.Unity.Api;
//using AppodealAds.Unity.Common;
using AdToApp;

/// <summary>
/// Ad cernel.
/// This is a class for advertizing integrating in application
/// </summary>
namespace Advertising{
	public class AdCernel : MonoBehaviour  {
		static public AdCernel Instance;

		public string AppKey;
		public AdNameType Type;
	
		string AdTypeString = AdToAppContentType.BANNER + AdToAppContentType.VIDEO + AdToAppContentType.IMAGE + AdToAppContentType.REWARDED + AdToAppContentType.INTERSTITIAL;

		// Use this for initialization
		void Awake(){
			if (!AppKey.Equals (""))
				AdToAppBinding.start (adContentType: AdTypeString, appId: AppKey);
			if (Instance == null)
				Instance = this;

			//ShowBanner (AdType.BANNER_BOTTOM);
		}

		void Start () {
			
		}
		
		// Update is called once per frame
		void Update () {
		
		}

		public void ShowBanner(AdType type){
			AdToAppBinding.removeAllBanners ();
			switch (type) {
			case AdType.BANNER_BOTTOM:
				//Appodeal.show (Appodeal.BANNER_BOTTOM);
				//AdToAppBinding.showBanner(AdToAppBannerSize.Size_1024x90, 0f, 0f, 1024f, 90f);
				AdToAppBinding.showBannerAtPosition (AdToAppBannerPosition.BottomCenter, AdToAppBannerSize.Size_320x50, 0/* margin top */, 0/* margin left */, 0/* margin bottom */, 0/* margin right */);
				//AdToAppBinding.showBanner(AdToAppBannerSize.Size_320x50, (Screen.width * 0.5f) - 320f,Screen.height - 100f,320f,50f);
				//AdToAppBinding.showBannerAtPosition (AdToAppBannerPosition.BottomCenter, AdToAppBannerSize.Size_1024x90, 0/* margin top */, 0/* margin left */, 0/* margin bottom */, 0/* margin right */);
				AdToAppBinding.setBannerRefreshInterval (60f);
				break;
			case AdType.BANNER_CENTER:
				//Appodeal.show (Appodeal.BANNER_CENTER);
				break;
			case AdType.BANNER_TOP:
				//Appodeal.show (Appodeal.BANNER_TOP);
				break;
			default:
				//Appodeal.show (Appodeal.BANNER);
				//AdToAppBinding.showBanner(AdToAppBannerSize.Size_1024x90, 0f, 0f, 1024f, 90f);
				//AdToAppBinding.showBannerAtPosition(AdToAppBannerPosition.BottomCenter, AdToAppBannerSize.Size_1024x90,0/* margin top */,0/* margin left */,0/* margin bottom */,0/* margin right */);
				AdToAppBinding.showBannerAtPosition (AdToAppBannerPosition.BottomCenter, AdToAppBannerSize.Size_320x50, 0/* margin top */, 0/* margin left */, 0/* margin bottom */, 0/* margin right */);
				//AdToAppBinding.showBanner(AdToAppBannerSize.Size_320x50, (Screen.width * 0.5f) - 320f,Screen.height - 100f,320f,50f);
				AdToAppBinding.setBannerRefreshInterval (60f);
				break;
			}
		}

		public void ShowInterstitialNoImage(){
			AdToAppBinding.showInterstitial(AdToAppContentType.INTERSTITIAL);
		}

		public void ShowInterstitial(){
			//Appodeal.show (Appodeal.INTERSTITIAL);
			//if(AdToAppBinding.hasInterstitial(AdToAppContentType.IMAGE) && !AdToAppBinding.isInterstitialDisplayed())
				AdToAppBinding.showInterstitial(AdToAppContentType.IMAGE);
		}

		public void ShowRewardedVideo(){
			//Appodeal.show (Appodeal.REWARDED_VIDEO);
			/*if (AdToAppBinding.hasInterstitial(AdToAppContentType.REWARDED) &&
				!AdToAppBinding.isInterstitialDisplayed())*/
			AdToAppBinding.showInterstitial(AdToAppContentType.REWARDED);
		}
		public void ShowVideo(){
			//Appodeal.setAutoCache(Appodeal.VIDEO, false);
			//Appodeal.initialize(AppKey, Appodeal.ALL);
			//Appodeal.cache(Appodeal.VIDEO);
			//Appodeal.show (Appodeal.VIDEO);
			/*if (AdToAppBinding.hasInterstitial(AdToAppContentType.VIDEO) &&
				!AdToAppBinding.isInterstitialDisplayed())*/
			AdToAppBinding.showInterstitial(AdToAppContentType.VIDEO);
		}

		public void HideAd(AdType type){
			switch (type) {
			case AdType.BANNER:
				AdToAppBinding.removeAllBanners ();
				//Appodeal.hide (Appodeal.BANNER);
				break;
			case AdType.BANNER_BOTTOM:
				AdToAppBinding.removeAllBanners ();
				//Appodeal.hide (Appodeal.BANNER_BOTTOM);
				break;
			case AdType.BANNER_CENTER:
				AdToAppBinding.removeAllBanners ();
				//Appodeal.hide (Appodeal.BANNER_CENTER);
				break;
			case AdType.BANNER_TOP:
				AdToAppBinding.removeAllBanners ();
				//Appodeal.hide (Appodeal.BANNER_TOP);
				break;
			case AdType.INTERSTITIAL:
				//Appodeal.hide (Appodeal.INTERSTITIAL);
				break;
			case AdType.REWARDED_VIDEO:
				//Appodeal.hide (Appodeal.REWARDED_VIDEO);
				break;
			case AdType.VIDEO:
				//Appodeal.hide (Appodeal.VIDEO);
				break;
			}
		}
	}
}

public enum AdNameType{
	APPODEAL,
	ADDTOAPP
}