﻿using UnityEngine;
using System.Collections;
//using AppodealAds.Unity.Api;
//using AppodealAds.Unity.Common;
using AdToApp;

public delegate void EmptyEvent();
/// <summary>
/// Class for callbacks 
/// </summary>
public class AdUserCallback : MonoBehaviour{//,//IInterstitialAdListener,IRewardedVideoAdListener,IBannerAdListener,IVideoAdListener {
	/// <summary>
	/// throw when interstitial loaded
	/// </summary>
	public static event EmptyEvent OnInterstitialLoaded;
	/// <summary>
	/// throw when interstitial failed within loading
	/// </summary>
	public static event EmptyEvent OnInterstitialFailedToLoad;
	/// <summary>
	/// throw when intersitial was show
	/// </summary>
	public static event EmptyEvent OnInterstitialShown;
	/// <summary>
	/// throw when interstitial closed
	/// </summary>
	public static event EmptyEvent OnInterstitialClosed;
	/// <summary>
	/// throw when interstitial clicked
	/// </summary>
	public static event EmptyEvent OnInterstitialClicked;

	/// <summary>
	/// throw when rewarded video loaded
	/// </summary>
	public static event EmptyEvent OnRewardedVideoLoaded;
	/// <summary>
	/// throw when rewarded video failed
	/// </summary>
	public static event EmptyEvent OnRewardedVideoFailedToLoad;
	/// <summary>
	/// throw when rewarded video shown 
	/// </summary>
	public static event EmptyEvent OnRewardedVideoShown;
	/// <summary>
	/// throw when rewarded video finished
	/// </summary> 
	public static event EmptyEvent OnRewardedVideoFinished;
	/// <summary>
	/// throw when rewarded video closed
	/// </summary>
	public static event EmptyEvent OnRewardedVideoClosed;

	/// <summary>
	/// throw when banner loaded
	/// </summary>
	public static event EmptyEvent OnBannerLoaded;
	/// <summary>
	/// throw when banner failed
	/// </summary>
	public static event EmptyEvent OnBannerFailedToLoad;
	/// <summary>
	/// throw when banner shown 
	/// </summary>
	public static event EmptyEvent OnBannerShown;
	/// <summary>
	/// throw when banner clicked
	/// </summary>
	public static event EmptyEvent OnBannerClicked;

	/// <summary>
	/// throw when video loaded
	/// </summary>
	public static event EmptyEvent OnVideoLoaded;
	/// <summary>
	/// throw when video failed
	/// </summary>
	public static event EmptyEvent OnVideoFailedToLoad;
	/// <summary>
	/// throw when video shown
	/// </summary>
	public static event EmptyEvent OnVideoShown;
	/// <summary>
	/// throw when video finished
	/// </summary>
	public static event EmptyEvent OnVideoFinished;
	/// <summary>
	/// throw when video closed
	/// </summary>
	public static event EmptyEvent OnVideoClosed;
	
	// Use this for initialization
	void Start () {
		//Appodeal.setInterstitialCallbacks (this);
		//Appodeal.setRewardedVideoCallbacks (this);
		//Appodeal.setBannerCallbacks (this);
		//Appodeal.setVideoCallbacks (this);

	}

	// Update is called once per frame
	void Update () {

	}

	#region IInterstitialAdListener implementation

	public void onInterstitialLoaded ()
	{
		if (OnInterstitialLoaded != null)
			OnInterstitialLoaded ();
	}

	public void onInterstitialFailedToLoad ()
	{
		if (OnInterstitialFailedToLoad != null)
			OnInterstitialFailedToLoad ();
	}

	public void onInterstitialShown ()
	{
		if (OnInterstitialShown != null)
			OnInterstitialShown ();
	}

	public void onInterstitialClosed ()
	{
		if (OnInterstitialClosed != null)
			OnInterstitialClosed ();
	}

	public void onInterstitialClicked ()
	{
		if (OnInterstitialClicked != null)
			OnInterstitialClicked ();
	}

	#endregion

	#region IRewardedVideoAdListener implementation

	public void onRewardedVideoLoaded ()
	{
		if (OnRewardedVideoLoaded != null)
			OnRewardedVideoLoaded ();
	}

	public void onRewardedVideoFailedToLoad ()
	{
		if (OnRewardedVideoFailedToLoad != null)
			OnRewardedVideoFailedToLoad ();
	}

	public void onRewardedVideoShown ()
	{
		if (OnRewardedVideoShown != null)
			OnRewardedVideoShown ();
	}

	public void onRewardedVideoFinished (int amount, string name)
	{
		if (OnRewardedVideoFinished != null)
			OnRewardedVideoFinished ();
	}

	public void onRewardedVideoClosed ()
	{
		if (OnRewardedVideoClosed != null)
			OnRewardedVideoClosed ();
	}

	#endregion

	#region IBannerAdListener implementation

	public void onBannerLoaded ()
	{
		if (OnBannerLoaded != null)
			OnBannerLoaded ();
	}

	public void onBannerFailedToLoad ()
	{
		if (OnBannerFailedToLoad != null)
			OnBannerFailedToLoad ();
	}

	public void onBannerShown ()
	{
		if (OnBannerShown != null)
			OnBannerShown ();
	}

	public void onBannerClicked ()
	{
		if (OnBannerClicked != null)
			OnBannerClicked ();
	}

	#endregion

	#region IVideoAdListener implementation

	public void onVideoLoaded ()
	{
		if (OnVideoLoaded != null)
			OnVideoLoaded ();
	}

	public void onVideoFailedToLoad ()
	{
		if (OnVideoFailedToLoad != null)
			OnVideoFailedToLoad ();
	}

	public void onVideoShown ()
	{
		if (OnVideoShown != null)
			OnVideoShown ();
	}

	public void onVideoFinished ()
	{
		if (OnVideoFinished != null)
			OnVideoFinished ();
	}

	public void onVideoClosed ()
	{
		if (OnVideoClosed != null)
			OnVideoClosed ();
	}

	#endregion
}
