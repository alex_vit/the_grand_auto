﻿using UnityEngine;
using System.Collections;
using Advertising;

public static class UserAdvertise{
	public static bool inited;
	static AdType currentAdType;

	static public void Init(){	
		if (!inited) {
			inited = true;
			ShowBanner (AdType.BANNER_BOTTOM);
		}
	}

	static public void ShowBanner(AdType type){
		if (AdCernel.Instance) {
			AdCernel.Instance.ShowBanner (type);
			currentAdType = type;
		}
	}
	static public void ShowVideo(){
		if (AdCernel.Instance) {
			AdCernel.Instance.ShowVideo ();
			currentAdType = AdType.VIDEO;
		}
	}
	static public void ShowRewardedVideo(){	
		if (AdCernel.Instance) {
			AdCernel.Instance.ShowRewardedVideo ();
			currentAdType = AdType.REWARDED_VIDEO;
		}
	}
	static public void ShowFullScreenAd(){
		if (AdCernel.Instance) {
			AdCernel.Instance.ShowInterstitial ();
			currentAdType = AdType.INTERSTITIAL;
		}
	}

	static public void ShowInterstitialNoImage(){
		if (AdCernel.Instance) {
			AdCernel.Instance.ShowInterstitial ();
			currentAdType = AdType.INTERSTITIAL;
		}
	}

	static public void HideCurrentAd(){
		if (AdCernel.Instance)
			AdCernel.Instance.HideAd (currentAdType);
	}

	static public void HideAd(AdType type){
		if (AdCernel.Instance)
			AdCernel.Instance.HideAd (type);
	}
}

public enum AdType{
	BANNER,
	BANNER_BOTTOM,
	BANNER_TOP,
	BANNER_CENTER,
	VIDEO,
	REWARDED_VIDEO,
	INTERSTITIAL
}