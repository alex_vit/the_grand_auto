﻿using UnityEngine;
using UnityEngine.Audio;
using System.Collections;

public class MusicGenerator : MonoBehaviour {


	//public AudioClip no_action;
	//public AudioClip action;

	public AudioMixerGroup action;

	public AudioMixerGroup no_action;


	float cur_temp = 0;
	float target_temp = 0;

	float speed_change = 0.1f;

	// Use this for initialization
	void Start () {



		action.audioMixer.SetFloat ("ActionVolume", -80);

		action.audioMixer.SetFloat ("NoActionVolume", -80);

	}

	
	// Update is called once per frame
	void Update () {

		float va;
		float va1;

		//cur_temp += (target_temp - cur_temp) * Time.deltaTime;

		if (cur_temp < 0.2f) {



			action.audioMixer.GetFloat ("ActionVolume", out va);
			action.audioMixer.GetFloat ("NoActionVolume", out va1);


			
			

			if (va1 < 0)
				action.audioMixer.SetFloat ("NoActionVolume", va1 + 80 * Time.deltaTime * speed_change);
			else
				if (va > -80)
					action.audioMixer.SetFloat ("ActionVolume", va - 80 * Time.deltaTime * speed_change);

			

		} else {

			
			action.audioMixer.GetFloat ("NoActionVolume", out va);
			action.audioMixer.GetFloat ("ActionVolume", out va1);


			
			

			if (va1 < 0)
				action.audioMixer.SetFloat ("ActionVolume", va1 + 80 * Time.deltaTime * speed_change);
			else
				if (va > -80)
					action.audioMixer.SetFloat ("NoActionVolume", va - 80 * Time.deltaTime * speed_change);


		}

		if (Input.GetKeyDown (KeyCode.Space)) {
			if(cur_temp == 0)
				cur_temp = 1;
			else
				cur_temp = 0;
		}


	}

	public void Set(float temp)
	{
		target_temp = temp;
	}

}
