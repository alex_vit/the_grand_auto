﻿using UnityEngine;
using UnityEditor;
using System.Collections.Generic;
using System.IO;
using System;
using System.Linq;
public class AutoSave : EditorWindow
{
	public static string relativeSavePath = "Autosaves";
	public static string autoSaveFileName = "AutoSave";
	public static int secondsBetweenSaves = 300;
	public static int maxSavedScenes = 10;
	public static bool enabled = false;
	static float nextsave = 0;
	static int timeLeft = 0;
	static int levelID;
	[MenuItem("Template/Autosave/Configure")]
	static void Init()
	{
		EditorWindow.GetWindow (typeof(AutoSave));
		Load ();
	}
	void OnGUI()
	{
		GUILayout.Label ("Autosave Settings", EditorStyles.boldLabel);
		GUILayout.Label ("Save Path");
		relativeSavePath = GUILayout.TextArea (relativeSavePath,50);
		GUILayout.Label ("Postfix");
		autoSaveFileName = GUILayout.TextArea (autoSaveFileName,50);
		secondsBetweenSaves = EditorGUILayout.IntSlider ("Seconds Between Save",secondsBetweenSaves, 1, 1200);
		maxSavedScenes = EditorGUILayout.IntSlider ("Max Saved Scenes", maxSavedScenes, 1, 20);
		enabled = GUILayout.Toggle (enabled, "Enabled");
		if (GUILayout.Button ("Save"))
		{
			Save ();
			Close ();
		}
	}
	static void Save()
	{
		EditorPrefs.SetString ("autosaveRelativeSavePath", relativeSavePath);
		EditorPrefs.SetString ("autosaveFileName", autoSaveFileName);
		EditorPrefs.SetInt ("autosaveSecondsBetweenSaves", secondsBetweenSaves);
		EditorPrefs.SetInt ("autosaveMaxSavedScenes", maxSavedScenes);
		EditorPrefs.SetBool ("autosaveEnabled", enabled);
	}
	static void Load()
	{
		relativeSavePath = EditorPrefs.GetString ("autosaveRelativeSavePath",relativeSavePath);
		autoSaveFileName = EditorPrefs.GetString ("autosaveFileName",autoSaveFileName);
		secondsBetweenSaves = EditorPrefs.GetInt ("autosaveSecondsBetweenSaves",secondsBetweenSaves);
		maxSavedScenes = EditorPrefs.GetInt ("autosaveMaxSavedScenes",maxSavedScenes);
		enabled = EditorPrefs.GetBool ("autosaveEnabled",enabled);
	}
	void OnDestroy()
	{
		Save ();
	}

	[InitializeOnLoad]
	public class StartUp
	{
		static StartUp()
		{
			EditorApplication.update += Update;
			string path = Application.dataPath + "/" + relativeSavePath + "/";
			if (!Directory.Exists(Application.dataPath + "/" + relativeSavePath))
			{
				Directory.CreateDirectory(path);
			}
			Load ();
		}
	}

	static void Update()
	{
		if (enabled && Time.realtimeSinceStartup >= nextsave && 
		   !EditorApplication.isPlaying && 
		   !EditorApplication.isCompiling)
		{
			SaveScene();
		}
		if (Time.realtimeSinceStartup < 0.2f)
			nextsave = 0;
	}

	static void SaveScene()
	{
		levelID = Application.loadedLevel;
		DeleteOlderScenes ();
		string path = Application.dataPath + "/" + relativeSavePath + "/" + Application.loadedLevelName + "_" + autoSaveFileName + "_" + DateTime.Now.ToString ("ddhhmmss") + ".unity";
		try
		{
			EditorApplication.SaveScene(path, true);
			nextsave = Time.realtimeSinceStartup + secondsBetweenSaves;
			Debug.Log ("Scene saved to: " + path);
		}
		catch
		{
			Debug.Log ("Saving Failed");	
		}
	}
	static void DeleteOlderScenes()
	{
		string _path = Application.dataPath + "/" + relativeSavePath + "/";
		DirectoryInfo dir = new DirectoryInfo (_path);
		var scenes = dir.GetFiles ("*.unity").OrderByDescending (a => a.LastWriteTime).ToList ();
		for (int i = maxSavedScenes; i < scenes.Count; i++)
		{
			try
			{
				scenes[i].Delete();
			}
			catch (IOException e)
			{
				Debug.Log(e);
			}
		}
	}

}
